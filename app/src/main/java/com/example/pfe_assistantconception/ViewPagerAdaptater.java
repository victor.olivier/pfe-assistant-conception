package com.example.pfe_assistantconception;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.ArrayList;

//**********************************************************CLASSE POUR ADAPTER LA CHECKLIST***************************************************************************************

public class ViewPagerAdaptater extends FragmentStateAdapter {
    public String  str_Nom_projet, str_Type_site,str_code_site, FolderPath, Nom_site, str_phase, str_operateur;
    public Boolean bool_orange;
    public Boolean bool_bytel;
    public Boolean bool_free;
    public Boolean bool_site_neuf;
    public ArrayList<String> Data_CR= new ArrayList<>();
    public ArrayList<String[]> Data_CR_Check = new ArrayList<>();
    public ArrayList<String> NomCategories;
    public ArrayList<String> NomElements;
    public ArrayList<String> DescriptifElements;
    public ArrayList<String> Commentaires;
    public ArrayList<String> Photos;
    public ArrayList<String[]> Phases;
    public String date;

    //Initialisation des fragments du viewpager
    Fragment fragment_infos = new Fragment_Infos();
    public Fragment fragment_checklist = new Fragment_Checklist();
    Fragment fragment_manuscrit = new Fragment_Manuscrit();



    public ViewPagerAdaptater(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle, String str_Nom_projet,String str_code_site, String Nom_site, Boolean bool_site_neuf, String str_Type_site, Boolean bool_orange, Boolean bool_bytel, Boolean bool_free,
                              ArrayList<String> Txt_ArrayList_NomCategories,ArrayList<String> Txt_ArrayList_NomElements,ArrayList<String> Txt_ArrayList_DescriptifElements,
                              ArrayList<String> Txt_ArrayList_Commentaires,ArrayList<String> Txt_ArrayList_Photos, String FolderPath, String Phase, String str_operateur, ArrayList<String[]> List_Phases, String date) {
        super(fragmentManager, lifecycle);
        this.str_code_site=str_code_site;
        this.str_Nom_projet = str_Nom_projet;
        this.str_Type_site = str_Type_site;
        this.bool_bytel = bool_bytel;
        this.bool_orange = bool_orange;
        this.bool_site_neuf = bool_site_neuf;
        this.NomCategories = Txt_ArrayList_NomCategories;
        this.NomElements=Txt_ArrayList_NomElements;
        this.DescriptifElements=Txt_ArrayList_DescriptifElements;
        this.Commentaires=Txt_ArrayList_Commentaires;
        this.Photos=Txt_ArrayList_Photos;
        this.FolderPath=FolderPath;
        this.Nom_site=Nom_site;
        this.str_phase=Phase;
        this.str_operateur=str_operateur;
        this.Phases=List_Phases;
        this.date=date;
        this.bool_free=bool_free;
    }

    public ViewPagerAdaptater(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle,ArrayList<String> Data_CR, ArrayList<String> Txt_ArrayList_NomCategories,ArrayList<String> Txt_ArrayList_NomElements,
                              ArrayList<String> Txt_ArrayList_DescriptifElements,ArrayList<String> Txt_ArrayList_Commentaires,ArrayList<String> Txt_ArrayList_Photos,String FolderPath,String Nom_site, ArrayList<String[]> Data_CR_Check,
                              String str_code_site, String Phase, String str_operateur,ArrayList<String[]> List_Phases, String date) {
        super(fragmentManager, lifecycle);
        this.Data_CR=Data_CR;
        this.NomCategories = Txt_ArrayList_NomCategories;
        this.NomElements=Txt_ArrayList_NomElements;
        this.DescriptifElements=Txt_ArrayList_DescriptifElements;
        this.Commentaires=Txt_ArrayList_Commentaires;
        this.Photos=Txt_ArrayList_Photos;
        this.FolderPath=FolderPath;
        this.Nom_site=Nom_site;
        this.Data_CR_Check=Data_CR_Check;
        this.str_code_site=str_code_site;
        this.str_phase=Phase;
        this.str_operateur=str_operateur;
        this.Phases=List_Phases;
        this.date=date;
    }

    @NonNull
    //Switch-Case pour afficher les fragments
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                Bundle args = new Bundle();
                try{
                    //Si l'Array List est vide, Activity_Creation_CR est lancée par Activity_Choix contexte
                    if(Data_CR.isEmpty()){
                        Log.i("trybool", "Orange : " + Boolean.toString(bool_orange));
                        Log.i("trybool", "Bytel : " + Boolean.toString(bool_bytel));
                        args.putString("arg_Nom_Projet", str_Nom_projet);
                        args.putString("arg_Code_Site", str_code_site);
                        args.putBoolean("arg_Bool_Orange", bool_orange);
                        args.putBoolean("arg_Bool_Bytel", bool_bytel);
                        args.putBoolean("arg_Bool_Free", bool_free);
                        args.putBoolean("arg_Bool_Site_Neuf", bool_site_neuf);
                        args.putString("arg_Type_site", str_Type_site);
                        args.putString("arg_Nom_site", Nom_site);
                        args.putString("arg_Folder", FolderPath);
                        args.putString("arg_Nom_Phase", str_phase);
                        args.putString("arg_operateur", str_operateur);
                        args.putString("arg_Date", date);

                    }
                    else{
                        //Si l'Array List n'est pas vide, Activity_Creation_CR est lancée par Activity_Liste (modification d'un CR existant)
                        args.putStringArrayList("arg_data",Data_CR);
                        args.putString("arg_Code_Site", str_code_site);
                        args.putString("arg_Folder", FolderPath);

                    }
                    args.putStringArrayList("arg_Noms_Categories",NomCategories);
                    args.putStringArrayList("arg_Noms_Elements",NomElements);
                    args.putStringArrayList("arg_Descriptifs_Elements",DescriptifElements);
                    args.putStringArrayList("arg_Commentaires_oui_non",Commentaires);
                    args.putStringArrayList("arg_Photos_oui_non",Photos);
                    args.putString("arg_FolderPath",FolderPath);
                    args.putString("arg_date",date);
                    fragment_infos.setArguments(args);
                    return fragment_infos;
                }catch(Exception e){
                }
            case 1:
                Bundle args1 = new Bundle();
                try{
                    args1.putStringArrayList("arg_Noms_Categories",NomCategories);
                    args1.putStringArrayList("arg_Noms_Elements",NomElements);
                    args1.putString("arg_Code_site", str_code_site);
                    args1.putStringArrayList("arg_Descriptifs_Elements",DescriptifElements);
                    args1.putStringArrayList("arg_Commentaires_oui_non",Commentaires);
                    args1.putStringArrayList("arg_Photos_oui_non",Photos);
                    args1.putString("arg_FolderPath",FolderPath);
                    args1.putSerializable("arg_Phases", Phases);
                    args1.putString("arg_Nom_Phase", str_phase);


                    if(!Data_CR_Check.isEmpty()){
                        args1.putSerializable("arg_data_cr_check", Data_CR_Check);
                        args1.putString("arg_Code_site", str_code_site);
                    }

                }catch(Exception e){
                }

                fragment_checklist.setArguments(args1);
                return fragment_checklist;

            case 2:
                Bundle args2 = new Bundle();
                args2.putString("arg_Folder", FolderPath);
                args2.putString("arg_Code_site", str_code_site);
                fragment_manuscrit.setArguments(args2);
                return fragment_manuscrit;
        }
        return null;
    }
    @Override
    public int getItemCount() {
        return 3;
    }

}


