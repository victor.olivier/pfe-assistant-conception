package com.example.pfe_assistantconception;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.FileList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static com.example.pfe_assistantconception.App.CHANNEL_1_ID;
import static com.example.pfe_assistantconception.App.CHANNEL_2_ID;

//#########################################################################################################################################################
//--------------------------------------------------------------------Listage des comptes-rendus------------------------------------------------------
//                                   ACTIVITE LISTANT LES CR LES ACTIONS POSSIBLE POUR CHAQUE CR SONT :
//                                   1. MODIFIER UN CR
//                                   2. VISUALISER UN CR
//                                   3. DEPOSER UN CR
//                                   4. SUPPRIMER UN CR
//#########################################################################################################################################################

public class Activity_liste extends AppCompatActivity {

    ListView ListView;

    //Passage par des Arraylists pour ajout dynamique des données du CR
    ArrayList<String> Array_Txt_list_operateur = new ArrayList<>();
    ArrayList<String> Array_Txt_list_projet = new ArrayList<>();
    ArrayList<String> Array_Txt_list_code_site = new ArrayList<>();
    ArrayList<String> Array_Txt_list_date = new ArrayList<>();
    ArrayList<Integer> Array_int_list_images = new ArrayList<>();
    ArrayList<String> Array_Txt_list_datepath = new ArrayList<>();
    ArrayList<String> Array_Etat_Depot = new ArrayList<>();

    DriveServiceHelper driveServiceHelper;
    String ID_Dossier;
    String nom_dossier_site;
    String Path_CR_en_cours;
    int compteur_depot = 0;
    int pos_upload;
    private NotificationManagerCompat notificationManager;
    public String code_site;
    public String operateur;
    Dialog progressdialog;
    Notification notification_dep;
    TextView pas_cr_save, Txt_titre;
    GoogleSignInOptions signInOptions;
    GoogleSignInClient client;
    Activity_liste.MyAdapter adapter;
    TextView Msg_progress;
    Boolean isrefreshing=false;
    String date_path;
    String etat_depot;

//#########################################################################################################################################################
//--------------------------------------------------------------------DEBUT METHODE ONCREATE------------------------------------------------------
//#########################################################################################################################################################

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste);

        notificationManager = NotificationManagerCompat.from(this);

        //Booleen pour savoir si l'activité se refresh ou pas
        Intent intent1 = getIntent();
        if (intent1 != null){
            isrefreshing=intent1.getBooleanExtra("isrefreshing",false);
        }

        //Arraylist à envoyer pour la fonction listerCR, elle contient les noms des fichiers txt et pdf
        ArrayList<String> allFiles = new ArrayList<>();
        //Récupération du chemin du dossier où chercher les fichiers
        String path = getPathDossierRacine();
        //Listage des CR enregistrés en local
        try {
            allFiles = listerCR(new File(path), allFiles);
        } catch (Exception e) {
        }

        //Création d'une autre ArrayList. Elle ne contient que les fichiers txt présents la première Arraylist.
        //Seul les données des fichiers txt sont utiles pour lister tous les CRs enregistrés sur le téléphone
        ArrayList<String> allFiles2 = new ArrayList<>();
        for (int i = 0; i < allFiles.size(); i++) {
            if (allFiles.get(i).contains(".txt")) {
                allFiles2.add(allFiles.get(i));
            }
        }

        Collections.sort(allFiles2);

        ListView = findViewById(R.id.listView2);
        ArrayList lignes = new ArrayList();
        if (allFiles2.size()!=0) {

            //Remplissage des Arraylists
            for (int i = 0; i < allFiles2.size(); i++) {

                try {
                    String line;
                    BufferedReader br = new BufferedReader(new FileReader(allFiles2.get(i)));
                    while ((line = br.readLine()) != null) {
                        lignes.add(line); //On remplit l'Array List avec les lignes du fichier
                    }
                    br.close();
                } catch (Exception E) {
                    displayMsg("Erreur lors de la lecture du fichier");
                }

                Array_Etat_Depot.add((String) lignes.get(lignes.size()-1));

                //Suppression du reste du chemin du fichier
                String str_nom_fichier = allFiles2.get(i).replace(getPathDossierRacine(), "");

                //Split du nom de fichier pour séparer les sous dossiers
                //tab_str[0] : Nom opérateur
                //tab_str[1] : Nom projet
                //tab_str[2] : Code Site et date
                String[] tab_str = str_nom_fichier.split("/");
                Array_Txt_list_operateur.add(tab_str[0]);
                //Détermination des logos
                if (Array_Txt_list_operateur.get(i).contains("Orange")) {
                    Array_int_list_images.add(R.drawable.logo_orange);
                } else if (Array_Txt_list_operateur.get(i).contains("Bouygues Télécom")){
                    Array_int_list_images.add(R.drawable.logo_bytel);
                }
                else if (Array_Txt_list_operateur.get(i).contains("Free Mobile")){
                    Array_int_list_images.add(R.drawable.logo_free);
                }
                Array_Txt_list_projet.add(tab_str[1]);
                //Split pour récupérer code site et nom du site
                String[] code_split = tab_str[2].split("_");
                Array_Txt_list_code_site.add(code_split[0]+"_"+code_split[1]);


                //Récupération de la date dans le nom du fichier txt et ajout dans la ArrayList
                String[] date_split = code_split[2].split("-");
                Array_Txt_list_datepath.add(code_split[2]);

                date_split[2].replace(".txt", "");
                Array_Txt_list_date.add(date_split[0] + "-" + date_split[1] + "-" + date_split[2]);
            }

            //Création de l'objet adapter pour générer le listview
            adapter = new Activity_liste.MyAdapter(this, Array_Txt_list_operateur, Array_Txt_list_projet, Array_Txt_list_code_site, Array_Txt_list_date, Array_int_list_images, Array_Etat_Depot);
            //Application de l'adapterau listview
            ListView.setAdapter(adapter);

            //Si l'aplication n'est pas en train de se refraichir, on demande à l'utilisateur de se connecter avec son compte Google Sade Télécom
            if(!isrefreshing) {
                requestSignIn();
            }

            //Réaction au clique sur un item de la liste
            ListView.setOnItemClickListener((arg0, view, position, arg3) -> {
                //Détermination du chemin vers le CR concerné
                operateur = Array_Txt_list_operateur.get(position);
                String nom_projet = Array_Txt_list_projet.get(position);
                code_site = Array_Txt_list_code_site.get(position);
                date_path= (String) Array_Txt_list_datepath.get(position);
                Path_CR_en_cours = path + operateur + "/" + nom_projet + "/" + code_site+"_"+date_path;

                //Récupération de la date du CR pour chercher le bon CR lors de la modification
                String date = Array_Txt_list_date.get(position);

                //Création du nom du dossier qui sera créé si le CR est à déposer sur le Drive
                nom_dossier_site = code_site + "_" + date;

                //***************************************************************************Ouverture du menu d'action******************************************
                Dialog menu=new Dialog(Activity_liste.this);
                menu.setContentView(R.layout.dialog_box_menu_custom);
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    menu.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                }

                menu.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                menu.getWindow().getAttributes().windowAnimations=R.style.animation;
                menu.setCancelable(true);
                TextView Title = menu.findViewById(R.id.Code_site);
                Title.setText(code_site);
                ImageView Img_operateur = menu.findViewById(R.id.Img_logo_op_dialog);
                if (operateur.matches("Orange")) {
                    Img_operateur.setImageResource(R.drawable.logo_orange);
                } else if (operateur.matches("Bouygues Télécom")) {
                    Img_operateur.setImageResource(R.drawable.logo_bytel);
                }
                else if (operateur.matches("Free Mobile")) {
                    Img_operateur.setImageResource(R.drawable.logo_free);
                }
                View Modifier = menu.findViewById(R.id.View_Modifier);
                View Visualiser = menu.findViewById(R.id.View_Visualiser);
                View Deposer = menu.findViewById(R.id.View_Deposer);
                View Supprimer = menu.findViewById(R.id.View_Supprimer);
                View Annuler = menu.findViewById(R.id.View_annuler);

                //Clique sur modifier le cr
                Modifier.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Appel de la fonction pour récupérer les données du compte rendu contenus dans le fichier txt généré lors de sa création
                        ArrayList<String> Data_CR_Infos = getDataCRInfos(Path_CR_en_cours, date);
                        ArrayList<String[]> Data_CR_Check = getDataCRChecklist(Path_CR_en_cours, date);
                        String date_cr=Array_Txt_list_datepath.get(position);
                        //Lancement de l'activité de création du CR
                        finish();
                        Intent intent = new Intent(Activity_liste.this, Activity_Creation_CR.class);
                        intent.putStringArrayListExtra("Data_CR", Data_CR_Infos);
                        intent.putExtra("Data_CR_Check", Data_CR_Check);
                        intent.putExtra("Date_CR",date_cr);
                        intent.putExtra("Etat_Depot_CR", etat_depot);
                        startActivity(intent);
                        menu.dismiss();
                    }
                });

                //Clique sur visualiser le PDF
                Visualiser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        File dossier = new File(Path_CR_en_cours);
                        File[] fichiers = dossier.listFiles();
                        String pdf;
                        //On parcours les fichiers dans le dossier pour trouver le pdf
                        for (int i = 0; i < fichiers.length; i++) {
                            if (fichiers[i].toString().contains(".pdf")) {
                                pdf = fichiers[i].getAbsolutePath();
                                File pdf_open = new File(pdf);
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                Uri uri = FileProvider.getUriForFile(Activity_liste.this, BuildConfig.APPLICATION_ID + ".provider", pdf_open);
                                intent.setDataAndType(uri, "application/pdf");
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                startActivity(intent);
                                //Le PDF est ouvert dans l'appli par défaut pour visualiser les pdf
                            }
                        }
                        menu.dismiss();
                    }
                });

                //Clique sur déposer le CR
                Deposer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Test si l'appareil est connecté à Internet
                        ConnectivityManager cm = (ConnectivityManager) Activity_liste.this.getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

                        //Si l'appareil est connecté au réseau
                        if (activeNetwork != null) {
                            //Si l'appareil est connecté en Wifi
                            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                                try{
                                    File dossier = new File(Path_CR_en_cours);
                                    if (dossier.exists()) //Si le dossier existe
                                    {
                                        File[] files1 = dossier.listFiles(); //Contient tous les fichiers présents dans le dossier
                                        for (File file : files1) {
                                            //Si le nom de fichier contient la date et se termine en .txt
                                            if (file.getName().contains(".txt")) {
                                                String name = Path_CR_en_cours+"/"+file.getName();
                                                String target = "DEPOT_NOK";
                                                String replace = "DEPOT_OK";
                                                replaceSelected(target, replace,name);
                                            }
                                        }
                                    }
                                    //Appel de la fonction pour déposer le CR sur le Drive
                                    Depose_Drive(operateur, nom_projet, view, Path_CR_en_cours, nom_dossier_site,position); //Fonction pour dépsoer les docs dans le Drive
                                }catch (Exception e){
                                    File dossier = new File(Path_CR_en_cours);
                                    if (dossier.exists()) //Si le dossier existe
                                    {
                                        File[] files1 = dossier.listFiles(); //Contient tous les fichiers présents dans le dossier
                                        for (File file : files1) {
                                            //Si le nom de fichier contient la date et se termine en .txt
                                            if (file.getName().contains(".txt")) {
                                                String name = Path_CR_en_cours+"/"+file.getName();
                                                String target = "DEPOT_OK";
                                                String replace = "DEPOT_NOK";
                                                replaceSelected(target, replace,name);
                                            }
                                        }
                                    }
                                }
                            }
                            //Si l'appareil est connecté avec les données mobiles
                            else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {

                                Dialog dialog1 = new Dialog(Activity_liste.this);
                                dialog1.setContentView(R.layout.dialog_box_custom);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    dialog1.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                                }

                                dialog1.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                dialog1.getWindow().getAttributes().windowAnimations = R.style.animation;
                                dialog1.setCancelable(false);
                                Button ok = dialog1.findViewById((R.id.Btn_dialog_ok));
                                ok.setText("Oui");
                                Button cancel = dialog1.findViewById(R.id.Btn_dialog_cancel);
                                cancel.setText("Non");
                                TextView Title = dialog1.findViewById(R.id.Txt_Title_Alert);
                                Title.setText("Données mobiles actives");
                                ImageView ic = dialog1.findViewById(R.id.Ic_alrt_dialog);
                                ic.setImageDrawable(getDrawable(R.drawable.ic_warning_orange));
                                TextView msg = dialog1.findViewById(R.id.Txt_Msg_alert_dialog);
                                msg.setText("ATTENTION : Vous êtes connecté avec vos données mobiles. Transférer les fichiers peut entrainer un coût supplémentaire. Voulez-vous continuer?");

                                ok.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog1.dismiss();
                                        try{
                                            File dossier = new File(Path_CR_en_cours);
                                            if (dossier.exists()) //Si le dossier existe
                                            {
                                                File[] files1 = dossier.listFiles(); //Contient tous les fichiers présents dans le dossier
                                                for (File file : files1) {
                                                    //Si le nom de fichier contient la date et se termine en .txt
                                                    if (file.getName().contains(".txt")) {
                                                        String name = Path_CR_en_cours+"/"+file.getName();
                                                        String target = "DEPOT_NOK";
                                                        String replace = "DEPOT_OK";
                                                        replaceSelected(target, replace,name);
                                                    }
                                                }
                                            }
                                            Depose_Drive(operateur, nom_projet, view, Path_CR_en_cours, nom_dossier_site, position); //Fonction pour dépsoer les docs dans le Drive
                                        }catch (Exception e){
                                            File dossier = new File(Path_CR_en_cours);
                                            if (dossier.exists()) //Si le dossier existe
                                            {
                                                File[] files1 = dossier.listFiles(); //Contient tous les fichiers présents dans le dossier
                                                for (File file : files1) {
                                                    //Si le nom de fichier contient la date et se termine en .txt
                                                    if (file.getName().contains(".txt")) {
                                                        String name = Path_CR_en_cours+"/"+file.getName();
                                                        String target = "DEPOT_OK";
                                                        String replace = "DEPOT_NOK";
                                                        replaceSelected(target, replace,name);
                                                    }
                                                }
                                            }

                                        }
                                    }
                                });

                                cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog1.dismiss();
                                    }
                                });

                                dialog1.show();
                            }
                        }
                        //Sinon si le téléphone n'est pas connecté au réseau
                        else {
                            Dialog dialog2 = new Dialog(Activity_liste.this);
                            dialog2.setContentView(R.layout.dialog_box_custom);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                dialog2.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                            }

                            dialog2.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            dialog2.getWindow().getAttributes().windowAnimations = R.style.animation;
                            dialog2.setCancelable(false);
                            Button ok = dialog2.findViewById((R.id.Btn_dialog_ok));
                            ok.setText("OK");
                            Button cancel = dialog2.findViewById(R.id.Btn_dialog_cancel);
                            TextView Title = dialog2.findViewById(R.id.Txt_Title_Alert);
                            Title.setText("Pas de connexion internet");
                            ImageView ic = dialog2.findViewById(R.id.Ic_alrt_dialog);
                            ic.setImageDrawable(getDrawable(R.drawable.ic_warning_orange));
                            TextView msg = dialog2.findViewById(R.id.Txt_Msg_alert_dialog);
                            msg.setText("Veuillez activer la Wi-fi ou vos données mobiles avant de réessayer.");
                            cancel.setVisibility(View.INVISIBLE);

                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog2.dismiss();
                                }
                            });
                            dialog2.show();

                        }
                        menu.dismiss();
                    }
                });

                //Clic sur supprimer le CR
                Supprimer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Dialog dialog3 = new Dialog(Activity_liste.this);
                        dialog3.setContentView(R.layout.dialog_box_custom);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            dialog3.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                        }

                        dialog3.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        dialog3.getWindow().getAttributes().windowAnimations = R.style.animation;
                        dialog3.setCancelable(false);
                        Button ok = dialog3.findViewById((R.id.Btn_dialog_ok));
                        ok.setText("Oui");
                        Button cancel = dialog3.findViewById(R.id.Btn_dialog_cancel);
                        cancel.setText("Non");
                        TextView Title = dialog3.findViewById(R.id.Txt_Title_Alert);
                        Title.setText("Attention");
                        ImageView ic = dialog3.findViewById(R.id.Ic_alrt_dialog);
                        ic.setImageDrawable(getDrawable(R.drawable.ic_warning_orange));
                        TextView msg = dialog3.findViewById(R.id.Txt_Msg_alert_dialog);
                        msg.setText("Si vous supprimez ce compte-rendu de votre téléphone assurez-vous de l'avoir déposé sur le Drive au préalable. Cette action est irréversible. Voulez-vous continuer ?");

                        ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Click sur oui
                                //Appel de la fonction pour supprimer le compte-rendu du téléphone.Renvoi vrai si le dossier a bien été supprimé
                                boolean suppr_OK = Suppression_CR(Path_CR_en_cours, position);
                                if (suppr_OK) {
                                    displayMsg("Compte-rendu du site " + code_site + " supprimé");

                                    //Rechargement de l'activité pour actualiser la liste des CR
                                    isrefreshing=true;
                                    finish();
                                    Intent intent = new Intent(Activity_liste.this, Activity_liste.class);
                                    intent.putExtra("isrefreshing", true);
                                    startActivity(intent);

                                } else {
                                    displayMsg("Une erreur s'est produite");
                                }
                                dialog3.dismiss();
                            }
                        });

                        cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog3.dismiss();
                            }
                        });


                        dialog3.show();

                        menu.dismiss();
                    }
                });

                //Clique sur Annuler le CR

                Annuler.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        menu.dismiss();

                    }
                });

                //Affichage du menu
                menu.show();

            });
        } else {

            //Si il n'y a pas de CR enregistré sur le téléphone, on masque le listview et on affiche un textview qui indique qu'il n'y a pas de CR de sauvegardé
            ListView.setVisibility(View.GONE);
            Txt_titre=findViewById(R.id.Txt_activity_Lister_CR);
            Txt_titre.setVisibility(View.GONE);
            pas_cr_save=findViewById(R.id.Txt__pas_CR);
            pas_cr_save.setVisibility(View.VISIBLE);
        }

    }

    //Fonction pour lister les CR enregistrés en local sur le téléphone --> Retourne un Array List contenant les chemins des CR
    public ArrayList<String> listerCR(File path, ArrayList<String> allFiles) {
        //Si le fichier est un dossier, on liste les fichiers présents dans celui-ci
        if (path.isDirectory()) {
            File[] list1 = path.listFiles();
            if (list1 != null) {
                for (File file : list1) {
                    // Appel récursif sur les sous-répertoires
                    listerCR(file, allFiles);
                }
            } else {
                System.err.println(path + " : Erreur de lecture.");
            }
        } else {
            String currentFilePath = path.getAbsolutePath();
            allFiles.add(currentFilePath);
        }
        return allFiles;
    }

    //Fonction pour récupérer le chemin où les CR sont enregistrés (Dossier documents)
    public String getPathDossierRacine() {
        return (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString() + "/Comptes-rendus de VT/");
    }

    //Fonction pour récupérer les données du compte-rendu à modifier pour les placer dans le fragment checklist
    public ArrayList<String[]> getDataCRChecklist(String pathCR, String date) {
        File path = new File(pathCR);
        ArrayList<String> ligne = new ArrayList<>(); //ArrayList contenant les lignes du fichier txt
        ArrayList<String> ligne_temp = new ArrayList<>();
        ArrayList<String[]> data_Check = new ArrayList<>();

        if (path.exists()) //Si le dossier existe
        {
            File[] files = path.listFiles(); //Contient tous les fichiers présents dans le dossier
            for (File file : files) {
                //Si le nom de fichier contient la date et se termine en .txt
                if (file.getName().contains(".txt")) {
                    try {
                        String line;
                        BufferedReader br = new BufferedReader(new FileReader(file));
                        while ((line = br.readLine()) != null) {
                            ligne.add(line); //On remplit l'Array List avec les lignes du fichier
                        }
                        br.close();
                    } catch (Exception E) {
                        displayMsg("Erreur lors de la lecture du fichier");
                    }

                    int n = 0;
                    //On repère le séparateur entre le fragment infos et le fragment checklist
                    for (int i = 0; i < ligne.size(); i++) {
                        if (ligne.get(i).matches("\\*\\*\\*\\*\\*")) {
                            n = i + 1;
                        }
                    }
                    //On récupère les lignes qui concernent la checklist
                    for (int i = n; i < ligne.size(); i++) {
                        ligne_temp.add(ligne.get(i));
                    }
                    etat_depot=ligne_temp.get(ligne_temp.size()-1);
                    ligne_temp.remove(ligne_temp.size()-1);

                    //On split ces lignes
                    for (int y = 0; y < ligne_temp.size(); y++) {
                        data_Check.add(ligne_temp.get(y).split("/", 5)); //Le résultat du split se palce dans la nouvelle ArrayList
                    }

                    //On supprime les espaces inutiles en début et en fin de chaines de caractères
                    for (int i = 0; i < data_Check.size(); i++) {
                        for (int j = 0; j < 5; j++) {
                            data_Check.get(i)[j] = data_Check.get(i)[j].trim();
                        }
                    }
                }
            }
        }
        return data_Check;
    }

    //Fonction pour récupérer les données du compte-rendu à modifier pour les placer dans le fragment infos
    public ArrayList<String> getDataCRInfos(String pathCR, String date) {
        File path = new File(pathCR);
        ArrayList<String> ligne = new ArrayList<>(); //ArrayList contenant les lignes du fichier txt
        ArrayList<String> dataInfo = new ArrayList<>();

        if (path.exists()) //Si le dossier existe
        {
            File[] files = path.listFiles(); //Contient tous les fichiers présents dans le dossier
            for (File file : files) {
                //Si le nom de fichier contient la date et se termine en .txt
                if (file.getName().contains(".txt")) {
                    try {
                        String line;
                        BufferedReader br = new BufferedReader(new FileReader(file));
                        while ((line = br.readLine()) != null) {
                            ligne.add(line); //On remplit l'Array List avec les lignes du fichier
                        }
                        br.close();
                    } catch (Exception E) {
                        displayMsg("Erreur lors de la lecture du fichier");
                    }

                }
            }
            int n = 0;
            for (int i = 0; i < ligne.size(); i++) {
                if (ligne.get(i).matches("\\*\\*\\*\\*\\*")) {
                    n = i;
                }
            }

            for (int i = 0; i < n; i++) {
                dataInfo.add(ligne.get(i));
            }

        } else {
            displayMsg("Le dossier spécifié n'existe pas");
        }
        //Les deux premières lignes de l'ArrayList sont vide, on les supprime (ATTENTION, PAS OPTIMAL A MODIFIER
        for (int i = 0; i < dataInfo.size(); i++) {
            Log.i("tryfile", ligne.get(i));
        }



        return dataInfo;
    }

    //Fonction pour supprimer un CR du téléphone
    public boolean Suppression_CR(String pathCR, int position) {
        File path = new File(pathCR);
        //Passe dans la boucle si le dossier en question existe bien
        if (path.exists()) {
            File[] files = path.listFiles();
            if (files == null) {
                //Sort de la boucle si il n'y a plus de fichier à supprimer

                return true;
            }
            //Supprime les fichiers un par un
            for (File file : files) {
                if (file.isDirectory()) {
                    Suppression_CR(file.toString(),position);
                } else {
                    file.delete();
                }
            }
        }
        return (path.delete());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 400:
                if (resultCode == RESULT_OK) {
                    handleSignInIntent(data);
                }
                else if(resultCode == RESULT_CANCELED){
                    finish();
                }
                else{
                    displayMsg("Une erreur s'est produite lors de la connexion au compte Google.");
                    finish();
                }
                break;
            case 1:

                Dialog dialog = new Dialog(Activity_liste.this);
                dialog.setContentView(R.layout.dialog_box_custom);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                }

                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
                dialog.setCancelable(false);
                Button ok = dialog.findViewById((R.id.Btn_dialog_ok));
                ok.setText("Oui");
                Button cancel = dialog.findViewById(R.id.Btn_dialog_cancel);
                cancel.setText("Non");
                TextView Title = dialog.findViewById(R.id.Txt_Title_Alert);
                Title.setText("Fichiers déposés");
                ImageView ic = dialog.findViewById(R.id.Ic_alrt_dialog);
                ic.setImageDrawable(getDrawable(R.drawable.ic_check_orange));
                TextView msg = dialog.findViewById(R.id.Txt_Msg_alert_dialog);
                msg.setText("Les fichiers ont bien été déposés sur le Drive. Voulez-vous les supprimer de votre téléphone ?");

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //Click sur oui
                        boolean suppr_OK = Suppression_CR(Path_CR_en_cours, pos_upload);
                        if (suppr_OK) {
                            displayMsg("Le compte-rendu du site a bien été supprimé de votre appareil");
                            isrefreshing=true;
                            //Rechargement de l'activité pour actualiser la liste des CR
                            finish();
                            Intent intent = new Intent(Activity_liste.this, Activity_liste.class);
                            intent.putExtra("isrefreshing", isrefreshing);
                            startActivity(intent);
                        } else {
                            displayMsg("Une erreur s'est produite");
                        }

                        dialog.dismiss();
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
        }
    }

    //Fonction pour demander connexion au compte Google
    public void requestSignIn() {
        signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .setHostedDomain("sade-telecom.fr")
                .requestScopes(new Scope(DriveScopes.DRIVE_FILE))
                .build();

        Log.i("tryaccount", "inrequest");

        client = GoogleSignIn.getClient(this, signInOptions);
        startActivityForResult(client.getSignInIntent(), 400);
    }

    //Accès à l'autorisation via les credentials de l'API Drive
    private void handleSignInIntent(Intent data) {
        GoogleSignIn.getSignedInAccountFromIntent(data)
                .addOnSuccessListener(googleSignInAccount -> {
                    GoogleAccountCredential credential = GoogleAccountCredential.usingOAuth2(Activity_liste.this, Collections.singleton(DriveScopes.DRIVE_FILE));
                    credential.setSelectedAccount(googleSignInAccount.getAccount());
                    Drive googleDriveService = new Drive.Builder(
                            AndroidHttp.newCompatibleTransport(),
                            new GsonFactory(),
                            credential)
                            .setApplicationName("PFE App")
                            .build();
                    try{
                        driveServiceHelper = new DriveServiceHelper(googleDriveService);

                    }catch(Exception e){
                    }

                })
                .addOnFailureListener(e -> displayMsg("Connexion au compte Google impossible"));
    }

    //Fonction pour déposer un fichier sur le Drive Google
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void uploadFile(View v, String ID_Drive, String Path_CR_En_Cours, String ID_Photos, int position, String ID_Croquis) throws IOException, InterruptedException {

        //Liste tous les fichiers dans le sous dossier
        File path = new File(Path_CR_En_Cours);
        ArrayList<File[]> subfolder = new ArrayList<>();
        File[] files;
        //Récupère les noms des fichiers listés
        if (path != null) {
            File[] files2 = path.listFiles();
            //On parcours les fichiers du dossier du projet
            for (int i = 0; i < files2.length; i++) {
                //Si le fichier est un répertoire, on liste les fichiers dans ce répertoire
                if (files2[i].isDirectory()) {
                    subfolder.add(files2[i].listFiles());
                }

            }
            files=files2;
            for(int i=0; i<subfolder.size();i++){
                files = ConcatTab(files, subfolder.get(i));
            }
//            File[] files = ConcatTab(files2, subfolder);
            String nom_fichier;

            if (files != null) {


                //Boite de dialogue pour montrer que le dépot est en cours
                Msg_progress.setText("Dépôt des fichiers en cours... 0/" + files.length);

                //On parcours la liste des fichiers contenus dans le dossier du site
                for (compteur_depot = 0; compteur_depot < files.length; compteur_depot++) {

                    File[] finalFiles = files;
                    progressdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            compteur_depot = finalFiles.length;
                            progressdialog.dismiss();
                            displayMsg("Le dépôt a été annulé");
                            notificationManager.cancel(2);
                            String Title1 = code_site + " : Echec du dépôt";
                            String Message1 = "Le dépôt a été annulé";
                            int ic = R.drawable.ic_warning;
                            sendOnChannel1(Title1, Message1, Activity_liste.this, operateur, ic,false);
                        }
                    });

//                    ConnectivityManager cm = (ConnectivityManager) Activity_liste.this.getSystemService(Context.CONNECTIVITY_SERVICE);
//                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

                    int finalI = compteur_depot;
                    nom_fichier = files[compteur_depot].getName(); //On récupère le nom du fichier sous forme d'une chaine de caractères
                    //Si le dépot à réussi
                    //Si le dépot à échoué, on affiche une erreur
                    if(isConnected()) {

                        driveServiceHelper.TaskFile(files[compteur_depot].toString(), ID_Drive, nom_fichier, ID_Photos, ID_Croquis)

                                .addOnSuccessListener(s -> {
                                    Msg_progress.setText("Dépôt des fichiers en cours... " + finalI + "/" + finalFiles.length); //Mise à jour du message dans la boite de dialogue avec le nombre de fichiers restants
                                    if (finalI == finalFiles.length - 1) {
                                        progressdialog.dismiss();
                                        notificationManager.cancel(2);
                                        //Envoi notif pour dire que le dépôt est OK
                                        String Title = code_site + " : Compte rendu déposé !";
                                        String Message = "Cliquez-ici pour générer un mail de compte-rendu";
                                        int ic = R.drawable.ic_check;
                                        sendOnChannel1(Title, Message, Activity_liste.this, operateur, ic, true);
                                        Array_Etat_Depot.set(position, "DEPOT_OK");
                                        adapter.notifyDataSetChanged();
                                        try {
                                            if(isConnected()) {
                                                SendMail(Path_CR_En_Cours, ID_Drive);
                                            }
                                            else{
                                                progressdialog.dismiss();
                                                notificationManager.cancel(2);
                                                displayMsg("Une erreur s'est produite");

                                                String Title1 = code_site + " : Echec du dépôt";
                                                String Message1 = "Une erreur s'est produite lors du dépôt. Veuillez réessayer.";
                                                int ic1 = R.drawable.ic_warning;
                                                sendOnChannel1(Title1, Message1, Activity_liste.this, operateur, ic1, false);
                                            }
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                })
                                .addOnFailureListener(e -> {
                                    progressdialog.dismiss();
                                    notificationManager.cancel(2);
                                    displayMsg("Une erreur s'est produite");

                                    String Title1 = code_site + " : Echec du dépôt";
                                    String Message1 = "Une erreur s'est produite lors du dépôt. Veuillez réessayer.";
                                    int ic = R.drawable.ic_warning;
                                    sendOnChannel1(Title1, Message1, Activity_liste.this, operateur, ic, false);
                                });
                    }
                    else{
                        progressdialog.dismiss();
                        notificationManager.cancel(2);
                        displayMsg("Une erreur s'est produite");

                        String Title1 = code_site + " : Echec du dépôt";
                        String Message1 = "Une erreur s'est produite lors du dépôt. Veuillez réessayer.";
                        int ic = R.drawable.ic_warning;
                        sendOnChannel1(Title1, Message1, Activity_liste.this, operateur, ic, false);
                    }
                }

            }

        }
        else {
            displayMsg("Une erreur s'est produite lors du listage des fichiers.");
            notificationManager.cancel(2);
            String Title1 = code_site + " : Echec du dépôt";
            String Message1 = "Une erreur s'est produite";
            int ic = R.drawable.ic_warning;
            sendOnChannel1(Title1, Message1, Activity_liste.this, operateur, ic,false);
        }
    }

    public static void replaceSelected(String target, String replace, String filename) {
        try {
            // input the file content to the StringBuffer "input"
            BufferedReader file = new BufferedReader(new FileReader(filename));
            StringBuffer inputBuffer = new StringBuffer();
            String line;

            while ((line = file.readLine()) != null) {
                inputBuffer.append(line);
                inputBuffer.append('\n');
            }
            file.close();
            String inputStr = inputBuffer.toString();

            System.out.println(inputStr); // display the original file for debugging

                inputStr = inputStr.replace(target, replace);

            // display the new file for debugging
            System.out.println("----------------------------------\n" + inputStr);

            // write the new string with the replaced line OVER the same file
            FileOutputStream fileOut = new FileOutputStream(filename);
            fileOut.write(inputStr.getBytes());
            fileOut.close();

        } catch (Exception e) {
            System.out.println("Problem reading file.");
        }
    }

    public static void depotTask(){

    }

    public void SendMail(String Path_CR_En_Cours, String ID_Drive) {

        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        emailIntent.setType("plain/text");
        emailIntent.setPackage("com.google.android.gm");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "ACSIT - CRVT : " + nom_dossier_site);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Bonjour,\n" +
                "\n" +
                "Un nouveau compte rendu de VT est disponible pour le site en objet.\n\n" +
                "Les différents éléments ont été déposés sur le Drive. Retrouvez les ici :\n\n" +
                "https://drive.google.com/drive/u/0/folders/" + ID_Drive + "\n\n " +
                "Cet e-mail a été généré avec l'application ACSIT.\n\n\n" +
                "SADE Télécom - 2021 - Tous droits réservés.");

        startActivity(emailIntent);
    }

    public File[] ConcatTab(File[] tab1, File[] tab2) {
        File[] tab3 = Arrays.copyOf(tab1, tab1.length + tab2.length);
        System.arraycopy(tab2, 0, tab3, tab1.length, tab2.length);
        return tab3;

    }

    //Appellé lors d'un clic sur le bouton retour
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (client != null) {
            client.signOut();
        }
        finish();

    }

    public boolean isConnected() throws InterruptedException, IOException {
        String command = "ping -c 1 google.com";
        return Runtime.getRuntime().exec(command).waitFor() == 0;
    }

    @Nullable
    @Override
    public ActionMode onWindowStartingSupportActionMode(@NonNull ActionMode.Callback callback) {
        return super.onWindowStartingSupportActionMode(callback);
    }

    //Fonction pour déposer des fichiers sur le Drive
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void Depose_Drive(String operateur, String nom_projet, View view, String Path_CR_en_cours, String nom_dossier_site, int position) {
        ArrayList<String> ID_Drive = new ArrayList();
        ArrayList<String> projet = new ArrayList();
        ArrayList<String> temp = new ArrayList();
        String fichier = "";
        //Récupération des IDs des Drives selon l'opérateur client
        if (operateur.contains("Bouygues")) {
            fichier = "Drive_Bouygues.txt";
        } else if (operateur.contains("Orange")) {
            fichier = "Drive_Orange.txt";
        }
        else if (operateur.contains("Free Mobile")) {
            fichier = "Drive_Free.txt";
        }
        //Récupération des IDs des Drives selon les projets et selon l'opérateur client
        try {
            InputStream data = getAssets().open(fichier);
            InputStreamReader isr = new InputStreamReader(data);
            BufferedReader reader = new BufferedReader(isr);
            while (reader.ready()) {
                temp.add(reader.readLine());
                projet.add(" ");
                ID_Drive.add(" ");
            }
            ArrayList<String[]> ligne_temp = new ArrayList<>(); //ArrayList de tableaux de string temporaire pour split
            for (int y = 0; y < temp.size(); y++) {
                ligne_temp.add(temp.get(y).split(":", 2)); //Le résultat du split se place dans la nouvelle ArrayList
                ID_Drive.set(y, ligne_temp.get(y)[1].trim()); //On remplit l'ArrayList avec les Ids du Drive
                projet.set(y, ligne_temp.get(y)[0].trim()); //On remplit l'ArrayList avec les noms des projets
            }

            for (int i = 0; i < projet.size(); i++) { //On parcours la liste des projets
                if (projet.get(i).contains(nom_projet)) {  //Si le nom du projet match avec un nom de la liste, le cr est déposé dans le dossier correspondant
                    //On appel la tâche permettant de créer un dossier sur le Drive dans le dossier du projet

                    progressdialog=new Dialog(Activity_liste.this);
                    progressdialog.setContentView(R.layout.dialog_progress);
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        progressdialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                    }

                    progressdialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    progressdialog.getWindow().getAttributes().windowAnimations=R.style.animation;

                    Msg_progress = progressdialog.findViewById(R.id.Txt_dep_en_cours);
                    Msg_progress.setText("Dépôt des fichiers en cours...");


                    progressdialog.show();
                    progressdialog.setCanceledOnTouchOutside(false);

                    String Title = code_site + " : Dépôt des éléments en cours";
                    String Message = "Veuillez laisser votre connexion internet active...";
                    sendOnChannel2(Title, Message, Activity_liste.this, operateur);

                    //Création du dossier du site
                    driveServiceHelper.TaskFolder(nom_dossier_site, ID_Drive.get(i)).addOnCompleteListener(task -> {
                        // Lorsque la tâche est terminée, on récupère l'ID du dossier qui vient d'être créé
                        ID_Dossier = driveServiceHelper.ID;
                        //Création du dossier Croquis
                        driveServiceHelper.TaskFolder("Croquis", ID_Dossier).addOnCompleteListener(task2 -> {
                        String ID_Croquis = driveServiceHelper.ID;
                        //Création du dossier photos
                        driveServiceHelper.TaskFolder("Photos", ID_Dossier).addOnCompleteListener(task1 -> {
                            String ID_Photos = driveServiceHelper.ID;
                            if (ID_Dossier == null) {
                                task1.isComplete();
//                                progressDialog.dismiss();
                                progressdialog.dismiss();

                                //Envoi notif pour dire que dépôt NOK
                                String Title1 = code_site + " : Echec du dépôt";
                                String Message1 = "Une erreur s'est produite. Les éléments n'ont pas été déposés";
                                int ic = R.drawable.ic_warning;
                                sendOnChannel1(Title1, Message1, Activity_liste.this, operateur, ic,false);
                                AlertDialog.Builder builder = new AlertDialog.Builder(Activity_liste.this);
                                builder.setMessage("Une erreur s'est produite. Veuillez vérifier votre connexion internet et réessayer.")
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                            }
                                        })
                                        .show();
                            }
                            try {
                                uploadFile(view, ID_Dossier, Path_CR_en_cours, ID_Photos, position, ID_Croquis); //Dépose des fichiers dans le dossier ainsi créé
                            } catch (IOException | InterruptedException e) {
                                e.printStackTrace();
                            }

                        });
                        });

                    });

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Fonction pour afficher un message toast
    public void displayMsg(String str) {
        Toast.makeText(this, str, Toast.LENGTH_LONG).show();
    }

    //Appellé lorsque l'activité est détruite
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(isrefreshing){
        }
        else if (client != null) {
            client.signOut();
        }
    }

    //Envoi une notification pour une alert : son + vibration + led
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void sendOnChannel1(String Title, String Message, Context context, String operateur, int ic, Boolean depot) {
        int op;
        if (operateur.equals("Orange")) {
            op = R.drawable.logo_orange;
        } else {
            op = R.drawable.logo_bytel;
        }
        if(!depot) {
            @SuppressLint("WrongConstant") Notification notification = new Notification.Builder(this, CHANNEL_1_ID)
                    .setSmallIcon(ic)
                    .setContentTitle(Title)
                    .setContentText(Message)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setCategory(NotificationCompat.CATEGORY_EVENT)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                            op))
                    .setStyle(new Notification.BigTextStyle().bigText(Message))
                    .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
                    .build();

            notificationManager.notify(1, notification);
        }
        else{

            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            emailIntent.setType("plain/text");
            emailIntent.setPackage("com.google.android.gm");
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "ACSIT - CRVT : " + nom_dossier_site);
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Bonjour,\n" +
                    "\n" +
                    "Un nouveau compte rendu de VT est disponible pour le site en objet.\n" +
                    "Les différents éléments ont été déposés sur le Drive. Retrouvez les ici :\n\n" +
                    "https://drive.google.com/drive/u/0/folders/" + ID_Dossier + "\n\n\n " +
                    "Cet e-mail a été généré avec l'application ACSIT.\n\n\n" +
                    "SADE Télécom - 2021 - Tous droits réservés.");

//            File dossier = new File(Path_CR_en_cours);
//            File[] fichiers = dossier.listFiles();
//            String pdf = null;
//            //On parcours les fichiers dans le dossier pour trouver le pdf
//            for (int i = 0; i < fichiers.length; i++) {
//                if (fichiers[i].toString().contains(".pdf")) {
//                    pdf=fichiers[i].toString();
//                }
//            }
//            if(pdf!=null){
//                File cr = new File(pdf);
//                Uri uri = FileProvider.getUriForFile(Activity_liste.this, BuildConfig.APPLICATION_ID + ".provider",cr);
//                emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
//            }
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 1, emailIntent, PendingIntent.FLAG_UPDATE_CURRENT);


            @SuppressLint("WrongConstant") Notification notification = new Notification.Builder(this, CHANNEL_1_ID)
                    .setSmallIcon(ic)
                    .setContentTitle(Title)
                    .setContentText(Message)
                    .setContentIntent(resultPendingIntent)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setCategory(NotificationCompat.CATEGORY_EVENT)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                            op))
                    .setStyle(new Notification.BigTextStyle().bigText(Message))
                    .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
                    .build();

            notificationManager.notify(1, notification);
        }
    }

    //Envoi une notification "Progress"
    @SuppressLint("WrongConstant")
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void sendOnChannel2(String Title, String Message, Context context, String operateur) {
        int op;
        if (operateur.equals("Orange")) {
            op = R.drawable.logo_orange;
        } else {
            op = R.drawable.logo_bytel;
        }
        notification_dep = new Notification.Builder(this, CHANNEL_2_ID)
                .setSmallIcon(R.drawable.ic_depot)
                .setOngoing(true)
                .setContentTitle(Title)
                .setContentText(Message)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setCategory(NotificationCompat.CATEGORY_PROGRESS)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), op))
                .setProgress(100, 0, true)
//                .setDefaults(Notification.DEFAULT_SOUND|Notification.DEFAULT_LIGHTS|Notification.DEFAULT_VIBRATE)
                .build();

        notificationManager.notify(2, notification_dep);
    }

    //////////////////////////////////////////////////////////////////////////DEFINITION DE LA CLASSE MYADAPTER POUR LE LISTEVIEW /////////////////////////////////////////////////////////////////////////
    class MyAdapter extends ArrayAdapter<String> {

        Context context;
        ArrayList<String> roperateur;
        ArrayList<String> rprojet;
        ArrayList<String> rcode;
        ArrayList<String> rdate;
        ArrayList<Integer> rImgs;
        ArrayList<String> retat_depot;

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        private MyAdapter(Context c, ArrayList<String> operateur, ArrayList<String> projet, ArrayList<String> code_site, ArrayList<String> date, ArrayList<Integer> imgs, ArrayList<String> etat_depot) {
            super(c, R.layout.item_liste_cr, R.id.Txt_list_operateur, operateur);
            this.context = c;
            this.roperateur = operateur;
            this.rprojet = projet;
            this.rcode = code_site;
            this.rdate = date;
            this.rImgs = imgs;
            this.retat_depot=etat_depot;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("ViewHolder") View row = layoutInflater.inflate(R.layout.item_liste_cr, parent, false);
            ImageView images = row.findViewById(R.id.Img_logo);
            TextView Txt_operateur = row.findViewById(R.id.Txt_list_operateur);
            TextView Txt_projet = row.findViewById(R.id.Txt_Nom_site);
            TextView Txt_code = row.findViewById(R.id.Txt_list_code_site);
            TextView Txt_date = row.findViewById(R.id.Txt_list_date);
            ImageView Img_etat=row.findViewById(R.id.Img_upload_ok);

            // Ajout des éléments de l'item dans les tableaux correspondants
            images.setImageResource(rImgs.get(position));
            Img_etat.setImageResource(R.drawable.ic_cloud_ok);
            Txt_operateur.setText(roperateur.get(position));
            if (roperateur.get(position).matches("Orange")) {
                Txt_operateur.setTextColor(Color.parseColor("#FF6600"));
            } else if (roperateur.get(position).matches("Bouygues Télécom")) {
                Txt_operateur.setTextColor(Color.parseColor("#009DCC"));
            }

            if(rprojet.get(position).matches("DI Zones Blanches")){
                Txt_projet.setText("DI ZB");
            }
            else {
                Txt_projet.setText(rprojet.get(position));
            }
            Txt_code.setText(rcode.get(position));
            Txt_date.setText(rdate.get(position));

//            Log.i("trydepot", retat_depot.get(0));
            if(retat_depot.get(position).contains("DEPOT_OK")){
              Img_etat.setVisibility(View.VISIBLE);
            }
            else{
                Img_etat.setVisibility(View.INVISIBLE);
            }

            return row;
        }

    }

}

