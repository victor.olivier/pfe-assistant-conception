package com.example.pfe_assistantconception;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;

//*********************************************************************************************************************************************************
//                                           FRAGMENT AVEC LA CHECKLIST, SAISI DE COMMENTAIRES ET PRISE DE PHOTOS
//*********************************************************************************************************************************************************

public class Fragment_Checklist extends Fragment {

    public ArrayList<String> Txt_ArrayList_NomElements = new ArrayList<>(), Txt_ArrayList_DescriptifElements = new ArrayList<>(),
            Txt_ArrayList_Photos = new ArrayList<>(), Txt_ArrayList_Commentaires = new ArrayList<>(), Txt_ArrayList_NomCategories = new ArrayList<>(), CategorieUniques = new ArrayList<>();
    public ChecklistAdapter checklistAdapter;
    private String champs_vides_infos, str_code_site, FolderPath, nom_phase, Ancien_etat;
    ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    ArrayList<String> Commentaires_Listes = new ArrayList<>(), Photo_Listes = new ArrayList<>();
    ArrayList<String[]> Data_CR_Check = new ArrayList<>(), Txt_ArrayList_Phases = new ArrayList<>();
    ArrayList<File> Img_list = new ArrayList<>();
    ArrayList<TextView> Txt_view_menu_list = new ArrayList<>();
    ArrayList<FloatingActionButton> Array_Float = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    int Position;
    Dialog progressdialog;
    String Path_Photo_prise;
    TextView Txt_save, Txt_check, Txt_search_nok;
    FloatingActionButton Float_menu, Float_Check, Float_Genere;
    SearchView Search_check;
    Animation fabOpen, fabClose, fabRClockwise, fabRAntiClockWise;
    Boolean isOpen = false, clique1fois = false, finished = false;
    String Nom_image_gall;
    int position_gall;

    @BindView(R.id.RecyclerviewChecklist)
    RecyclerView recyclerMain;


//#########################################################################################################################################################
//--------------------------------------------------------------------DEBUT METHODE ONCREATEVIEW------------------------------------------------------
//#########################################################################################################################################################

    @RequiresApi(api = Build.VERSION_CODES.O)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment__checklist, container, false);
        ButterKnife.bind(getActivity());

        Float_menu = v.findViewById(R.id.Float_Menu);
        Float_Check = v.findViewById(R.id.Float_Check);
        Float_Genere = v.findViewById(R.id.Float_Genere);
        Search_check = v.findViewById(R.id.Search_check);

        Txt_save = v.findViewById(R.id.Txt_Save);
        Txt_check = v.findViewById(R.id.Txt_Verif);
        Txt_search_nok = v.findViewById(R.id.Txt_search_nok);

        fabOpen = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_open);
        fabClose = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_close);
        fabRClockwise = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_clockwise);
        fabRAntiClockWise = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_anticlockwise);


        //On récupère les éléments enregistrés dans les fichiers txt pour la checklist
        if (getArguments() != null) {
            Txt_ArrayList_NomCategories = getArguments().getStringArrayList("arg_Noms_Categories");
            str_code_site = getArguments().getString("arg_Code_site");
            Txt_ArrayList_NomElements = getArguments().getStringArrayList("arg_Noms_Elements");
            Txt_ArrayList_DescriptifElements = getArguments().getStringArrayList("arg_Descriptifs_Elements");
            Txt_ArrayList_Commentaires = getArguments().getStringArrayList("arg_Commentaires_oui_non");
            Txt_ArrayList_Photos = getArguments().getStringArrayList("arg_Photos_oui_non");
            FolderPath = getArguments().getString("arg_FolderPath");
            Txt_ArrayList_Phases = (ArrayList<String[]>) getArguments().getSerializable("arg_Phases");
            nom_phase = getArguments().getString("arg_Nom_Phase");

            Data_CR_Check = (ArrayList<String[]>) getArguments().getSerializable("arg_data_cr_check");
        }


        //Création d'une liste d'objets de la classe ItemChecklist. Les items sont créés selon ce qui a été renseignés dans les fichiers txt
        List<ItemChecklist> Checklist = new ArrayList<>();
        for (int i = 0; i < Txt_ArrayList_NomElements.size(); i++) {
            Checklist.add((new ItemChecklist(Txt_ArrayList_NomElements.get(i), Txt_ArrayList_DescriptifElements.get(i), Txt_ArrayList_Commentaires.get(i), Txt_ArrayList_Photos.get(i))));
            Commentaires_Listes.add("");
            Photo_Listes.add("");
        }

        //Création et remplissage des listes pour chaque éléments de la checklist. On indique si un commentaire et une photo sont attendu ou non
        for (int i = 0; i < Commentaires_Listes.size(); i++) {
            if (Checklist.get(i).getCommentaire_oui_non().contains("oui")) {
                Commentaires_Listes.set(i, "COMMENTARY REQUIRED");
            } else if (Checklist.get(i).getCommentaire_oui_non().contains("non")) {
                Commentaires_Listes.set(i, "COMMENTARY NOT REQUIRED");
            }

            if (Checklist.get(i).getPhoto_oui_non().contains("oui")) {
                Photo_Listes.set(i, "IMAGE REQUIRED");
            } else if (Checklist.get(i).getPhoto_oui_non().contains("non")) {
                Photo_Listes.set(i, "IMAGE NOT REQUIRED");
            }
        }

        Boolean elem_ok[] = new Boolean[Txt_ArrayList_NomElements.size()];

        for (int i = 0; i < Txt_ArrayList_Phases.size(); i++) {
            if (Txt_ArrayList_Phases.get(i)[0].matches("commun")) {
                elem_ok[i] = true;
            } else {
                for (int j = 0; j < Txt_ArrayList_Phases.get(i).length; j++) {
                    if (Txt_ArrayList_Phases.get(i)[j].trim().matches(nom_phase)) {
                        elem_ok[i] = true;
                        j = Txt_ArrayList_Phases.get(i).length;
                    } else {
                        elem_ok[i] = false;
                    }
                }
                if (elem_ok[i] == null) {
                    elem_ok[i] = false;
                }
            }
        }

        //On liste les différentes catégories dans le fichier texte (Une catégorie par lignes)
        CategorieUniques.add(Txt_ArrayList_NomCategories.get(0));
        //On créé une ArrayList contenant les différetes catégories (pas de doublon)
        for (int i = 1; i < Txt_ArrayList_NomCategories.size(); i++) {
            //Si la catégorie n'existe pas dans la liste des catégories unique, on l'ajoute
            if (!CategorieUniques.contains(Txt_ArrayList_NomCategories.get(i))) {
                CategorieUniques.add(Txt_ArrayList_NomCategories.get(i));
            }

        }

//        On créé les HashMap en triant les éléments selon leur catégorie
        for (int j = 0; j < CategorieUniques.size(); j++) {
            for (int i = 0; i < Txt_ArrayList_NomCategories.size(); i++) {
                if (Txt_ArrayList_NomCategories.get(i).contains(CategorieUniques.get(j))) {
                    HashMap<String, String> dataMAp2 = new HashMap<>();
                    dataMAp2.put("Title", CategorieUniques.get(j));
                    dataMAp2.put("Nom", Txt_ArrayList_NomElements.get(i));
                    dataMAp2.put("Descriptif", Txt_ArrayList_DescriptifElements.get(i));
                    dataMAp2.put("Commentaire_oui_non", Txt_ArrayList_Commentaires.get(i));
                    dataMAp2.put("Photos_oui_non", Txt_ArrayList_Photos.get(i));

                    if (Txt_ArrayList_Commentaires.get(i).contains("oui")) {
                        dataMAp2.put("Commentaire", "COMMENTARY REQUIRED");
                    } else {
                        dataMAp2.put("Commentaire", "COMMENTARY NOT REQUIRED");
                    }

                    if (Txt_ArrayList_Photos.get(i).contains("oui")) {
                        dataMAp2.put("Photos", "IMAGE REQUIRED");
                    } else {
                        dataMAp2.put("Photos", "IMAGE NOT REQUIRED");
                    }

                    if (Txt_ArrayList_Commentaires.get(i).contains("non") && Txt_ArrayList_Photos.get(i).contains("non")) {
                        dataMAp2.put("CORRECT_INCORRECT", "CORRECT");
                    } else {
                        dataMAp2.put("CORRECT_INCORRECT", "INCORRECT");
                    }
                    dataMAp2.put("Pictures_number", String.valueOf(0));
                    dataList.add(dataMAp2);
                }
            }
        }


        //On parcourt la liste des éléments, si ils ne matchent pas avec la phase du projet en cours, on les tag "à supprimer"
        for (int i = 0; i < dataList.size(); i++) {
            if (elem_ok[i] == false) {
                dataList.get(i).put("Nom", "todelete");
            }
        }

        //On supprime les éléments tagés précédemment
        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).containsValue("todelete")) {
                dataList.remove(i);
                i--;
            }
        }


        //Si Data_CR_Check n'est pas vide, nous sommes en modification de CR
        if (Data_CR_Check != null) {


            for (int i = 0; i < dataList.size(); i++) {
                for (int j = 0; j < Data_CR_Check.size(); j++) {
                    if (Data_CR_Check.get(j)[0].matches(Objects.requireNonNull(dataList.get(i).get("Title"))) && (Data_CR_Check.get(j)[1].matches(Objects.requireNonNull(dataList.get(i).get("Nom"))))) {
                        String commentaire = Data_CR_Check.get(j)[2].replace("%n%", "\n");
                        dataList.get(i).put("Commentaire", commentaire);
                        dataList.get(i).put("Photos", Data_CR_Check.get(j)[4]);
                    }
                }
            }

            //On récupère le nombre de photos présentes dans le dossier pour chaque élément
            File path = new File(FolderPath + "/Photos");
            if (path.exists()) {
                for (int i = 0; i < dataList.size(); i++) {
                    int pict_div_number = 0;
                    File[] files = path.listFiles();
                    for (File file : files) {
                        if (file.getPath().contains(dataList.get(i).get("Title")) && file.getPath().contains(dataList.get(i).get("Nom"))) {
                            pict_div_number++;
                        }
                    }
                    dataList.get(i).put("Pictures_number", String.valueOf(pict_div_number));
                }

                File[] list_img = path.listFiles();
                for(int i=0; i<list_img.length;i++){
                    Img_list.add(list_img[i]);
                }
            }
        }

//        //RecyclerViewParent : Catégories
        recyclerMain = v.findViewById(R.id.RecyclerviewChecklist);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerMain.setLayoutManager(linearLayoutManager);

        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(getActivity()) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };

        checklistAdapter = new ChecklistAdapter(dataList, getActivity(), this, this);
        recyclerMain.setAdapter(checklistAdapter);
        //Ajout du header Sticky
        RecyclerItemDecoration recyclerItemDecoration = new RecyclerItemDecoration(getActivity(), 60, true, getSectionCallback(dataList));
        recyclerMain.addItemDecoration(recyclerItemDecoration);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));
        recyclerMain.addItemDecoration(itemDecorator);

        //Appellé à chaque fois qu'un caractère est saisi dans une zone de commentaires
        checklistAdapter.setOnTextChange(new ChecklistAdapter.OnTextChange() {
            @Override
            public void addCommentary(int position, String Commentaire) {
                //Si le champ est vide, le commentaire repasse à l'état "requis"
                if (Commentaire.trim().matches("")) {
                    dataList.get(position).put("Commentaire", "COMMENTARY REQUIRED");
                }
                //Sinon, on récupère le commentaire
                else {
                    dataList.get(position).put("Commentaire", Commentaire);
                }
            }
        });

        //Appellé à chaque fois qu'un clique a lieu sur un item de la checklist
        checklistAdapter.setOnItemClickListener(new ChecklistAdapter.OnItemClickListener() {
            @Override
            //Appellé lors du clique sur l'appareil photo
            public void onAppPhotoClick(int position) {
                String Nom_element = dataList.get(position).get("Nom");
                String Categorie = dataList.get(position).get("Title");
                String Nom_image = str_code_site + "_" + Categorie + "_" + Nom_element;
                //On mémorise l'ancien état en cas d'annulation de la prise de photo
                Ancien_etat = dataList.get(position).get("Photos");
                //On mémorise l'élément en cours en cas d'annulation de la prise de photo
                Position = position;
                //On fait passer la photo de l'élément en cours à "OK", le fond de l'image passe en vert
                dataList.get(position).put("Photos", "PHOTO OK");
                int pic_num = Integer.valueOf(dataList.get(position).get("Pictures_number"));
                pic_num++;
                dataList.get(position).put("Pictures_number", String.valueOf(pic_num));

                String dossier = FolderPath + "/Photos";
                File Photo_folders = new File(dossier);
                if (!Photo_folders.exists()) {
                    try {
                        Photo_folders.mkdir();
                    } catch (Exception e) {
                    }
                }

                Path_Photo_prise = dossier + "/" + Nom_image + ".JPG";
                File file = new File(Path_Photo_prise);
                if(file.exists()){
                    int nb=1;
                    Boolean name=true;
                    while(name) {
                        Path_Photo_prise = dossier + "/" + Nom_image + "(" + nb + ")" + ".JPG";
                        File file2 = new File(Path_Photo_prise);
                        if(file2.exists()){
                            nb++;
                        }
                        else{
                            name=false;
                        }
                    }
                }

                    //On récupère l'URI de la photo à créer
                    Uri uriimage = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", new File(Path_Photo_prise));
                    //On ouvre l'appareil photo en indiquant l'URI de la photo à créer
                    Intent intent_picture = new Intent((MediaStore.ACTION_IMAGE_CAPTURE));
                    intent_picture.putExtra(MediaStore.EXTRA_OUTPUT, uriimage);
                    intent_picture.putExtra("Position", position);
                    intent_picture.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);



                    //MAJ de la liste en arrière plan
                    recyclerMain.post(new Runnable() {
                        @Override
                        public void run() {
                            checklistAdapter.notifyDataSetChanged();
                        }
                    });

                    //Ouverture de l'appareil photo
                    ((Activity) getActivity()).startActivityForResult(intent_picture, position + 100);

            }

            @Override
            //Appellé à chaque fois qu'on clique sur un item de la liste
            public void OnItemClick(int position) {
            }

            @Override
            public void OnEyeClick(int position) {
                //Détermination nom image
                String Nom_element = dataList.get(position).get("Nom");
                String Categorie = dataList.get(position).get("Title");
                String Nom_image = str_code_site + "_" + Categorie + "_" + Nom_element;
                ArrayList<Bitmap> pic = new ArrayList<>();

                int pic_num = Integer.valueOf(dataList.get(position).get("Pictures_number"));

                //Tâche pout lister et afficher les photos de l'élement
                AsyncTask task = new ProgressTask(Nom_image, pic, getActivity(), pic_num, position).execute();

            }

            @Override
            public void OnGalleryClick(int position) {
                String Nom_element = dataList.get(position).get("Nom");
                String Categorie = dataList.get(position).get("Title");
                Nom_image_gall = str_code_site + "_" + Categorie + "_" + Nom_element;
                position_gall = position;

                Intent chooseFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
                chooseFileIntent.setType("image/*");
                chooseFileIntent.addCategory(Intent.CATEGORY_OPENABLE);
                chooseFileIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                chooseFileIntent = Intent.createChooser(chooseFileIntent, "Choisissez une ou plusieurs images");
                startActivityForResult(chooseFileIntent, 6);
            }
        });

        //Ecoute des messages strings
        ViewModelProviders.of(getActivity()).get(CRViewModel.class).getMessageContainerChecklist().observe(this, new Observer<String>() {
            //Appellé lorsqu'un message est reçu
            @Override
            public void onChanged(@Nullable String msg) {
                //Création du CR NOK
                if (msg.matches("files_nok")) {

                    Dialog dialog = new Dialog(getActivity());
                    dialog.setContentView(R.layout.dialog_box_custom);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        dialog.getWindow().setBackgroundDrawable(getActivity().getDrawable(R.drawable.dialog_background));
                    }

                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
                    dialog.setCancelable(false);
                    Button ok = dialog.findViewById((R.id.Btn_dialog_ok));
                    ok.setText("Oui");
                    Button cancel = dialog.findViewById(R.id.Btn_dialog_cancel);
                    cancel.setText("Non");
                    TextView Title = dialog.findViewById(R.id.Txt_Title_Alert);
                    Title.setText("Compte-rendu existant");
                    ImageView ic = dialog.findViewById(R.id.Ic_alrt_dialog);
                    ic.setImageDrawable(getActivity().getDrawable(R.drawable.ic_warning_orange));
                    TextView msg1 = dialog.findViewById(R.id.Txt_Msg_alert_dialog);
                    msg1.setText("Un compte rendu associé à cette VT existe déjà. Voulez-vous mettre à jour les éléments ?");

                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Envoie d'un message au fragment infos pour demander d'envoyer les données à l'activité
                            ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(CRViewModel.class).sendMessageToInfos("collect_data_genere");
                            dialog.dismiss();
                            ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(CRViewModel.class).sendMessageToActivity("replaceCR");
                        }
                    });

                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }
                //Erreur
                else if (msg.matches("files_nok")) {
                    displayMsg("Une erreur s'est produite");
                } else if (msg.matches("app clicked")) {
                    displayMsg(msg);
                }
                //Si la prise de photo a été annulée par l'utilisateur
                else if (msg.matches("ANNULE")) {
                    dataList.get(Position).put("Photos", Ancien_etat);
                    int pic_num = Integer.parseInt(dataList.get(Position).get("Pictures_number"));
                    pic_num--;
                    Log.i("tryannule", String.valueOf(pic_num));
                    dataList.get(Position).put("Pictures_number", String.valueOf(pic_num));
                    recyclerMain.post(new Runnable() {
                        @Override
                        public void run() {
                            checklistAdapter.notifyDataSetChanged();
                        }
                    });

                } else if (msg.matches("Picture_OK")) {

                    try {
                        Img_list.add(new File(Path_Photo_prise));
                        galleryAddPic(Path_Photo_prise);

                    } catch (Exception e) {
                        Log.i("tryfile", e.toString());
                    }

                }
                //Lors de la vérification de la checklist, l'acivité demande la checklist
                else if (msg.matches("CHECK_INFO_RECU_SEND_DATA")) {
                    HashMap<String, String> dataMAp2 = new HashMap<>();
                    dataMAp2.put("USAGE", "POUR VERIF");

                    checklistAdapter.getListfull().add(dataMAp2);
                    ViewModelProviders.of(getActivity()).get(CRViewModel.class).sendArrayListHasMaptoActivity(checklistAdapter.getListfull());
                    checklistAdapter.getListfull().remove(checklistAdapter.ListFULL.size() - 1);
                }
                //Lors de la vérification de la checklist pour générer le CR, l'acivité demande la checklist
                else if (msg.matches("CHECK_INFO_RECU_SEND_DATA_POUR_GENERE")) {
                    HashMap<String, String> dataMAp2 = new HashMap<>();
                    dataMAp2.put("USAGE", "POUR GENERE");
                    checklistAdapter.getListfull().add(dataMAp2);
                    ViewModelProviders.of(getActivity()).get(CRViewModel.class).sendArrayListHasMaptoActivity(checklistAdapter.getListfull());
                    checklistAdapter.getListfull().remove(checklistAdapter.ListFULL.size() - 1);

                } else if (msg.matches("STORAGE_FULL")) {
                    Save_CR();
                }
            }
        });

        //Appellé lors du clique sur le bouton pour générer la Checklist
        Float_Check.setOnClickListener(v1 -> {

            for (int i = 0; i < dataList.size(); i++) {
                if (dataList.get(i).get("Photos").contains("PHOTO OK") && !dataList.get(i).get("Commentaire").contains("REQUIRED")) {
                    dataList.get(i).put("CORRECT_INCORRECT", "CORRECT");
                } else if (dataList.get(i).get("Photos").contains("PHOTO OK") && dataList.get(i).get("Commentaire").contains("COMMENTARY NOT REQUIRED")) {
                    dataList.get(i).put("CORRECT_INCORRECT", "CORRECT");
                } else if (dataList.get(i).get("Photos").contains("IMAGE NOT REQUIRED") && !dataList.get(i).get("Commentaire").contains("REQUIRED")) {
                    dataList.get(i).put("CORRECT_INCORRECT", "CORRECT");
                } else if (dataList.get(i).get("Commentaire").contains("COMMENTARY REQUIRED") || dataList.get(i).get("Photos").contains("IMAGE REQUIRED")) {
                    dataList.get(i).put("CORRECT_INCORRECT", "INCORRECT");
                }
            }

            //MAJ de la checklist en arrière-plan
            recyclerMain.post(new Runnable() {
                @Override
                public void run() {
                    checklistAdapter.notifyDataSetChanged();
                }
            });
            //Envoie un message dans le ViewModel à destination du fragment Fragment_Infos pour vérifier le formulaire dans le but de mettre à jour les badges de notif
            ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(CRViewModel.class).sendMessageToInfos("collect_data_verif");
        });

        //Appellé lorsque l'on clique sur le bouton pour générer le compte-rendu
        Float_Genere.setOnClickListener(v1 -> {
            Save_CR();
        });


        Search_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Search_check.setIconified(false);
            }
        });

        Search_check.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                checklistAdapter.getFilter().filter(newText);
                return false;
            }
        });

        //Appelé lors du clique sur le bouton qui permet d'afficher/masquer les boutons pour vérifier la checklist et générer le CR AVEC ANIMATION
        Float_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isOpen) {
                    Float_Genere.startAnimation(fabClose);
                    Float_Check.startAnimation(fabClose);
                    Txt_save.startAnimation(fabClose);
                    Txt_check.startAnimation(fabClose);
                    Float_menu.startAnimation(fabRClockwise);

                    Search_check.startAnimation(fabClose);

                    Float_Genere.setClickable(false);
                    Float_Check.setClickable(false);

                    isOpen = false;
                    clique1fois = true;
                } else {
                    Float_Genere.startAnimation(fabOpen);
                    Float_Check.startAnimation(fabOpen);
                    Txt_save.startAnimation(fabOpen);
                    Txt_check.startAnimation(fabOpen);
                    Float_menu.startAnimation(fabRAntiClockWise);
                    Search_check.startAnimation(fabOpen);

                    Float_Genere.setClickable(true);
                    Float_Check.setClickable(true);

                    clique1fois = true;
                    isOpen = true;
                }
            }
        });


        return v;
    }

    //#########################################################################################################################################################
//--------------------------------------------------------------------FIN METHODE ONCREATEVIEW------------------------------------------------------
//#########################################################################################################################################################

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        final int[] num_page_pdf = new int[1];

        if(resultCode == RESULT_OK && data!=null){

            switch (requestCode) {
                case 6:
                    ArrayList<Uri> ListUri = new ArrayList<>();
                    ArrayList<String> Paths = new ArrayList<>();

                    if(null != data) { // checking empty selection
                        if(null != data.getClipData()) { // checking multiple selection or not
                            for(int i = 0; i < data.getClipData().getItemCount(); i++) {
                                ListUri.add(data.getClipData().getItemAt(i).getUri());
                                Paths.add(FileUtils.getPath(getActivity(),ListUri.get(i)));

                            }



                            File Photo_folders = new File(FolderPath + "/Photos");
                            if (!Photo_folders.exists()) {
                                try {
                                    Photo_folders.mkdir();
                                } catch (Exception e) {
                                }
                            }

                            for(int i=0; i<Paths.size();i++) {

                                Path_Photo_prise = Photo_folders + "/" + Nom_image_gall + ".JPG";
                                File file = new File(Path_Photo_prise);
                                File file2 = null;
                                if (file.exists()) {
                                    int nb = 1;
                                    Boolean name = true;
                                    while (name) {
                                        Path_Photo_prise = Photo_folders + "/" + Nom_image_gall + "(" + nb + ")" + ".JPG";
                                        file2 = new File(Path_Photo_prise);
                                        if (file2.exists()) {
                                            nb++;
                                        } else {
                                            name = false;
                                        }
                                    }
                                }


                                try {
                                    copy(Paths.get(i), Path_Photo_prise);
                                    if(file2!=null) {
                                        Img_list.add(file2);
                                        galleryAddPic(Path_Photo_prise);


                                        int pic_num = Integer.valueOf(dataList.get(position_gall).get("Pictures_number"));
                                        pic_num++;
                                        dataList.get(position_gall).put("Pictures_number", String.valueOf(pic_num));
                                        dataList.get(position_gall).put("Photos", "PHOTO OK");
                                        checklistAdapter.notifyDataSetChanged();
                                    }
                                    else{
                                        Img_list.add(file);
                                        galleryAddPic(Path_Photo_prise);


                                        int pic_num = Integer.valueOf(dataList.get(position_gall).get("Pictures_number"));
                                        pic_num++;
                                        dataList.get(position_gall).put("Pictures_number", String.valueOf(pic_num));
                                        dataList.get(position_gall).put("Photos", "PHOTO OK");
                                        checklistAdapter.notifyDataSetChanged();

                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }


                        }
                        else {

                            Uri uri_pdf = data.getData();
                            String Imgpath=FileUtils.getPath(getActivity(),uri_pdf);

                            File Photo_folders = new File(FolderPath + "/Photos");
                            if (!Photo_folders.exists()) {
                                try {
                                    Photo_folders.mkdir();
                                } catch (Exception e) {
                                }
                            }

                            Path_Photo_prise = Photo_folders + "/" + Nom_image_gall + ".JPG";
                            File file = new File(Path_Photo_prise);
                            File file2 = null;
                            if(file.exists()){
                                int nb=1;
                                Boolean name=true;
                                while(name) {
                                    Path_Photo_prise = Photo_folders + "/" + Nom_image_gall + "(" + nb + ")" + ".JPG";
                                    file2 = new File(Path_Photo_prise);
                                    if(file2.exists()){
                                        nb++;
                                    }
                                    else{
                                        name=false;
                                    }
                                }
                            }

                            try {
                                copy(Imgpath, Path_Photo_prise);
                                if(file2!=null) {
                                    Img_list.add(file2);
                                    galleryAddPic(Path_Photo_prise);


                                    int pic_num = Integer.valueOf(dataList.get(position_gall).get("Pictures_number"));
                                    pic_num++;
                                    dataList.get(position_gall).put("Pictures_number", String.valueOf(pic_num));
                                    dataList.get(position_gall).put("Photos", "PHOTO OK");
                                    checklistAdapter.notifyDataSetChanged();
                                }
                                else{
                                    Img_list.add(file);
                                    galleryAddPic(Path_Photo_prise);


                                    int pic_num = Integer.valueOf(dataList.get(position_gall).get("Pictures_number"));
                                    pic_num++;
                                    dataList.get(position_gall).put("Pictures_number", String.valueOf(pic_num));
                                    dataList.get(position_gall).put("Photos", "PHOTO OK");
                                    checklistAdapter.notifyDataSetChanged();
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }

                    break;
            }

        }
    }

    public static void copy(String pathsrc, String pathdst) throws IOException {
        File sourceLocation = new File (pathsrc);
        File targetLocation = new File (pathdst);
        try {
                if(sourceLocation.exists()){
                    InputStream in = new FileInputStream(sourceLocation);
                    OutputStream out = new FileOutputStream(targetLocation);
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                    in.close();
                    out.close();
                }

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class ProgressTask extends AsyncTask<String, Void, Boolean> {

        public ProgressTask(String Nom_Image, ArrayList<Bitmap> Pic, Context context, int pic_num, int position) {
            dialog = new Dialog(context);
            Nom_image = Nom_Image;
            pic = Pic;
            num1_pic = pic_num;
            posi = position;
        }

        //ProgressDialog
        private Dialog dialog;
        private String Nom_image;
        private int num1_pic, posi;
        private ArrayList<Bitmap> pic;
        //Dialog pour afficher les photos
        Dialog dialog1 = new Dialog(getActivity());
        Boolean pasvide = false;

        protected void onPreExecute() {


            //Vérification si il existe au moins une photo correspondant à l'élément
            for (int i = 0; i < Img_list.size(); i++) {
                if (Img_list.get(i).getName().contains(Nom_image)) {
                    pasvide = true;
                }
            }

            //Si il y a au moins une photo prise pour l'élément, on les liste et on les affiche
            if (pasvide) {
                Log.i("trypasvide", "pasvide");
                dialog = new Dialog(getActivity());
                this.dialog.setContentView(R.layout.dialog_progress);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    this.dialog.getWindow().setBackgroundDrawable(getActivity().getDrawable(R.drawable.dialog_background));
                }

                this.dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                this.dialog.getWindow().getAttributes().windowAnimations = R.style.animation;

                TextView Msg_progress = this.dialog.findViewById(R.id.Txt_dep_en_cours);
                Msg_progress.setText("Veuillez patienter...");
                this.dialog.setCanceledOnTouchOutside(false);

                this.dialog.show();
            }
            //Sinon, on averti l'utilisateur qu'il n'y a rien à afficher
            else {
                displayMsg("Aucune photo n'a été prise pour cet élément");
            }
        }

        protected void onPostExecute(final Boolean success) {
            if (pasvide) {
//                if (dialog.isShowing()) {
                dialog.dismiss();
//                }

                dialog1.show();
            }

        }

        //Tâche de fond pour lister les images et les afficher dans la boute de dialogue
        protected Boolean doInBackground(final String... args) {
            if (!pasvide) {
                return false;
            } else {
                //Listage des photos prises
                ArrayList<Integer> pos = new ArrayList<>();
                for (int i = 0; i < Img_list.size(); i++) {
                    if (Img_list.get(i).getName().contains(Nom_image)) {
                        Bitmap bit = BitmapFactory.decodeFile(Img_list.get(i).getAbsolutePath());
                        pic.add(getResizedBitmap(bit, bit.getWidth() / 5, bit.getHeight() / 5));
                        pos.add(i);
                    }
                }

                //Boite de dialogue pour visualiser les photos de l'élément
                dialog1.setContentView(R.layout.dialog_open_pictures);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dialog1.getWindow().setBackgroundDrawable(getActivity().getDrawable(R.drawable.dialog_background));
                }

                dialog1.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog1.getWindow().getAttributes().windowAnimations = R.style.animation;
                dialog1.setCancelable(false);
                Button close = dialog1.findViewById((R.id.Btn_dialog_pictures_close));
                close.setText("Fermer");
                TextView Txt_nom = dialog1.findViewById(R.id.Txt_Nom_image);
                Txt_nom.setText(Nom_image);
                ImageView Img_previous = dialog1.findViewById(R.id.Img_previous);
                ImageView Img_next = dialog1.findViewById(R.id.Img_next);
                ImageView Picture = dialog1.findViewById(R.id.Img_picture);
                ImageView Delete_Picture = dialog1.findViewById(R.id.Img_delete_picture);
                TextView Txt_number = dialog1.findViewById(R.id.Txt_compteur_photo);
                Img_previous.setImageDrawable(getResources().getDrawable(R.drawable.ic_previous_grey));

                final int[] i = {0};
                Picture.setImageBitmap(pic.get(i[0]));
                Txt_number.setText(String.valueOf(i[0] + 1) + "/" + pic.size());
                Img_previous.setEnabled(false);
                if (pic.size() == 1) {
                    Img_next.setEnabled(false);
                    Img_next.setImageDrawable(getResources().getDrawable(R.drawable.ic_next_grey));
                }

                //Affichage de l'image suivante
                Img_next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Img_previous.setEnabled(true);
                        Img_previous.setImageDrawable(getResources().getDrawable(R.drawable.ic_previous_accent));

                        Picture.setImageBitmap(pic.get(i[0] + 1));
                        i[0]++;
                        Txt_number.setText(String.valueOf(i[0] + 1) + "/" + pic.size());

                        if (i[0] >= pic.size() - 1) {
                            Img_next.setEnabled(false);
                            Img_next.setImageDrawable(getResources().getDrawable(R.drawable.ic_next_grey));

                        }
                    }
                });

                //Affichage de l'image précédente
                Img_previous.setOnClickListener(new View.OnClickListener() {
                    @SuppressLint("UseCompatLoadingForDrawables")
                    @Override
                    public void onClick(View v) {
                        Img_next.setEnabled(true);
                        Img_next.setImageDrawable(getResources().getDrawable(R.drawable.ic_next_accent));

                        Picture.setImageBitmap(pic.get(i[0] - 1));
                        i[0]--;
                        Txt_number.setText(String.valueOf(i[0] + 1) + "/" + pic.size());

                        if (i[0] < 1) {
                            Img_previous.setEnabled(false);
                            Img_previous.setImageDrawable(getResources().getDrawable(R.drawable.ic_previous_grey));
                        }
                    }
                });

                //Bouton pour supprimer la photo
                Delete_Picture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Dialog pour confirmer la suppression de la photo
                        Dialog dialog2 = new Dialog(getActivity());
                        dialog2.setContentView(R.layout.dialog_box_custom);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            dialog2.getWindow().setBackgroundDrawable(getActivity().getDrawable(R.drawable.dialog_background));
                        }

                        dialog2.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        dialog2.getWindow().getAttributes().windowAnimations = R.style.animation;
                        dialog2.setCancelable(false);
                        Button ok = dialog2.findViewById((R.id.Btn_dialog_ok));
                        ok.setText("Oui");
                        Button cancel = dialog2.findViewById(R.id.Btn_dialog_cancel);
                        cancel.setText("Non");
                        TextView Title = dialog2.findViewById(R.id.Txt_Title_Alert);
                        Title.setText("Suppression de la photo");
                        ImageView ic = dialog2.findViewById(R.id.Ic_alrt_dialog);
                        ic.setImageDrawable(getActivity().getDrawable(R.drawable.ic_warning_orange));
                        TextView msg1 = dialog2.findViewById(R.id.Txt_Msg_alert_dialog);
                        msg1.setText("Voulez-vous supprimer cette photo ?");

                        //Suppression de la photo et miseà jour à de la boite de dialogue en conséquence
                        ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                pic.remove(i[0]);
                                String path = Img_list.get(pos.get(i[0])).getPath();
                                File delete = new File(path);
                                delete.delete();
                                int posit = pos.get(i[0]);
                                Img_list.remove(posit);
                                Log.i("trydelete", String.valueOf(Img_list.size()));
                                num1_pic--;
                                dataList.get(posi).put("Pictures_number", String.valueOf(num1_pic));
                                if(num1_pic==0){
                                    dataList.get(posi).put("CORRECT_INCORRECT", "INCORRECT");
                                    dataList.get(posi).put("Photos", "REQUIRED");
                                }
                                checklistAdapter.notifyDataSetChanged();
                                if (num1_pic != 0) {
                                    if (i[0] != 0) {
                                        i[0]--;
                                    }

                                    Picture.setImageBitmap(pic.get(i[0]));
                                    Txt_number.setText(String.valueOf(i[0] + 1) + "/" + pic.size());
                                    dialog2.dismiss();
                                    displayMsg("La photo a été correctement supprimée.");
                                } else {
                                    dialog2.dismiss();
                                    Txt_number.setText(String.valueOf(i[0] + 1) + "/" + pic.size());
                                    displayMsg("La photo a été correctement supprimée.");

                                    dialog1.dismiss();
                                }
                            }
                        });

                        //Bouton fermer la boite de dialogue
                        cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog2.dismiss();
                            }
                        });
                        dialog2.show();
                    }
                });

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.dismiss();
                    }
                });

                return false;
            }
        }
    }

    private void Save_CR() {
        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).get("Photos").contains("PHOTO OK") && !dataList.get(i).get("Commentaire").contains("REQUIRED")) {
                dataList.get(i).put("CORRECT_INCORRECT", "CORRECT");
            } else if (dataList.get(i).get("Photos").contains("PHOTO OK") && dataList.get(i).get("Commentaire").contains("COMMENTARY NOT REQUIRED")) {
                dataList.get(i).put("CORRECT_INCORRECT", "CORRECT");
            } else if (dataList.get(i).get("Photos").contains("IMAGE NOT REQUIRED") && !dataList.get(i).get("Commentaire").contains("REQUIRED")) {
                dataList.get(i).put("CORRECT_INCORRECT", "CORRECT");
            } else if (dataList.get(i).get("Commentaire").contains("COMMENTARY REQUIRED") || dataList.get(i).get("Photos").contains("IMAGE REQUIRED")) {
                dataList.get(i).put("CORRECT_INCORRECT", "INCORRECT");
            }
        }

        //MAJ de la checklist en arrière-plan
        recyclerMain.post(new Runnable() {
            @Override
            public void run() {
                checklistAdapter.notifyDataSetChanged();
            }
        });

        //Envoie un message dans le ViewModel à destination du fragment Fragment_Infos pour vérifier le formulaire dans le but de générer le CR
        ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(CRViewModel.class).sendMessageToInfos("collect_data_verif_pour_genere");
    }

    //Section CallBack pour le sticky header
    private RecyclerItemDecoration.SectionCallback getSectionCallback(final ArrayList<HashMap<String, String>> list) {
        return new RecyclerItemDecoration.SectionCallback() {
            @Override
            public boolean isSection(int pos) {
                return pos == 0 || list.get(pos).get("Title") != list.get(pos - 1).get("Title");
            }

            @Override
            public String getSectionHeaderName(int pos) {
                HashMap<String, String> dataMap = list.get(pos);
                return dataMap.get("Title");
            }
        };
    }

    public void displayMsg(String str) {
        Toast.makeText(getContext(), str, Toast.LENGTH_LONG).show();
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);
        matrix.postRotate(90);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }


    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

        if (clique1fois) {
            if (isOpen) {
                Float_Genere.setVisibility(View.VISIBLE);
                Float_Check.setVisibility(View.VISIBLE);
                Txt_save.setVisibility(View.VISIBLE);
                Txt_check.setVisibility(View.VISIBLE);
                Search_check.setVisibility(View.VISIBLE);
            } else {
                Float_Genere.setVisibility(View.INVISIBLE);
                Float_Check.setVisibility(View.INVISIBLE);
                Txt_save.setVisibility(View.INVISIBLE);
                Txt_check.setVisibility(View.INVISIBLE);
                Search_check.setVisibility(View.INVISIBLE);

            }
        }
    }

    private void galleryAddPic(String currentPhotoPath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(currentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }
}



