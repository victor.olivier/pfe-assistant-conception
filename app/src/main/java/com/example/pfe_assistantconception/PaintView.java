package com.example.pfe_assistantconception;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.pdf.PdfRenderer;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class PaintView extends View {

    public static int BRUSH_SIZE = 10;
    public static final int DEFAULT_COLOR = Color.BLACK;
    public static final int DEFAULT_BG_COLOR = Color.WHITE;
    private static final float TOUCH_TOLERANCE = 4;

    private float mX, mY;
    private Path mPath;
    private Paint mPaint;
    private int currentColor;
    private int backgroundColor = DEFAULT_BG_COLOR;
    private int strokeWidth;
    private Bitmap mBitmap, Fond_PDF;
    private Canvas mCanvas;
    String Titre;
    private Paint mBitmapPaint = new Paint(Paint.DITHER_FLAG);

    PointF beginCoordinateRect = new PointF(), beginCoordinateCircle = new PointF(),beginCoordinateLine = new PointF(), beginCoordinateTriangle = new PointF();
    PointF endCoordinateRect = new PointF(), endCoordinateCircle= new PointF(), endCoordinateLine = new PointF(), endCoordinateTriangle = new PointF();
    PointF miroirCoordinate = new PointF();

    //Vrai si le fond est extrait d'un pdf, faux si le fond est vierge
    private Boolean isPDF, modif, drawRectangle=false, drawCircle =false;
    public boolean draw = true;
    public int style = 1;
    //style = 1 --> draw
    //style = 2 --> rectangle
    //style = 3 --> circle
    //style = 4 --> line
    //style = 5 --> triangle

    int height_bmp, width_bmp;

    private ArrayList<Draw> paths = new ArrayList<>(); //Contient la liste des chemins dessinés avec le doigt
    private ArrayList<Draw> undo = new ArrayList<>();  //Contient la liste des chemins dessinés avec le doigt et supprimés

    public PaintView(Context context) {
        super(context, null);
    }

    public PaintView(Context context, AttributeSet attrs) {
        super(context, attrs);

        //Initilisation des paramètres de base du paintview
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(DEFAULT_COLOR);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setXfermode(null);
        mPaint.setAlpha(0xff);

    }

    //Initialisation du paintview avec fond vierge
    public void initialise(int heightgen, int widthgen) {
        isPDF = false;
        modif = false;

        //On récupère les dimensions de la zone de dessin
        int height = heightgen;
        int width = widthgen;

        //Création du bitmap vierge blanc
        mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        currentColor = DEFAULT_COLOR;
        strokeWidth = BRUSH_SIZE;

    }

    public void initialisePDF(int heightgen, int widthgen, Bitmap bmp) {
        isPDF = true;
        modif = false;

        Fond_PDF = bmp; //Le BMP du fond pdf est utilisé en tant qu'arrière plan

        //On récupère les dimensions de l'image de fond
        height_bmp = heightgen;
        width_bmp = widthgen;

        //Création d'un Bitmap transparent pour appliquer les dessins
        mBitmap = Bitmap.createBitmap(width_bmp, height_bmp, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        //Application du fond sur la Canvas
        mCanvas.drawBitmap(Fond_PDF,0,0,null);
        currentColor = DEFAULT_COLOR;
        strokeWidth = BRUSH_SIZE;
    }

    //Initialisation du paintview avec fond PDF
    public void initialiseEDIT(int heightgen, int widthgen, Bitmap bmp, String titre) {
        isPDF = true;
        modif = true;
        Titre = titre;

        Fond_PDF = bmp; //Le BMP du fond pdf sans dessin sera utilisé en tant qu'arrière plan

        //On récupère les dimensions de l'image de fond
        height_bmp = heightgen;
        width_bmp = widthgen;

        //Création d'un Bitmap transparent pour appliquer les dessins
        mBitmap = Bitmap.createBitmap(width_bmp, height_bmp, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        //Application du fond sur la Canvas
        mCanvas.drawBitmap(Fond_PDF,0,0,null);
        currentColor = DEFAULT_COLOR;
        strokeWidth = BRUSH_SIZE;

    }

    //On Draw --> Appel auto à chaque dessin
    @Override
    protected void onDraw(Canvas canvas) {

        canvas.save();
        //Si cle fond n'est pas tiré d'un pdf --> fond blanc
        if (!isPDF) {
            mCanvas.drawColor(backgroundColor); // WRONG
        }
        else{
            mCanvas.drawBitmap(Fond_PDF,0,0,null);
        }

        //On redessine les chemins un par un
        for (Draw draw : paths) {
            if(draw.type==1) {
                mPaint.setColor(draw.color); // WRONG
                mPaint.setStrokeWidth(draw.strokeWidth);
                mPaint.setMaskFilter(null);
                mCanvas.drawPath(draw.path, mPaint);
            }
            else if(draw.type==2){
                mPaint.setColor(draw.color); // WRONG
                mPaint.setStrokeWidth(draw.strokeWidth);
                mPaint.setMaskFilter(null);
                mCanvas.drawRect(draw.beginx,draw.beginy,draw.endx,draw.endy,mPaint);
            }
            else if(draw.type==3){
                mPaint.setColor(draw.color); // WRONG
                mPaint.setStrokeWidth(draw.strokeWidth);
                mPaint.setMaskFilter(null);
                mCanvas.drawOval(draw.beginx,draw.beginy,draw.endx,draw.endy,mPaint);
            }
            else if(draw.type==4){
                mPaint.setColor(draw.color); // WRONG
                mPaint.setStrokeWidth(draw.strokeWidth);
                mPaint.setMaskFilter(null);
                mCanvas.drawLine(draw.beginx,draw.beginy,draw.endx,draw.endy,mPaint);
            }
            else if (draw.type==5 || draw.type==6){
                mPaint.setColor(draw.color); // WRONG
                mPaint.setStrokeWidth(draw.strokeWidth);
                mPaint.setMaskFilter(null);
                mCanvas.drawLine(draw.beginx,draw.beginy,draw.endx,draw.endy,mPaint);
                mCanvas.drawLine(draw.beginx,draw.beginy,draw.mirx,draw.miry,mPaint);
                mCanvas.drawLine(draw.mirx,draw.miry,draw.endx,draw.endy,mPaint);
            }
        }

        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
        canvas.restore();

    }

    //Appelé lors d'un appui sur le paintview
    private void touchStart(float x, float y) {

        //DOIGT LEVE
        if(style==1) {
            mPath = new Path();
            Draw draw = new Draw(1,currentColor, strokeWidth, mPath);
            paths.add(draw);
            mPath.reset();
            mPath.moveTo(x, y);
        }

        //RECTANGLE
        else if(style==2)
        {
//            drawRectangle = true; // Stop drawing the rectangle
            beginCoordinateRect.x = x;
            beginCoordinateRect.y = y;
            endCoordinateRect.x = x;
            endCoordinateRect.y = y;
            Draw drawRect = new Draw(2,currentColor, strokeWidth, beginCoordinateRect.x,beginCoordinateRect.y,  beginCoordinateRect.x,beginCoordinateRect.y);
            paths.add(drawRect);

        }
        //DrawCircle
        else if(style==3){

//            drawCircle = true; // Stop drawing the rectangle
            beginCoordinateCircle.x = x;
            beginCoordinateCircle.y = y;
            endCoordinateCircle.x = x;
            endCoordinateCircle.y = y;
            Draw drawCircle = new Draw(3,currentColor, strokeWidth, beginCoordinateCircle.x,beginCoordinateCircle.y,  beginCoordinateCircle.x,beginCoordinateCircle.y);
            paths.add(drawCircle);

        }
        //DrawLine
        else if(style==4){

            beginCoordinateLine.x = x;
            beginCoordinateLine.y = y;
            endCoordinateLine.x = x;
            endCoordinateLine.y = y;
            Draw drawLine = new Draw(4,currentColor, strokeWidth, beginCoordinateLine.x,beginCoordinateLine.y,  endCoordinateLine.x,endCoordinateLine.y);
            paths.add(drawLine);

        }
        //DrawTriangle
        else if(style==5){
            beginCoordinateTriangle.x = x;
            beginCoordinateTriangle.y = y;
            endCoordinateTriangle.x = x;
            endCoordinateTriangle.y = y;
            miroirCoordinate.x=x;
            miroirCoordinate.y=y;
            Draw drawTriangle = new Draw(5,currentColor, strokeWidth, beginCoordinateTriangle.x,beginCoordinateTriangle.y,  endCoordinateTriangle.x,endCoordinateTriangle.y, miroirCoordinate.x,miroirCoordinate.y);
            paths.add(drawTriangle);
        }

        //DrawTriangle
        else if(style==6){
            beginCoordinateTriangle.x = x;
            beginCoordinateTriangle.y = y;
            endCoordinateTriangle.x = x;
            endCoordinateTriangle.y = y;
            miroirCoordinate.x=x;
            miroirCoordinate.y=y;
            Draw drawTriangle = new Draw(6,currentColor, strokeWidth, beginCoordinateTriangle.x,beginCoordinateTriangle.y,  endCoordinateTriangle.x,endCoordinateTriangle.y, miroirCoordinate.x,miroirCoordinate.y);
            paths.add(drawTriangle);
        }
        mX = x;
        mY = y;

    }

    //Appellé lorsque le doigt bouge sur le paintview
    private void touchMove(float x, float y) {
        //DOIGT LEVE
        if(style==1) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);

            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                mX = x;
                mY = y;
            }
        }
        //RECTANGLE
        else if(style==2){
            endCoordinateRect.x = x;
            endCoordinateRect.y = y;
            paths.remove(paths.size()-1);
            Draw drawRect = new Draw(2,currentColor, strokeWidth, beginCoordinateRect.x,beginCoordinateRect.y,  endCoordinateRect.x,endCoordinateRect.y);
            paths.add(drawRect);
        }
        //CERCLE
        else if(style==3){
            endCoordinateCircle.x = x;
            endCoordinateCircle.y = y;
            paths.remove(paths.size()-1);
            Draw drawCircle = new Draw(3,currentColor, strokeWidth, beginCoordinateCircle.x,beginCoordinateCircle.y,  endCoordinateCircle.x,endCoordinateCircle.y);
            paths.add(drawCircle);
        }
        //LINE
        else if(style==4){
            endCoordinateLine.x = x;
            endCoordinateLine.y = y;
            paths.remove(paths.size()-1);
            Draw drawLine = new Draw(4,currentColor, strokeWidth, beginCoordinateLine.x,beginCoordinateLine.y,  endCoordinateLine.x,endCoordinateLine.y);
            paths.add(drawLine);
        }
        //TRIANGLE
        else if(style==5){
            endCoordinateTriangle.x=x;
            endCoordinateTriangle.y=y;
            if(endCoordinateTriangle.x > beginCoordinateTriangle.x && endCoordinateTriangle.y > beginCoordinateTriangle.y){
                miroirCoordinate.y=endCoordinateTriangle.y;
                miroirCoordinate.x = beginCoordinateTriangle.x - (endCoordinateTriangle.x - beginCoordinateTriangle.x);
            }
            else if(endCoordinateTriangle.x < beginCoordinateTriangle.x && endCoordinateTriangle.y > beginCoordinateTriangle.y){
                miroirCoordinate.y=endCoordinateTriangle.y;
                miroirCoordinate.x = beginCoordinateTriangle.x + (beginCoordinateTriangle.x - endCoordinateTriangle.x);
            }
            else if(endCoordinateTriangle.x > beginCoordinateTriangle.x && endCoordinateTriangle.y < beginCoordinateTriangle.y){
                miroirCoordinate.y = endCoordinateTriangle.y;
                miroirCoordinate.x = beginCoordinateTriangle.x -(endCoordinateTriangle.x-beginCoordinateTriangle.x);
            }
            else if(endCoordinateTriangle.x < beginCoordinateTriangle.x && endCoordinateTriangle.y < beginCoordinateTriangle.y){
                miroirCoordinate.y = endCoordinateTriangle.y;
                miroirCoordinate.x= beginCoordinateTriangle.x+(beginCoordinateTriangle.x - endCoordinateTriangle.x);
            }

            paths.remove(paths.size()-1);
            Draw drawTriangle = new Draw(5,currentColor, strokeWidth, beginCoordinateTriangle.x,beginCoordinateTriangle.y,  endCoordinateTriangle.x,endCoordinateTriangle.y, miroirCoordinate.x,miroirCoordinate.y);
            paths.add(drawTriangle);

        }

        //TRIANGLE2
        else if(style==6){
            endCoordinateTriangle.x=x;
            endCoordinateTriangle.y=y;
            if(endCoordinateTriangle.x > beginCoordinateTriangle.x && endCoordinateTriangle.y > beginCoordinateTriangle.y){
                miroirCoordinate.x=endCoordinateTriangle.x;
                miroirCoordinate.y = beginCoordinateTriangle.y-(endCoordinateTriangle.y - beginCoordinateTriangle.y);
            }
            else if(endCoordinateTriangle.x < beginCoordinateTriangle.x && endCoordinateTriangle.y > beginCoordinateTriangle.y){
                miroirCoordinate.x=endCoordinateTriangle.x;
                miroirCoordinate.y = beginCoordinateTriangle.y - (endCoordinateTriangle.y - beginCoordinateTriangle.y);
            }
            else if(endCoordinateTriangle.x > beginCoordinateTriangle.x && endCoordinateTriangle.y < beginCoordinateTriangle.y){
                miroirCoordinate.x = endCoordinateTriangle.x;
                miroirCoordinate.y = beginCoordinateTriangle.y + (beginCoordinateTriangle.y-endCoordinateTriangle.y);
            }
            else if(endCoordinateTriangle.x < beginCoordinateTriangle.x && endCoordinateTriangle.y < beginCoordinateTriangle.y){
                miroirCoordinate.x = endCoordinateTriangle.x;
                miroirCoordinate.y= beginCoordinateTriangle.y+(beginCoordinateTriangle.y - endCoordinateTriangle.y);
            }

            paths.remove(paths.size()-1);
            Draw drawTriangle = new Draw(5,currentColor, strokeWidth, beginCoordinateTriangle.x,beginCoordinateTriangle.y,  endCoordinateTriangle.x,endCoordinateTriangle.y, miroirCoordinate.x,miroirCoordinate.y);
            paths.add(drawTriangle);

        }

    }

    //Appellé lorsque le doigt est levé du paintview
    private void touchUp() {
        //DOIGT LEVE
        if(style==1) {
            mPath.lineTo(mX, mY);
        }
    }

    //Appellé à chaque evènement lié à l'appui sur l'écran
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        if (draw) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                        touchStart(x, y);
                        invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                        touchUp();
                        invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                        touchMove(x, y);
                        invalidate();
                    break;
            }
        }

        return true;

    }

    //Appellé pour clear le croquis
    public void clear() {
        if(isPDF) {
            Bitmap bit = Bitmap.createBitmap(width_bmp, height_bmp, Bitmap.Config.ARGB_8888);
            mBitmap = bit;
            mCanvas.setBitmap(mBitmap);
            mCanvas.drawBitmap(Fond_PDF, 0, 0, null);
            paths.clear();
            invalidate();
        }
        else{
            paths.clear();
            invalidate();
        }
    }

    public void putOverlay(Bitmap bitmap, Bitmap overlay) {
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        canvas.drawBitmap(overlay, 0, 0, paint);
    }

    //Appellé pour annuler le dernier trait
    public void undo() {
        if (paths.size() > 0) {

            if(isPDF) {

                Bitmap bit = Bitmap.createBitmap(width_bmp, height_bmp, Bitmap.Config.ARGB_8888);
                mBitmap = bit;
                mCanvas.setBitmap(mBitmap);
                mCanvas.drawBitmap(Fond_PDF, 0, 0, null);
                undo.add(paths.remove(paths.size() - 1));
                invalidate(); // add
            }
            else{
                undo.add(paths.remove(paths.size() - 1));
                invalidate(); // add
            }

        } else {
            Toast.makeText(getContext(), "Il n'y a rien à effacer", Toast.LENGTH_LONG).show();
        }

    }

    //Appellé pour retrouver le dernier élément supprimé
    public void redo() {
        if (undo.size() > 0) {
            if(isPDF) {
                Bitmap bit = Bitmap.createBitmap(width_bmp, height_bmp, Bitmap.Config.ARGB_8888);
                mBitmap = bit;
                mCanvas.setBitmap(mBitmap);
                mCanvas.drawBitmap(Fond_PDF, 0, 0, null);
                paths.add(undo.remove(undo.size() - 1));
                invalidate();
            }
            else{
                paths.add(undo.remove(undo.size() - 1));
                invalidate();
            }

        }

    }

    //Sauvegarde du croquis sous forme PNF
    public void saveImage(String FOLDERPATH, String NOM_CROQUIS, String CODE_SITE) throws IOException {

        File subDirectory = new File(FOLDERPATH + "/Croquis");

        if (!subDirectory.exists()) {
            subDirectory.mkdir();
        }
        File image;

        if (modif) {
            image = new File(subDirectory + "/" + CODE_SITE + "_" + NOM_CROQUIS + ".png");

            if (image.exists()) {
                image.delete();
            }

        } else {
            image = new File(subDirectory + "/" + CODE_SITE + "_" + NOM_CROQUIS + ".png");

            Boolean rename = true;
            //On vérifie si un croquis du même nom existe déjà
            if (image.exists()) {
                int i = 1;
                //Si il existe, on le renomme jusqu'à obtenir un nom valide
                while (rename) {
                    image = new File(subDirectory + "/" + CODE_SITE + "_" + NOM_CROQUIS + "(" + i + ")" + ".png");
                    if (image.exists()) {
                        i++;
                    } else {
                        rename = false;
                    }
                }
            }

        }

        FileOutputStream fileOutputStream;

        //Si le croquis est vierge
        if (!isPDF) {

            try {

                fileOutputStream = new FileOutputStream(image);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();

                Toast.makeText(getContext(), "Croquis sauvegardé", Toast.LENGTH_LONG).show();

            } catch (FileNotFoundException e) {


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //Si le croquis est issu d'un fond PDF
        else {
            //Création d'un bitmap en fusionnant le fond du pdf et le paintview
            Bitmap Croquis = overlay(Fond_PDF, mBitmap);
            Log.i("trysize", "pdf height : " + Fond_PDF.getHeight() + "pdf width : " + Fond_PDF.getWidth() + "paint height : " + mBitmap.getHeight() + "paint width : " + mBitmap.getWidth());
            fileOutputStream = new FileOutputStream(image);
            Croquis.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            Toast.makeText(getContext(), "Croquis sauvegardé", Toast.LENGTH_LONG).show();

        }
    }


    public void setStrokeWidth(int width) {
        strokeWidth = width;
    }

    public int getStrokeWidth() {
        return strokeWidth;
    }

    //Fonction pour fusionner deux Bitmap de mêmes dimensions
    public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, new Matrix(), null);
        return bmOverlay;
    }

    public int getCurrentColor() {
        return currentColor;
    }

    public void setColor(int color) {
        currentColor = color;
    }

    public boolean isDraw() {
        return draw;
    }

    public void setDraw(boolean draw) {
        this.draw = draw;
    }

    public int getStyle() {
        return style;
    }

    public void setStyle(int style) {
        this.style = style;
    }
}
