package com.example.pfe_assistantconception;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;

import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

//*********************************************************************************************************************************************************
//                                                        CLASSE POUR GERER DEPOT SUR LE DRIVE
//*********************************************************************************************************************************************************

public class DriveServiceHelper {
    private final Executor mExecutor = Executors.newSingleThreadExecutor();
    private final Drive mDriveService;
    public String ID;
    public Boolean Dossier_Existant=false;

    public DriveServiceHelper(Drive mDriveService)
    {
        this.mDriveService = mDriveService;
    }

    String fichiertype;

    //Tâche pour créer un fichier sur le Drive
    public Task<String> TaskFile(String fichier, String ID_Drive,String nom_fichier, String ID_Photos, String ID_Croquis) {

        String IDdoss=null;

        if(nom_fichier.endsWith(".pdf")){
            fichiertype="application/pdf";
            IDdoss =ID_Drive;
        }
        else if(nom_fichier.endsWith(".txt")){
            fichiertype="text/plain";
            IDdoss=ID_Drive;
        }
        else if(nom_fichier.endsWith(".png")){
            fichiertype="image/png";
            IDdoss=ID_Croquis;
        }
        else if(nom_fichier.toLowerCase().endsWith(".jpg")){
            fichiertype="image/jpeg";
            IDdoss =ID_Photos;
        }

        String finalIDdoss = IDdoss;
        return Tasks.call(mExecutor,() ->
        {
            createFile(fichier, finalIDdoss, fichiertype, nom_fichier);
            return ID_Drive;
        });
    }

    //Fonction pour créer un fichier sur le Drive
    public void createFile(String filePath, String ID_Drive, String fichiertype, String nom_fichier)
    {
        //Données du fichier à créer sur le Drive. ATTENTION : set parents : correspond à l'ID du dossier dans lequel le CR doit être placé
        File fileMetaData = new File();
        fileMetaData.setName(nom_fichier);
        fileMetaData.setParents(Collections.singletonList(ID_Drive));
        fileMetaData.setMimeType(fichiertype);

        //filePath : Chemin abolu du fichier à Upload
        java.io.File file = new java.io.File(filePath);

        //Type du fichier à upload
        FileContent mediaContent = new FileContent(fichiertype,file);

        File myfile = null;
        //Upload du fichier
        try{
            myfile=mDriveService.files().create(fileMetaData, mediaContent).setSupportsTeamDrives(true).execute();
        }
        catch(Exception e){
            e.printStackTrace();
        }

        if(myfile == null){
        }

    }

    //Tâche pour créer un dossier sur le Drive
    public Task<String> TaskFolder(String Nom_Dossier, String ID_Drive){
        return Tasks.call(mExecutor,() ->
        {
            CreateFolder(Nom_Dossier, ID_Drive);

            return ID;
        });
    }

    //Fonction pour créer un dossier sur le Drive
    public void CreateFolder (String Nom_Dossier, String ID_parent) throws IOException {
        File fileMetadata = new File();
        fileMetadata.setName(Nom_Dossier);
        fileMetadata.setParents(Collections.singletonList(ID_parent));
        fileMetadata.setMimeType("application/vnd.google-apps.folder");

        File myfolder = mDriveService.files().create(fileMetadata).setSupportsTeamDrives(true).execute();

        ID=myfolder.getId();
    }
}