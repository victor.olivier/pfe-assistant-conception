package com.example.pfe_assistantconception;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.HashMap;

//*********************************************************************************************************************************************************
//                                           CLASSE "BOITE AUX LETTRES" POUR COMMUNIQUER ENTRE LES FRAGMENTS ET L'ACTIVITE DE CREA CR
//*********************************************************************************************************************************************************

public class CRViewModel extends ViewModel {
    private MutableLiveData<String> messageContainerInfos;
    private MutableLiveData<String> messageContainerChecklist;
    private MutableLiveData<String> messageContainerActivity;
    private MutableLiveData<ArrayList<String>> messageContainerArrayListActivity;
    private MutableLiveData<ArrayList<String>> messageContainerArrayListChecklist;
    private MutableLiveData<String> messageContainerChecklistAdapter;
    private MutableLiveData<ArrayList<String>> messageContainerArrayListChecklistAdapter;
    private MutableLiveData<ArrayList<String[]>> messageContainerArrayListTableauStringActivity;
    private MutableLiveData<String> messageContainerMainAdapter;
    private MutableLiveData<ArrayList<ArrayList<String[]>>> messageContainerArrayListArrayListTableauStringActivity;
    private MutableLiveData<ArrayList<String[]>> messageContainerArrayListTableauStringChecklistAdapter;
    private MutableLiveData<ArrayList<ArrayList<String>>> messageContainerArrayListArrayListStringstoActivity;
    private MutableLiveData<ArrayList<ArrayList<String>>> messageContainerArrayListArrayListStringstoChecklist;
    private MutableLiveData<ArrayList<HashMap<String,String>>> messageContainerArrayListHasMaptoActivity;

    public void init()
    {
        messageContainerInfos = new MutableLiveData<>();
        messageContainerChecklist = new MutableLiveData<>();
        messageContainerActivity = new MutableLiveData<>();
        messageContainerArrayListActivity = new MutableLiveData<>();
        messageContainerArrayListChecklist = new MutableLiveData<>();
        messageContainerChecklistAdapter = new MutableLiveData<>();
        messageContainerArrayListChecklistAdapter = new MutableLiveData<>();
        messageContainerMainAdapter = new MutableLiveData<>();
        messageContainerArrayListTableauStringActivity = new MutableLiveData<>();
        messageContainerArrayListArrayListTableauStringActivity = new MutableLiveData<>();
        messageContainerArrayListTableauStringChecklistAdapter = new MutableLiveData<>();
        messageContainerArrayListArrayListStringstoActivity = new MutableLiveData<>();
        messageContainerArrayListArrayListStringstoChecklist = new MutableLiveData<>();
        messageContainerArrayListHasMaptoActivity = new MutableLiveData<>();
    }

    public void sendArrayListHasMaptoActivity(ArrayList<HashMap<String,String>> msg){ messageContainerArrayListHasMaptoActivity.setValue(msg); }
    public void sendArrayListArrayLisToChecklist(ArrayList<ArrayList<String>> msg){ messageContainerArrayListArrayListStringstoChecklist.setValue(msg); }
    public void sendArrayListArrayListStringtoActivity(ArrayList<ArrayList<String>> msg){ messageContainerArrayListArrayListStringstoActivity.setValue(msg); }
    public void sendMessageToChecklistAdapter(String msg){ messageContainerChecklistAdapter.setValue(msg);}
    public void sendMessageToChecklist(String msg)
    {
        messageContainerChecklist.setValue(msg);
    }
    public void sendMessageToInfos(String msg) { messageContainerInfos.setValue(msg); }
    public void sendMessagetoMainAdapter(String msg)
    {
        messageContainerMainAdapter.setValue(msg);
    }
    public void sendMessageToActivity(String msg) { messageContainerActivity.setValue(msg); }
    public void sendArrayListToActivity(ArrayList<String> msg) { messageContainerArrayListActivity.setValue(msg); }
    public void sendArrayListToChecklist(ArrayList<String> msg) { messageContainerArrayListChecklist.setValue(msg); }
    public void sendArrayListToChecklistAdaptater(ArrayList<String> msg){ messageContainerArrayListChecklistAdapter.setValue(msg); }
    public void sendArrayListeTableauStringToActivity(ArrayList<String[]> msg){ messageContainerArrayListTableauStringActivity.setValue(msg); }
    public void sendArrayListArrayListTableauStringActivity(ArrayList<ArrayList<String[]>> msg){ messageContainerArrayListArrayListTableauStringActivity.setValue(msg); }
    public LiveData<ArrayList<HashMap<String,String>>> getArrayListHashMapActivity(){return messageContainerArrayListHasMaptoActivity;}
    public LiveData<String> getMessageContainerInfos() {
        return messageContainerInfos;
    }
    public LiveData<String> getMessageContainerActivity() {
        return messageContainerActivity;
    }
    public LiveData<String> getMessageContainerChecklist() {
        return messageContainerChecklist;
    }
    public LiveData<String> getMessageContainerMainAdapter() {
        return messageContainerMainAdapter;
    }
    public LiveData<ArrayList<String>> getArrayListContainerActivity() {return messageContainerArrayListActivity; }
    public LiveData<ArrayList<String>> getArrayListContainerChecklist() {return messageContainerArrayListChecklist; }
    public LiveData<String> getMessageContainerChecklistAdapter(){return messageContainerChecklistAdapter;}
    public LiveData<ArrayList<String>> getArrayListChecklistAdapter(){return messageContainerArrayListChecklistAdapter;}
    public LiveData<ArrayList<String[]>> getArrayListTableauStringActivity(){return messageContainerArrayListTableauStringActivity;}
    public LiveData<ArrayList<ArrayList<String[]>>> getArrayListArrayListTableauStringActivity(){ return messageContainerArrayListArrayListTableauStringActivity; }
    public LiveData<ArrayList<ArrayList<String>>> getArrayListArrayListStringActivity(){ return messageContainerArrayListArrayListStringstoActivity;}
    public LiveData<ArrayList<ArrayList<String>>> getArrayListArrayListStringChecklist(){ return messageContainerArrayListArrayListStringstoChecklist;}

}
