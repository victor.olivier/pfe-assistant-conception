package com.example.pfe_assistantconception;

import android.graphics.Path;
import android.graphics.RectF;

public class Draw {

    public int color;
    public int strokeWidth;
    public Path path;
    public float beginx, beginy, endx,endy, mirx, miry;
    public int type;

    public Draw(int type,int color, int strokeWidth, Path path) {
        this.type = type;
        this.color = color;
        this.strokeWidth = strokeWidth;
        this.path = path;

    }

    public Draw(int type,int color, int strokeWidth, float beginx, float beginy, float endx, float endy) {
        this.type = type;
        this.color = color;
        this.strokeWidth = strokeWidth;
        this.beginx = beginx;
        this.beginy=beginy;
        this.endx = endx;
        this.endy = endy;

    }

    public Draw(int type,int color, int strokeWidth, float beginx, float beginy, float endx, float endy, float mirx, float miry) {
        this.type = type;
        this.color = color;
        this.strokeWidth = strokeWidth;
        this.beginx = beginx;
        this.beginy=beginy;
        this.endx = endx;
        this.endy = endy;
        this.mirx=mirx;
        this.miry=miry;
    }


}
