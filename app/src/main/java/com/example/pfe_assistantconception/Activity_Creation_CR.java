package com.example.pfe_assistantconception;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.pdf.PdfDocument;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Scanner;

import static com.example.pfe_assistantconception.App.CHANNEL_1_ID;
import static com.example.pfe_assistantconception.App.CHANNEL_3_ID;

//#########################################################################################################################################################
//--------------------------------------------------------------------CREATION D'UN COMPTE-RENDU------------------------------------------------------
//                                   ACTIVITE HERBERGEANT LES 3 FRAGMENTS DE CREATION DE COMPTE RENDUS A SAVOIR :
//                                   1. FRAGMENT INFOS
//                                   2. FRAGMENT CHECKLIST
//                                   3. FRAGMENT MANUSCRIT
//                                   LES FRAGMENTS SONT PLACES DANS UN VIEWPAGER
//                                   LA GENERATION DU TXT ET DU PDF SE FONT ICI
//#########################################################################################################################################################

public class Activity_Creation_CR extends AppCompatActivity {

    private Boolean bool_orange = false,bool_bytel = false, bool_free = false,bool_Site_neuf = false,bool_orange_from_infos = false,bool_bytel_from_infos = false,bool_free_from_infos = false;
    private int refresh=0,champs_restants_infos = 4;
    public String str_Nom_projet, str_Type_site, operateur, neuf_ou_existant, typologie, str_commentaire,
            FOLDERNAME, FILENAMETXT, FILENAMEPDF, str_contenuCRTXT, str_code_site, FolderPath,
            str_nom_site, FolderPathImage, str_phase, client_cr, codesite_cr, typesite_cr, projet_cr, siteneuf_cr, nomsite_cr, FolderPathCroquis,
            adresse_cr, code_postal_cr, ville_cr, vtdate_cr, str_operateur, coordonnes, etat_depot, date, line = System.getProperty("line.separator");
    private ArrayList<String> Data_CR, Image_Path_List = new ArrayList<>(),Categorie_Unique = new ArrayList<>(),Txt_ArrayList_NomCategories = new ArrayList<>(),
            Txt_ArrayList_NomElements = new ArrayList<>(), Txt_ArrayList_DescriptifElements = new ArrayList<>(),
            Txt_ArrayList_Photos = new ArrayList<>(), Txt_ArrayList_Commentaires = new ArrayList<>(), Txt_ArrayList_allElements = new ArrayList<>(), Croquis_Path_List = new ArrayList<>();
    private ArrayList<String[]> Data_CR_Check,Txt_ArrayList_Phases = new ArrayList<>();
    private ViewPager2 viewpager;
    private TabLayout tabLayout;
    private BadgeDrawable badgeDrawableChecklist, badgeDrawableInfos;
    private CRViewModel pagerAgentViewModel;
    private Bitmap bmp;
    Dialog progressdialog;
    TextView Msg_progress;
    ArrayList<HashMap<String, String>> dataChecklist = new ArrayList<>();
    ViewPagerAdaptater viewPagerAdaptater;
    private NotificationManagerCompat notificationManager;

    //***************************************************POLICES A APPLIQUER DANS LA CREATION DE COMPTES RENDUS*************************************************************
    private static final Font Titre1 = new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD, new BaseColor(0, 147, 208));
    private static final Font Titre2 = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD, new BaseColor(0, 147, 208));
    private static final Font Souligne = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.UNDERLINE);
    private static final Font Gras = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    private static final Font INVALIDE = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLDITALIC, BaseColor.RED);
    private static final Font Correct = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, BaseColor.GREEN);
    private static final Font Incorrect = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, BaseColor.ORANGE);
    private static final Font Categorie = new Font(Font.FontFamily.TIMES_ROMAN, 24, Font.BOLD | Font.UNDERLINE, new BaseColor(0, 130, 203));
    private static final Font Nom_element = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD, new BaseColor(0, 100, 255));


//#########################################################################################################################################################
//--------------------------------------------------------------------DEBUT METHODE ONCREATE------------------------------------------------------
//#########################################################################################################################################################

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creation__c_r);

        //Verification des permissions
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.CAMERA
        };

        //Redemande les permissions si elles n'ont pas été accordées
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        notificationManager = NotificationManagerCompat.from(this);

        //Initialisation du View Model pour recevoir les datas des fragments
        pagerAgentViewModel = ViewModelProviders.of(this).get(CRViewModel.class);
        pagerAgentViewModel.init();

        //*******************************************************ECOUTE HASH MAP <STRING,STRING>*********************************************

        //Récupération de la checklist et MAJ des badges de notifications
        pagerAgentViewModel.getArrayListHashMapActivity().observe(this, new Observer<ArrayList<HashMap<String, String>>>() {
            @Override
            public void onChanged(ArrayList<HashMap<String, String>> hashMaps) {
                if (hashMaps.size() != 0 && hashMaps.get(hashMaps.size() - 1) != null) {

                    //provient de FRAGMENT_CHECKLIST, vérification de la checklist
                    if (hashMaps.get(hashMaps.size() - 1).get("USAGE").matches("POUR VERIF")) {
                        dataChecklist = hashMaps;
                        int ItemIncorrect_Checklist = 0;
                        for (int i = 0; i < dataChecklist.size(); i++) {
                            if (dataChecklist.get(i).get("CORRECT_INCORRECT") == "INCORRECT") {
                                ItemIncorrect_Checklist++;
                            }
                        }
                        badgeDrawableChecklist.setNumber(ItemIncorrect_Checklist);

                        if (ItemIncorrect_Checklist == 0) {
                            badgeDrawableChecklist.setVisible(false);
                        } else {
                            badgeDrawableChecklist.setVisible(true);
                        }

                        if (badgeDrawableInfos.getNumber() == 0 && badgeDrawableChecklist.getNumber() == 0) {
                            displayMsg("Checklist COMPLETE");
                        } else {
                            displayMsg("Checklist INCOMPLETE");
                        }
                    }

                    //provient de FRAGMENT_CHECKLIST, vérification de la checklist pour GENERER LE CR ENSUITE
                    if (hashMaps.get(hashMaps.size() - 1).get("USAGE").matches("POUR GENERE")) {
                        dataChecklist = hashMaps;
                        int ItemIncorrect_Checklist = 0;
                        for (int i = 0; i < dataChecklist.size(); i++) {
                            if (dataChecklist.get(i).get("CORRECT_INCORRECT") == "INCORRECT") {
                                ItemIncorrect_Checklist++;
                            }
                        }

                        badgeDrawableChecklist.setNumber(ItemIncorrect_Checklist);

                        if (ItemIncorrect_Checklist == 0) {
                            badgeDrawableChecklist.setVisible(false);
                        } else {
                            badgeDrawableChecklist.setVisible(true);
                        }

                        //Si la checklist est complète, on informe l'utilisateur et on demande à générer le CR
                        if (badgeDrawableInfos.getNumber() == 0 && badgeDrawableChecklist.getNumber() == 0) {
                            Dialog dialog = new Dialog(Activity_Creation_CR.this);
                            dialog.setContentView(R.layout.dialog_box_custom);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                            }

                            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
                            dialog.setCancelable(false);
                            Button ok = dialog.findViewById((R.id.Btn_dialog_ok));
                            ok.setText("Oui");
                            Button cancel = dialog.findViewById(R.id.Btn_dialog_cancel);
                            cancel.setText("Non");
                            TextView Title = dialog.findViewById(R.id.Txt_Title_Alert);
                            Title.setText("Checklist complète");
                            ImageView ic = dialog.findViewById(R.id.Ic_alrt_dialog);
                            ic.setImageDrawable(getDrawable(R.drawable.ic_check_orange));
                            TextView msg = dialog.findViewById(R.id.Txt_Msg_alert_dialog);
                            msg.setText("La Checklist est complète ! Voulez-vous générer le compte-rendu ?");

                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //Envoie d'un message au fragment infos pour indiquer d'envoyer les données pour génrer le CR
//                                    pagerAgentViewModel.sendMessageToInfos("collect_data_genere");
//                                    dialog.dismiss();
                                    Msg_progress.setText("Génération du compte-rendu en cours.\nVeuillez patienter...\n" +
                                            "Vous pouvez éteindre l'écran de votre téléphone mais veillez à ne pas quitter l'application.");
                                    progressdialog.setCanceledOnTouchOutside(false);
                                    progressdialog.show();
                                    Genere_CR();
                                    dialog.dismiss();
                                }
                            });

                            cancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });

                            dialog.show();

                        }

                        //Si la checkist n'est pas complète, on informe l'utilisateur et on lui demande si il souhaite tout de même générer le CR
                        else {

                            Dialog dialog1 = new Dialog(Activity_Creation_CR.this);
                            dialog1.setContentView(R.layout.dialog_box_custom);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                dialog1.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                            }

                            dialog1.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            dialog1.getWindow().getAttributes().windowAnimations = R.style.animation;
                            dialog1.setCancelable(false);
                            Button ok1 = dialog1.findViewById((R.id.Btn_dialog_ok));
                            ok1.setText("Oui");
                            Button cancel1 = dialog1.findViewById(R.id.Btn_dialog_cancel);
                            cancel1.setText("Non");
                            TextView Title1 = dialog1.findViewById(R.id.Txt_Title_Alert);
                            Title1.setText("Checklist incomplète");
                            ImageView ic1 = dialog1.findViewById(R.id.Ic_alrt_dialog);
                            ic1.setImageDrawable(getDrawable(R.drawable.ic_warning_orange));
                            TextView msg1 = dialog1.findViewById(R.id.Txt_Msg_alert_dialog);
                            msg1.setText("ATTENTION !! La Checklist est incomplète. Voulez-vous tout de même générer le compte-rendu ?");

                            ok1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog1.dismiss();

                                    Dialog dialog2 = new Dialog(Activity_Creation_CR.this);
                                    dialog2.setContentView(R.layout.dialog_box_custom);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                        dialog2.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                                    }

                                    dialog2.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    dialog2.getWindow().getAttributes().windowAnimations = R.style.animation;
                                    dialog2.setCancelable(false);
                                    Button ok2 = dialog2.findViewById((R.id.Btn_dialog_ok));
                                    ok2.setText("Oui");
                                    Button cancel2 = dialog2.findViewById(R.id.Btn_dialog_cancel);
                                    cancel2.setText("Non");
                                    TextView Title2 = dialog2.findViewById(R.id.Txt_Title_Alert);
                                    Title2.setText("Checklist incomplète");
                                    ImageView ic2 = dialog2.findViewById(R.id.Ic_alrt_dialog);
                                    ic2.setImageDrawable(getDrawable(R.drawable.ic_warning_orange));
                                    TextView msg2 = dialog2.findViewById(R.id.Txt_Msg_alert_dialog);
                                    msg2.setText("Êtes-vous sûr ? Vos collègues pourraient manquer d'informations avec un compte-rendu incomplet");

                                    ok2.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Msg_progress.setText("Génération du compte-rendu en cours.\nVeuillez patienter...\n" +
                                                    "Vous pouvez éteindre l'écran de votre téléphone mais veillez à ne pas quitter l'application.");
                                            progressdialog.setCanceledOnTouchOutside(false);
                                            progressdialog.show();
                                            Genere_CR();
                                            dialog2.dismiss();
                                        }
                                    });

                                    cancel2.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog2.dismiss();
                                        }
                                    });

                                    dialog2.show();
                                }
                            });

                            cancel1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog1.dismiss();
                                }
                            });

                            dialog1.show();
                        }
                    }
                }
            }
        });

        //*******************************************************ECOUTE ARRAYLIST <STRING>*********************************************


        pagerAgentViewModel.getArrayListContainerActivity().observe(this, new Observer<ArrayList<String>>() {
            @Override
            public void onChanged(ArrayList<String> strings) {
                //En provenance de Fragment Infos, mis à jour des badges de notifs, juste pour verif
                if (strings.get(0).matches("POUR_VERIF")) {
                    champs_restants_infos = Integer.parseInt(strings.get(1));
                    //Si il n'y a pas de champs vides dans le fragment FragmentInfo
                    badgeDrawableInfos.setNumber(champs_restants_infos);
                    if (champs_restants_infos == 0) {
                        badgeDrawableInfos.setVisible(false);
                    } else {
                        badgeDrawableInfos.setVisible(true);
                    }

                    //Envoie une demande à Fragment Checklist pour recevoir les datas
                    pagerAgentViewModel.sendMessageToChecklist("CHECK_INFO_RECU_SEND_DATA");

                }
                //En provenance de Fragment Infos, mis à jour des badges de notifs, pour verif et générer derrière
                if (strings.get(0).matches("POUR_GENERE")) {
                    champs_restants_infos = Integer.parseInt(strings.get(1));
                    //Si il n'y a pas de champs vides dans le fragment FragmentInfo
                    badgeDrawableInfos.setNumber(champs_restants_infos);
                    if (champs_restants_infos == 0) {
                        badgeDrawableInfos.setVisible(false);
                    } else {
                        badgeDrawableInfos.setVisible(true);
                    }

                    // Recupération des données pour mettre en forme le PDF et le txt

                    FILENAMETXT = strings.get(3);
                    FILENAMEPDF = strings.get(4);
                    str_contenuCRTXT = strings.get(5);
                    client_cr = strings.get(6);
                    codesite_cr = strings.get(7);
                    typesite_cr = "Type de site : " + strings.get(8);
                    projet_cr = strings.get(9);
                    if (strings.get(10).toLowerCase().matches("Neuf")) {
                        siteneuf_cr = "Site neuf";
                    } else if (strings.get(10).toLowerCase().matches("Existant")) {
                        siteneuf_cr = "Site existant";
                    }

                    nomsite_cr = strings.get(11);
                    adresse_cr = strings.get(12);
                    code_postal_cr = strings.get(13);
                    ville_cr = strings.get(14);
                    if (strings.get(15).matches("bool_bytel")) {
                        bool_bytel_from_infos = true;
                    } else if (strings.get(15).matches("bool_orange")) {
                        bool_orange_from_infos = true;
                    }else if (strings.get(15).matches("bool_free")) {
                        bool_free_from_infos = true;
                    }
                    vtdate_cr = strings.get(16);
                    str_commentaire = strings.get(17);
                    str_phase=strings.get(18);
                    coordonnes= strings.get(19);

                    //Envoie une message à Fragment Checklist pour demander d'envoyer les données
                    pagerAgentViewModel.sendMessageToChecklist("CHECK_INFO_RECU_SEND_DATA_POUR_GENERE");

                }
            }
        });

        //*******************************************************ECOUTE STRING*********************************************

        pagerAgentViewModel.getMessageContainerActivity().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                //Si il s'agit d'un remplacement de compte-rendu et non d'uen création initiale
                if (s.matches("replaceCR")) {
                    String PathCR = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString() + "/" + FOLDERNAME;
                    boolean supprfile = false;
                    try {
                        supprfile = Suppression_CR_def(PathCR);
                    } catch (Exception e) {
                    }
                    if (supprfile) {
                        SavefichierTxt();
                        CreatePDF();
                        //Signale que la creation du PDF de rempalcement s'est bien déroulée
                        pagerAgentViewModel.sendMessageToChecklist("files_ok");
                    } else {
                        //Signale une erreur lors de la suppression du PDF
                        pagerAgentViewModel.sendMessageToChecklist("error");
                    }

                }
            }
        });


        //Récupération du contexte de la VT
        Recuperer_donnees();

        //----------------------------------------Remplissage de l'ArrayList contenant toutes les lignes du fichier txt (checklist correspondante au projet------------------------------------------------

        //Selection opérateur
        Txt_ArrayList_allElements = new ArrayList<>();
        if (bool_orange) {
            operateur = "Orange";
        } else if (bool_bytel) {
            operateur = "Bouygues Télécom";
        }
        else if (bool_free) {
            operateur = "Free Mobile";

        }

        //Selection neuf ou existant
        if (bool_Site_neuf) {
            neuf_ou_existant = "neuf";
        } else {
            neuf_ou_existant = "existant";
        }

        //Selection typo du site
        if (str_Type_site.contains("Pylône")) {
            typologie = "pylone";
        } else if (str_Type_site.contains("Terrasse")) {
            typologie = "terrasse";
        } else {
            typologie = "autretypo";
        }
        String operateurfil;
        if (operateur == "Bouygues Télécom") {
            operateurfil = "Bytel";
        } else if(operateur=="Orange") {
            operateurfil = "Orange";
        }
        else{
            operateurfil = "Free";
        }

        //Récupération de la checklist correspondante
        String path_fichier_checklist_documents=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString() + "/ACSIT_CHECKLISTS";
        File dossier_checklist=new File(path_fichier_checklist_documents);

        //Si le dossier des Checklists personnalisé existe
        if(dossier_checklist.exists()) {
            Log.i("trypouet", "dossier existe");
            String Path_Checklist = path_fichier_checklist_documents + "/" + operateurfil + "_" + neuf_ou_existant + "_" + typologie + ".txt";
            File File_Checklist = new File(Path_Checklist);

            //Si le fichier de la checklist personnalisée existe
            if (File_Checklist.exists()) {
                Log.i("trypouet", "fichier existe");
                try {
                    displayMsg("Vous utilisez une Checklist personnalisée");
                    Scanner s = new Scanner(new File(Path_Checklist)).useDelimiter(System.lineSeparator());

                    //Tant que le fichier comporte d'autres lignes
                    while (s.hasNextLine()) {
                        //On ajoute la ligne dans la liste
                        Txt_ArrayList_allElements.add(s.nextLine());
                    }
                    s.close();
                } catch (Exception E) {

                    //Si il y a un problème avec la lecture du fichier personnalisé, on prend la checklist de base
                    displayMsg("Vous utilisez la Checklist de base");
                    Txt_ArrayList_allElements.clear();

                    String path_fichier_checklist = operateurfil + "_" + neuf_ou_existant + "_" + typologie + ".txt";
                    path_fichier_checklist.trim();
                    try {
                        InputStream data = this.getAssets().open(path_fichier_checklist);
                        InputStreamReader isr = new InputStreamReader(data, "ISO-8859-1");
                        BufferedReader reader = new BufferedReader(isr);
                        //Remplissage d'uen array list contenant la checklist
                        while (reader.ready()) {
                            Txt_ArrayList_allElements.add(reader.readLine());
                        }
                    } catch (Exception e) {
                    }
                }
            } else {

                displayMsg("Vous utilisez la checklist de base");
                String path_fichier_checklist = operateurfil + "_" + neuf_ou_existant + "_" + typologie + ".txt";
                path_fichier_checklist.trim();
                try {
                    InputStream data = this.getAssets().open(path_fichier_checklist);
                    InputStreamReader isr = new InputStreamReader(data, "ISO-8859-1");
                    BufferedReader reader = new BufferedReader(isr);
                    //Remplissage d'uen array list contenant la checklist
                    while (reader.ready()) {
                        Txt_ArrayList_allElements.add(reader.readLine());
                    }
                } catch (Exception e) {
                }
            }
        }
        else{
            //Récupération de la checklist correspondante
            try{
                dossier_checklist.mkdir();
            } catch (Exception e) {
                e.printStackTrace();
            }

            String path_fichier_checklist = operateurfil + "_" + neuf_ou_existant + "_" + typologie + ".txt";
            path_fichier_checklist.trim();
            try {
                InputStream data = this.getAssets().open(path_fichier_checklist);
                InputStreamReader isr = new InputStreamReader(data, "ISO-8859-1");
                BufferedReader reader = new BufferedReader(isr);
                //Remplissage d'uen array list contenant la checklist
                while (reader.ready()) {
                    Txt_ArrayList_allElements.add(reader.readLine());
                }
            } catch (Exception e) {
            }
        }


            //-----------------------------------------------------------Remplissage des différentes ArrayList en splittant les lignes du fichier-------------------------------------------
        for (int i = 0; i < Txt_ArrayList_allElements.size(); i++) {
            String[] resultSplit = Txt_ArrayList_allElements.get(i).split("/");
            int size = resultSplit.length;
            Txt_ArrayList_NomCategories.add(resultSplit[0]);
            Txt_ArrayList_NomElements.add(resultSplit[1]);
            Txt_ArrayList_DescriptifElements.add(resultSplit[2]);
            Txt_ArrayList_Commentaires.add(resultSplit[3]);
            Txt_ArrayList_Photos.add((resultSplit[4]));

            //Si le split est plus grand, c'est que l'élément est sépcifique a certaines phases de projet
            if (size > 5) {
                String[] tab = new String[resultSplit.length - 5];
                int pos = 0;

                for (int j = 5; j < resultSplit.length; j++) {
                    tab[pos] = resultSplit[j];
                    pos++;
                }
                //On ajoute le tablea udes phases de cet élément dans une arrayList
                Txt_ArrayList_Phases.add(tab);

            } else {
                //Si l'élément n'est pas spécifique à une phase, on ajoute "commun" dans la liste des phases de cet élément
                String[] tab = new String[]{"commun"};
                Txt_ArrayList_Phases.add(tab);
            }
        }

        //Initilisation du Viewpager
        viewpager = findViewById(R.id.viewpager);

        //Récupération de l'intent de création
        Intent intent1 = getIntent();
        //Si l'intent existe, il s'agit d'une modification d'un CR existant
        if (intent1 != null) {
            //On recupère sa date de création et son état de dépôt
            if (intent1.hasExtra("Data_CR")) {
                date=intent1.getStringExtra("Date_CR");
                etat_depot=intent1.getStringExtra("Etat_Depot_CR");
            }
            else{
                date=new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                etat_depot="DEPOT_NOK";
            }
        }
        //Sinon, c'est une création et la date à utiliser est la date du jour
        else{
            date=new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        }

        //Initilisation du dossier de sauvegarde du CR
        FOLDERNAME = "Comptes-rendus de VT" + "/" + operateur + "/" + str_Nom_projet + "/" + str_code_site + "_" + str_nom_site+"_"+date;
        FolderPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString() + "/" + FOLDERNAME;
        Create_folders();

        //Récupération de l'intent
        Intent intent = getIntent();

        //Cas d'une modification
        if (intent.hasExtra("Data_CR")) {
            //Initilisation du ViewPager
            viewPagerAdaptater = new ViewPagerAdaptater(getSupportFragmentManager(), getLifecycle(), Data_CR, Txt_ArrayList_NomCategories, Txt_ArrayList_NomElements, Txt_ArrayList_DescriptifElements,
                    Txt_ArrayList_Commentaires, Txt_ArrayList_Photos, FolderPath, str_nom_site, Data_CR_Check, str_code_site, str_phase, str_operateur, Txt_ArrayList_Phases,date);
        }
        //Cas d'une création
        else {
            //Initilisation du Viewpager
            viewPagerAdaptater = new ViewPagerAdaptater(getSupportFragmentManager(), getLifecycle(), str_Nom_projet, str_code_site, str_nom_site, bool_Site_neuf, str_Type_site, bool_orange, bool_bytel, bool_free,
                    Txt_ArrayList_NomCategories, Txt_ArrayList_NomElements, Txt_ArrayList_DescriptifElements, Txt_ArrayList_Commentaires, Txt_ArrayList_Photos, FolderPath, str_phase, str_operateur, Txt_ArrayList_Phases,date);
            Log.d("trytest", "pas datacr");
        }

        //Viewpager avec défilement horizontal
        viewpager.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        viewpager.setPageTransformer(new ZoomOutPageTransformer());
        //Application des paramètres du viewpager
        viewpager.setAdapter(viewPagerAdaptater);
        //L'activité se lance avec le fragment à la position 1 (Création du CR)
        viewpager.setCurrentItem(1);

        //Création du TabLayout (menu de navigation en haut de l'écran)
        tabLayout = findViewById(R.id.tabLayout);
        Initialisation_TabLayout();

        //Initialisation de la Progress Dialog pour la génération du CR
        progressdialog = new Dialog(Activity_Creation_CR.this);
        progressdialog.setContentView(R.layout.dialog_progress);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressdialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
        }
        progressdialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        progressdialog.getWindow().getAttributes().windowAnimations = R.style.animation;
        Msg_progress = progressdialog.findViewById(R.id.Txt_dep_en_cours);
        Msg_progress.setText("Dépôt des fichiers en cours...");
        progressdialog.setCanceledOnTouchOutside(false);

        //Appellé lorsque la progress dialog se ferme, la génération du CR est terminée
        progressdialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDismiss(DialogInterface dialog) {
                displayMsg("Le compte-rendu a bien été généré");
                String Title = "Compte-rendu généré";
                String Message = "Le compte-rendu du site " + codesite_cr + "_" + nomsite_cr + " a été généré avec succès.";
                int ic = R.drawable.ic_check;
                //Envooie d'une notification pour informer que le CR a bien été généré
                sendOnChannel1(Title, Message, Activity_Creation_CR.this, operateur, ic);
                Activity_Creation_CR.this.finish();
            }
        });

    }

//#########################################################################################################################################################
//--------------------------------------------------------------------FIN METHODE ONCREATE------------------------------------------------------
//#########################################################################################################################################################

    //Fonction pour vérifier les permissions
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    //Fonction pour générer le compte-rendu en PDF
    public void Genere_CR() {
        //On appel la tâche permettant de créer un dossier dans le dossier du projet
        new Thread() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            public void run() {
                Categorie_Unique.add(dataChecklist.get(0).get("Title"));
                for (int i = 1; i < dataChecklist.size(); i++) {
                    for (int j = 0; j < Categorie_Unique.size(); j++) {
                        if (dataChecklist.get(i).get("Title").matches(Categorie_Unique.get(j))) {
                            j = Categorie_Unique.size();
                        } else if (j == Categorie_Unique.size() - 1) {
                            Categorie_Unique.add(dataChecklist.get(i).get("Title"));
                        }
                    }
                }
                Suppression_CR_def(FolderPath);

                //Mise en forme du fichier txt
                String Txt_Checklist = MiseEnFormeChecklistTxt(dataChecklist, Categorie_Unique);
                str_contenuCRTXT = str_contenuCRTXT + Txt_Checklist;

                //Sauvegarde du fichier txt
                SavefichierTxt();

                //Initilisation du dossier photos
                FolderPathImage = FolderPath + "/Photos";

                File f2 = new File(FolderPathImage);
                File[] fichiers = f2.listFiles();
                // Si le répertoire n'est pas vide
                if (fichiers != null) {
                    // On ajoute les noms dans une array liste
                    for (File f : fichiers) {
                        Image_Path_List.add(f.toString());
                    }
                }

                //Initilisation du dossier croquis
                FolderPathCroquis = FolderPath + "/Croquis";

                File f3 = new File(FolderPathCroquis);
                File[] fichiers1 = f3.listFiles();
                // Si le répertoire n'est pas vide
                if (fichiers1 != null) {
                    // On ajoute les noms dans une array liste
                    for (File f : fichiers1) {
                        Croquis_Path_List.add(f.toString());
                    }
                }

                sendOnChannel3(codesite_cr +" : génération du compte-rendu","Le compte rendu du site " + codesite_cr + " est en cours de génération", Activity_Creation_CR.this, operateur);
                  //Génération et sauvegarde du PDF sur le téléphone
                CreatePDF();

                //Retour à l'accueil à la fin de la création du CR
                progressdialog.dismiss();
                notificationManager.cancel(3);
                Intent myIntent = new Intent(Activity_Creation_CR.this, Activity_Accueil.class);
                finish();
                startActivity(myIntent);
            }
        }.start();


    }

    //Création des dossiers de comptes-rendus si ils n'existent pas
    public void Create_folders() {
        File Docs = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());
        if (!Docs.exists()) {
            try {
                Docs.mkdir();
            } catch (Exception e) {
            }
        }

        File CR_folders = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString() + "/Comptes-rendus de VT");
        if (!CR_folders.exists()) {
            try {
                CR_folders.mkdir();
            } catch (Exception e) {
            }
        }
        File OP_folders = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString() + "/Comptes-rendus de VT/" + operateur);
        if (!OP_folders.exists()) {
            try {
                OP_folders.mkdir();
            } catch (Exception e) {
            }
        }
        File PRO_folders = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString() + "/Comptes-rendus de VT/" + operateur + "/" + str_Nom_projet);
        if (!PRO_folders.exists()) {
            try {
                PRO_folders.mkdir();
            } catch (Exception e) {
            }
        }
        File Site_folders = new File(FolderPath);
        if (!Site_folders.exists()) {
            try {
                Site_folders.mkdir();
            } catch (Exception e) {
            }
        }

        File Photo_folders = new File(FolderPath + "/Photos");
        if (!Photo_folders.exists()) {
            try {
                Photo_folders.mkdir();
            } catch (Exception e) {
            }
        }
    }

    //Fonction pour supprimer un CR du téléphone
    public void Suppression_CR(String pathCR) {
        File path = new File(pathCR);
        //Passe dans la boucle si le dossier en question existe bien
        if (path.exists()) {
            File[] files1 = path.listFiles();
            for (int i = 0; i < files1.length; i++) {
                if (files1[i].getName().toLowerCase().contains(".pdf") || files1[i].getName().toLowerCase().contains(".txt")) {
                    files1[i].delete();
                }
            }

        }
    }

    //Fonction poru supprimer tous les fichiers d'un dossier passé en paramètre
    public void delete_all(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                delete_all(child);

        fileOrDirectory.delete();
    }


    //Appellée lors de l'appui sur le bouton retour Android
    public void onBackPressed() {

        //Boite de dialogue pour alerter sur retour et perte de données
        Dialog dialog = new Dialog(Activity_Creation_CR.this);
        dialog.setContentView(R.layout.dialog_box_custom);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
        }

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
        dialog.setCancelable(false);
        Button ok = dialog.findViewById((R.id.Btn_dialog_ok));
        ok.setText("Oui");
        Button cancel = dialog.findViewById(R.id.Btn_dialog_cancel);
        cancel.setText("Non");
        TextView Title = dialog.findViewById(R.id.Txt_Title_Alert);
        Title.setText("Annuler le compte-rendu ?");
        ImageView ic = dialog.findViewById(R.id.Ic_alrt_dialog);
        ic.setImageDrawable(getDrawable(R.drawable.ic_warning_orange));
        TextView msg = dialog.findViewById(R.id.Txt_Msg_alert_dialog);
        msg.setText("Voulez-vous abandonner la VT en cours ? ATTENTION : TOUTES LES DONNEES SAISIES SERONT PERDUES");

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File doss = new File(FolderPath);
                //SI on est modif on ne uspprime pas
                Intent intent = getIntent();
                if (intent != null) {
                    if (intent.hasExtra("Data_CR")) {
                        //Si il s'agit d'une modif de CR existant, on ne touche pas aux éléments existants
                        Intent intent2 = new Intent(Activity_Creation_CR.this, Activity_Accueil.class);
                        startActivity(intent2);
                        finish();
                        dialog.dismiss();
                    }
                    else {
                        //Si il s'agit d'une création de CR, on supprime les fichiers que l'on vient de générer
                        delete_all(doss);
                        Intent intent2 = new Intent(Activity_Creation_CR.this, Activity_Accueil.class);
                        startActivity(intent2);
                        finish();
                        dialog.dismiss();
                    }
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    //Mise en forme de la partie Checklist du fichier txt
    private String MiseEnFormeChecklistTxt(ArrayList<HashMap<String, String>> dataChecklist, ArrayList<String> Categorie_Unique) {
        String str_checklist = line;
        //On parcours chacune des catégorie (pas de doublons)
        for (int i = 0; i < Categorie_Unique.size(); i++) {
            //On parcourt le la liste des éléments
            for (int j = 0; j < dataChecklist.size(); j++) {
                //Si un élément correpond à la catégorie en cours
                if (dataChecklist.get(j).get("Title").matches(Categorie_Unique.get(i))) {
                    //On inscrit l'élément, son commentaire et l'état de la photo
                    String commentaire =   dataChecklist.get(j).get("Commentaire").replace("\n","%n%");
                    str_checklist = str_checklist + dataChecklist.get(j).get("Title") + " / " + dataChecklist.get(j).get("Nom") + " / " + commentaire + " / Photos / " + dataChecklist.get(j).get("Photos") + line;
                } else {

                }
            }
        }
        str_checklist=str_checklist+etat_depot;

        return str_checklist;

    }

    //Sauvegarde du fichier Txt
    private void SavefichierTxt() {
        if (StorageUtils.isExternalStorageWritable()) {
            Log.i("tryname", FILENAMETXT + "  " + FOLDERNAME);
            StorageUtils.setTextInStorage(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), this, FILENAMETXT, FOLDERNAME, str_contenuCRTXT);
        } else {
            displayMsg("Création de fichier impossible");
        }
    }

    //Fonction supprimant les fichiers txt et pdf des CR existants
    public boolean Suppression_CR_def(String pathCR) {
        File path = new File(pathCR);
        //Passe dans la boucle si le dossier en question existe bien
        if (path.exists()) {
            File[] files = path.listFiles();
            if (files == null) {
                //Sort de la boucle si il n'y a plus de fichier à supprimer
                return true;
            }
            //Supprime les fichiers un par un
            for (File file : files) {
                if (file.isDirectory()) {
                    Suppression_CR(file.toString());
                } else {
                    if (file.getPath().contains(".txt") || file.getPath().contains(".pdf")) {
                        file.delete();
                    }

                }
            }
        }
        return (path.delete());
    }

    //Appellée à chaque fois que l'activité se rafraichit (dans le cadre d'une prise de photo par exemple)
    @Override
    protected void onResume() {
        super.onResume();
        if(refresh!=0) {
            //Récupération de l'espace de stockage disponible
            StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
            long bytesAvailable;
            if (android.os.Build.VERSION.SDK_INT >=
                    android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
                bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
            } else {
                bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
            }
            long megAvailable = bytesAvailable / (1024 * 1024);

            //**************************************************************Gestion du stockage**************************************************************

            //Stockage critique, moins de 50Mo disponible
            if (megAvailable < 50) {

                Dialog dialog = new Dialog(Activity_Creation_CR.this);
                dialog.setContentView(R.layout.dialog_box_custom);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                }

                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
                dialog.setCancelable(false);
                Button ok = dialog.findViewById((R.id.Btn_dialog_ok));
                ok.setText("OK");
                Button cancel = dialog.findViewById(R.id.Btn_dialog_cancel);
                cancel.setVisibility(View.INVISIBLE);
                TextView Title = dialog.findViewById(R.id.Txt_Title_Alert);
                Title.setText("Espace de stockage saturé !");
                ImageView ic = dialog.findViewById(R.id.Ic_alrt_dialog);
                ic.setImageDrawable(getDrawable(R.drawable.ic_warning_orange));
                TextView msg = dialog.findViewById(R.id.Txt_Msg_alert_dialog);
                msg.setText("L'espace de stockage de votre appareil est saturé. Vous n'avez que " + String.valueOf(megAvailable) + " Mo disponible.\n" +
                        "Le fichier PDF va être généré tel quel. Veuillez libérer de la place sur votre appareil avant de modifier ce compte-rendu.");
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        pagerAgentViewModel.sendMessageToChecklist("STORAGE_FULL");
                    }
                });

                dialog.show();
            }
        }
        refresh++;
    }

    //Fonction pour générer le fichier PDF, la mise en forme se fait ici
    public void CreatePDF() {
        //Chemin du fichier PDF
        String path = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString()) + "/" + FOLDERNAME + "/" + FILENAMEPDF;
        File file = new File(path);
        int nbpages = 1;
        //Créer le fichier PDF si il n'existe pas
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //Format A4
        com.itextpdf.text.Document document = new com.itextpdf.text.Document(com.itextpdf.text.PageSize.A4);
        document.addCreationDate();
        document.addTitle("CRVT_"+str_code_site);
        PdfWriter writer = null;

        try {
            writer = PdfWriter.getInstance(document, new FileOutputStream(path));
            writer.createXmpMetadata();
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
        document.open();
        document.setMargins(40,40,40,40);
        //Mise en forme du PDF ici
        try {
            Image sade = init_image("logo_sade.png", Element.ALIGN_LEFT, 100, 100, 10);
            //Tableau avec les logos
            PdfPTable table = new PdfPTable(2);
            table.setWidthPercentage(100);
            PdfPCell c1 = new PdfPCell(sade);
            c1.setHorizontalAlignment(Element.ALIGN_LEFT);
            c1.setBorderColor(BaseColor.WHITE);
            table.addCell(c1);
            if (bool_orange_from_infos) {
                Image operateur = init_image("logo_orange.png", Element.ALIGN_RIGHT, 45, 45, 10);
                c1 = new PdfPCell(operateur);
                c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                c1.setBorderColor(BaseColor.WHITE);
                table.addCell(c1);
            } else if (bool_bytel_from_infos) {
                Image operateur = init_image("logo_bytel.png", Element.ALIGN_RIGHT, 120, 120, 10);
                c1 = new PdfPCell(operateur);
                c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                c1.setBorderColor(BaseColor.WHITE);
                table.addCell(c1);
            }
            else if (bool_free_from_infos) {
                Image operateur = init_image("logo_free.png", Element.ALIGN_RIGHT, 120, 120, 10);
                c1 = new PdfPCell(operateur);
                c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                c1.setBorderColor(BaseColor.WHITE);
                table.addCell(c1);
            }
            document.add(table);
            document.add(new Paragraph(line));

            //Titre
            PdfPTable Tab_CR_Titre = new PdfPTable(1);
            Tab_CR_Titre.setWidthPercentage(100);
            c1 = new PdfPCell(new Phrase("Compte-rendu de visite technique", Titre1));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setBorderColor(BaseColor.WHITE);
            Tab_CR_Titre.addCell(c1);
            document.add(Tab_CR_Titre);
            document.add(new Paragraph(line));
            document.add(new Paragraph(line));

            //Projet
            PdfPTable Tab_projet = new PdfPTable(1);
            Tab_projet.setWidthPercentage(100);
            c1 = new PdfPCell(new Phrase(str_Nom_projet + " - " + str_phase, Titre2));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setBorderColor(BaseColor.WHITE);
            Tab_projet.addCell(c1);
            document.add(Tab_projet);
            document.add(new Paragraph(line));

            //Code Site et nom de site
            PdfPTable Tab_site = new PdfPTable(1);
            Tab_site.setWidthPercentage(100);
            if (codesite_cr.trim().matches("")) {
                c1 = new PdfPCell(new Phrase(nomsite_cr, Titre2));
            } else if (nomsite_cr.trim().matches("")) {
                c1 = new PdfPCell(new Phrase(codesite_cr, Titre2));
            } else if (!nomsite_cr.trim().matches("") && !codesite_cr.trim().matches("")) {
                c1 = new PdfPCell(new Phrase(codesite_cr + " - " + nomsite_cr, Titre2));
            }
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            c1.setBorderColor(BaseColor.WHITE);
            Tab_site.addCell(c1);
            document.add(Tab_site);
            document.add(new Paragraph(line));
            document.add(new Paragraph(line));

            String img_page_garde = null;

            for (int i = 0; i < Image_Path_List.size(); i++) {
                if (Image_Path_List.get(i).contains("Photomontages_Vue générale")) {
                    img_page_garde = Image_Path_List.get(i);
                }
            }

            if (img_page_garde != null) {
                Image vue_gen = init_image_check(img_page_garde, Element.ALIGN_CENTER, 270, 450, 30);
                PdfPTable image_tab = new PdfPTable(1);
                image_tab.setWidthPercentage(100);
                PdfPCell c2 = new PdfPCell(vue_gen);
                c2.setHorizontalAlignment(Element.ALIGN_CENTER);
                c2.setBorderColor(BaseColor.WHITE);
                image_tab.addCell(c2);
                document.add(image_tab);
            } else {
                Paragraph txt_erreur = new Paragraph("Vue générale du site non photographiée", INVALIDE);
                PdfPTable image_tab = new PdfPTable(1);
                image_tab.setWidthPercentage(100);
                PdfPCell c2 = new PdfPCell(txt_erreur);
                c2.setHorizontalAlignment(Element.ALIGN_CENTER);
                c2.setBorderColor(BaseColor.WHITE);
                image_tab.addCell(c2);
                document.add(image_tab);
            }

            //Date VT :
            document.add(new Paragraph(line));
            document.add(new Paragraph(line));
            document.add(new Paragraph(vtdate_cr, Souligne));
            document.add(new Paragraph(line));

            //Lieu VT :
            if (!adresse_cr.trim().matches("")) {
                document.add(new Paragraph("Adresse : " + adresse_cr));
            } else {
                Chunk adresse = new Chunk("Adresse : ");
                Chunk invalide = new Chunk("ELEMENT NON RENSEIGNE", INVALIDE);
                Paragraph adresse1 = new Paragraph();
                adresse1.add(adresse);
                adresse1.add(invalide);
                document.add(adresse1);
            }


            if (!code_postal_cr.trim().matches("")) {
                document.add(new Paragraph("Code postal : " + code_postal_cr));
            } else {
                Chunk cp = new Chunk("Code postal : ");
                Chunk cpinvalide = new Chunk("ELEMENT NON RENSEIGNE", INVALIDE);
                Paragraph codepostal = new Paragraph();
                codepostal.add(cp);
                codepostal.add(cpinvalide);
                document.add(codepostal);
            }


            if (!ville_cr.trim().matches("")) {
                document.add(new Paragraph("Ville : " + ville_cr));
            } else {
                Chunk vi = new Chunk("Ville : ");
                Chunk viinvalide = new Chunk("ELEMENT NON RENSEIGNE", INVALIDE);
                Paragraph villeinvalide = new Paragraph();
                villeinvalide.add(vi);
                villeinvalide.add(viinvalide);
                document.add(villeinvalide);
            }

            if (!coordonnes.trim().matches("")) {
                document.add(new Paragraph("Coordonnées GPS : " + coordonnes));
            } else {
                Chunk coor = new Chunk("Coordonnées GPS : ");
                Chunk coorinvalide = new Chunk("ELEMENT NON RENSEIGNE", INVALIDE);
                Paragraph coorin = new Paragraph();
                coorin.add(coor);
                coorin.add(coorinvalide);
                document.add(coorin);
            }


            document.add(new Paragraph(line));

            //Paragraphe infos
            document.add(new Paragraph(typesite_cr));
            document.add(new Paragraph(siteneuf_cr, Souligne));

            //Saut de page
            document.newPage();
            writer.setPageEvent(new MyFooter(codesite_cr, nomsite_cr));

            document.add(new Paragraph("Commentaires généraux sur la VT :", Categorie));
            document.add(new Paragraph(line));
            document.add(new Paragraph(str_commentaire));
            document.newPage();


            //--------------------------------------------------------------------------AFFICHAGE ELEMENTS CHECKLIST--------------------------------------------------------

            //On parcourt la liste des catégories
            for (int i = 0; i < Categorie_Unique.size(); i++) {
                //Nom de la catégorie
                document.add(new Paragraph(Categorie_Unique.get(i), Categorie));
                document.add(new Paragraph(line));

                //On parcours la liste des élements de la catégorie
                for (int j = 0; j < dataChecklist.size(); j++) {
                    if (dataChecklist.get(j).get("Title").matches(Categorie_Unique.get(i))) {

                        //Booléen pour savoir si il y a un commentaire et des photos à afficher dans l'élément courant
                        Boolean CommtoPrint=true;
                        Boolean ImgtoPrint=true;

                        //Si un commentaire n'est pas demandé
                        if(dataChecklist.get(j).get("Commentaire").trim().contains("COMMENTARY NOT REQUIRED")){
                            CommtoPrint=false;
                        }
                        //Sinon si si un commentaire est demandé mais non saisi
                        else if(dataChecklist.get(j).get("Commentaire").trim().contains("COMMENTARY REQUIRED")){
                            CommtoPrint=false;
                        }
                        else
                        {
                            CommtoPrint=true;
                        }

                        ArrayList<String> Img_elem = new ArrayList<>();

                        //Si la prise de photo n'étatit pas demandée
                        if (dataChecklist.get(j).get("Photos").contains("IMAGE NOT REQUIRED")) {
                            ImgtoPrint=false;
                        }
                        //Si la photo était requise mais non prise
                        else if (dataChecklist.get(j).get("Photos").contains("IMAGE REQUIRED"))
                        {
                            ImgtoPrint=false;
                        }

                        else
                        {
                            ImgtoPrint=true;
                        }

                        //Si il n'y a rien à afficher dans l'élément courant, on saute une ligne
                        if(!CommtoPrint && !ImgtoPrint) {
                            document.add(new Paragraph(line));
                        }
                        //Sinon on affiche le nom de l'élément
                        else{
                            //Nom de l'élément
                            document.add(new Paragraph(dataChecklist.get(j).get("Nom"), Nom_element));
                            document.add(new Paragraph(line));
                        }

                        if(CommtoPrint){
                            document.add(new Paragraph(dataChecklist.get(j).get("Commentaire")));
                            document.add(new Paragraph(line));
                        }
                        if(ImgtoPrint){
                            //On parcourt les photos du site et on liste celles de l'élément courant à afficher
                            for (int k = 0; k < Image_Path_List.size(); k++) {
                                if (Image_Path_List.get(k).contains(dataChecklist.get(j).get("Title")) && Image_Path_List.get(k).contains(dataChecklist.get(j).get("Nom"))) {
                                    Img_elem.add(Image_Path_List.get(k));
                                }
                            }

                            //Affichage des photos de l'élément courant
                            for (int l = 0; l < Img_elem.size(); l = l + 2) {
                                //Si il s'agit de la dernière ligne d'un tableau et nombre de photos impaire, il n'y a qu'un image à afficher
                                if (l + 1 >= Img_elem.size()) {
                                    PdfPTable img_tab = new PdfPTable(2);
                                    img_tab.setWidthPercentage(100);
                                    Image img1 = init_image_check(Img_elem.get(l), Element.ALIGN_CENTER, 200, 333, 30);
                                    PdfPCell c6 = new PdfPCell(img1);
                                    c6.setHorizontalAlignment(Element.ALIGN_CENTER);
                                    c6.setBorderColor(BaseColor.WHITE);
                                    img_tab.addCell(c6);

                                    PdfPCell c7 = new PdfPCell();
                                    c7.setHorizontalAlignment(Element.ALIGN_CENTER);
                                    c7.setBorderColor(BaseColor.WHITE);
                                    img_tab.addCell(c7);

                                    img_tab.setSpacingBefore(15);
                                    document.add(img_tab);
                                    document.add(new Paragraph(line));
                                } else {
                                    PdfPTable img_tab = new PdfPTable(2);
                                    img_tab.setWidthPercentage(100);
                                    Image img1 = init_image_check(Img_elem.get(l), Element.ALIGN_CENTER, 200, 333, 30);
                                    PdfPCell c6 = new PdfPCell(img1);
                                    c6.setHorizontalAlignment(Element.ALIGN_CENTER);
                                    c6.setBorderColor(BaseColor.WHITE);
                                    img_tab.addCell(c6);

                                    Image img2 = init_image_check(Img_elem.get(l + 1), Element.ALIGN_CENTER, 200, 333, 30);
                                    PdfPCell c7 = new PdfPCell(img2);
                                    c7.setHorizontalAlignment(Element.ALIGN_CENTER);
                                    c7.setBorderColor(BaseColor.WHITE);
                                    img_tab.addCell(c7);

                                    img_tab.setSpacingBefore(10);
                                    document.add(img_tab);
                                    document.add(new Paragraph(line));
                                }

                            }
                        }

                    }
                }
                document.newPage();
            }

            document.add(new Paragraph("Photos diverses :", Categorie));
            document.add(new Paragraph(line));
            ArrayList<String> Img_divers = new ArrayList<>();
            for (int k = 0; k < Image_Path_List.size(); k++) {
                if (Image_Path_List.get(k).contains("Divers")) {
                    Img_divers.add(Image_Path_List.get(k));
                }
            }

            for (int l = 0; l < Img_divers.size(); l = l + 2) {
                //Si il s'agit de la dernière ligne d'un tableau et nombre de photos impaire, il n'y a qu'un image à afficher
                if (l + 1 >= Img_divers.size()) {
                    PdfPTable img_tab = new PdfPTable(2);
                    img_tab.setWidthPercentage(100);
                    Image img1 = init_image_check(Img_divers.get(l), Element.ALIGN_CENTER, 200, 333, 10);
                    PdfPCell c6 = new PdfPCell(img1);
                    c6.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c6.setBorderColor(BaseColor.WHITE);
                    img_tab.addCell(c6);

                    PdfPCell c7 = new PdfPCell();
                    c7.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c7.setBorderColor(BaseColor.WHITE);
                    img_tab.addCell(c7);


                    document.add(img_tab);
                    document.add(new Paragraph(line));
                } else {
                    PdfPTable img_tab = new PdfPTable(2);
                    img_tab.setWidthPercentage(100);
                    Image img1 = init_image_check(Img_divers.get(l), Element.ALIGN_CENTER, 200, 333, 10);
                    PdfPCell c6 = new PdfPCell(img1);
                    c6.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c6.setBorderColor(BaseColor.WHITE);
                    img_tab.addCell(c6);

                    Image img2 = init_image_check(Img_divers.get(l + 1), Element.ALIGN_CENTER, 200, 333, 10);
                    PdfPCell c7 = new PdfPCell(img2);
                    c7.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c7.setBorderColor(BaseColor.WHITE);
                    img_tab.addCell(c7);


                    document.add(img_tab);
                    document.add(new Paragraph(line));
                }

            }

            //*********************************************************CROQUIS*********************************************************************************
            document.newPage();
            document.add(new Paragraph("Croquis :", Categorie));
            document.add(new Paragraph(line));
            for (int k = 0; k < Croquis_Path_List.size(); k++) {
                File croquis  = new File(Croquis_Path_List.get(k));

                String s = croquis.getName().replace(str_code_site+"_","");
                String s2=s.replace(".png","");

                document.add(new Paragraph(s2 +" : "));
                document.add(new Paragraph(line));

                Image croquis_img = init_croquis(Croquis_Path_List.get(k), Element.ALIGN_CENTER, 390, 650, 50);
                PdfPTable image_tab = new PdfPTable(1);
                image_tab.setWidthPercentage(100);
                PdfPCell c2 = new PdfPCell(croquis_img);
                c2.setHorizontalAlignment(Element.ALIGN_CENTER);
                c2.setBorderColor(BaseColor.WHITE);
                image_tab.addCell(c2);
                document.add(image_tab);

                if(k!=Croquis_Path_List.size()-1) {
                    document.newPage();
                }

            }


        } catch (DocumentException | IOException e) {
            e.printStackTrace();
            Log.d("trypdf", e.toString());
        }
        document.close();
    }

    //Fonction pour initialiser les images de la partie checklist du PDF
    public Image init_image_check(String Pathimage, int align, float width, float height, int quality) throws IOException, BadElementException {
        FileInputStream ims = new FileInputStream(Pathimage);
        bmp = BitmapFactory.decodeStream(ims);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, quality, stream);
        Image image = Image.getInstance(stream.toByteArray());
        if(image.getHeight()<image.getWidth()){
            image.setRotationDegrees(270);
        }

        image.scaleToFit(width, height);
        image.setAlignment(align);
        return image;
    }

    //Fonction pour initialiser les images du PDF
    public Image init_image(String nomimage, int align, float width, float height, int quality) throws IOException, BadElementException {
        InputStream ims = this.getResources().getAssets().open(nomimage);
        bmp = BitmapFactory.decodeStream(ims);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, quality, stream);
        Image image = Image.getInstance(stream.toByteArray());
        image.scaleToFit(width, height);
        image.setAlignment(align);
        return image;
    }

    public Image init_croquis(String Pathimage, int align, float width, float height, int quality) throws IOException, BadElementException {
        FileInputStream ims = new FileInputStream(Pathimage);
        bmp = BitmapFactory.decodeStream(ims);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, quality, stream);
        Image image = Image.getInstance(stream.toByteArray());
        image.scaleToFit(width, height);
        image.setAlignment(align);
        return image;
    }


    //Fonction pour récupérer les données reçus par l'actiité
    public void Recuperer_donnees() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("Operateur")) {
                str_operateur = intent.getStringExtra("Operateur");
            }
            if (intent.hasExtra("Code_site")) {
                str_code_site = intent.getStringExtra("Code_site");
            }
            if (intent.hasExtra("Nom_Phase")) {
                str_phase = intent.getStringExtra("Nom_Phase");
            }
            if (intent.hasExtra("Nom_Site")) {
                str_nom_site = intent.getStringExtra("Nom_Site");
            }

            if (intent.hasExtra("Orange")) {
                bool_orange = intent.getBooleanExtra("Orange", false);
                Log.i("trybool", "Orange : " + Boolean.toString(bool_orange));
            }

            if (intent.hasExtra("Bytel")) {
                bool_bytel = intent.getBooleanExtra("Bytel", false);
                Log.i("trybool", "Bytel : " + Boolean.toString(bool_bytel));
            }

            if (intent.hasExtra("Free")) {
                bool_free = intent.getBooleanExtra("Free", false);
                Log.i("trybool", "Free : " + Boolean.toString(bool_free));
            }

            if (intent.hasExtra("Site_neuf")) {
                bool_Site_neuf = intent.getBooleanExtra("Site_neuf", false);
            }

            if (intent.hasExtra("Nom_projet")) {
                str_Nom_projet = intent.getStringExtra("Nom_projet");
            }

            if (intent.hasExtra("Type_site")) {
                str_Type_site = intent.getStringExtra("Type_site");
            }
            if (intent.hasExtra("Data_CR")) {
                Data_CR = intent.getStringArrayListExtra("Data_CR");
                if (Data_CR.get(0).matches("Orange")) {
                    bool_orange = true;
                } else if (Data_CR.get(0).matches("Bouygues Télécom")) {
                    bool_bytel = true;
                }
                else if (Data_CR.get(0).matches("Free Mobile")) {
                    bool_free = true;
                }
                if (Data_CR.get(4).trim().matches("Site neuf")) {
                    bool_Site_neuf = true;
                } else if (Data_CR.get(4).matches("Site existant")) {
                    bool_Site_neuf = false;
                }
                str_Type_site = Data_CR.get(2); //Si il s'agit d'une modification de CR, la typo est sauvée dans l'arraylit, dans la case 2
                str_code_site = Data_CR.get((1));
                str_Nom_projet = Data_CR.get((3));
                str_nom_site = Data_CR.get(5);
                str_phase = Data_CR.get(6);

                Log.i("tryData", Data_CR.get(0));
                Log.i("tryData", Data_CR.get(1));
                Log.i("tryData", Data_CR.get(2));
                Log.i("tryData", Data_CR.get(3));
                Log.i("tryData", Data_CR.get(4));
                Log.i("tryData", Data_CR.get(5));
                Log.i("tryData", Data_CR.get(6));

            }
            if (intent.hasExtra("Data_CR_Check")) {
                Data_CR_Check = (ArrayList<String[]>) intent.getSerializableExtra("Data_CR_Check");
            }

        }

    }

    //Fonction pour initialiser le tablayout
    public void Initialisation_TabLayout() {
        //Badge de notification : Nombre d'éléments restants à cocher dans la checklist
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(
                tabLayout, viewpager, (tab, position) -> {
            switch (position) {
                case 0: {
                    tab.setText("Infos");
                    tab.setIcon(R.drawable.ic_form);
                    badgeDrawableInfos = tab.getOrCreateBadge();
                    badgeDrawableInfos.setBackgroundColor(
                            ContextCompat.getColor(getApplicationContext(), R.color.colorAccent)
                    );
                    //Badge de notification : Nombre d'éléments restants à cocher dans la checklist
                    badgeDrawableInfos.setNumber(5);
                    badgeDrawableInfos.setVisible(true);
                    badgeDrawableInfos.setMaxCharacterCount(3);
                    break;
                }
                case 1: {
                    tab.setText("Checklist");
                    tab.setIcon(R.drawable.ic_checklist);
                    badgeDrawableChecklist = tab.getOrCreateBadge();
                    badgeDrawableChecklist.setBackgroundColor(
                            ContextCompat.getColor(getApplicationContext(), R.color.colorAccent)
                    );
                    //Badge de notification : Nombre d'éléments restants à cocher dans la checklist
                    badgeDrawableChecklist.setNumber(Txt_ArrayList_NomElements.size());
                    badgeDrawableChecklist.setVisible(true);
                    badgeDrawableChecklist.setMaxCharacterCount(3);
                    break;
                }
                case 2: {
                    tab.setText("Notes");
                    tab.setIcon(R.drawable.ic_manuscrit);
                    break;
                }

            }
        }
        );
        tabLayoutMediator.attach();
    }

    //Appelée après la prise d'une photo
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode>=300){
            if(resultCode!=RESULT_OK) {
                displayMsg("Choix du fichier annulé");
            }

        }
        else if (requestCode >= 100 &&  requestCode<= 300) {
//            if (resultCode != RESULT_CANCELED) {
//            }
            if(resultCode!=RESULT_CANCELED){
                pagerAgentViewModel.sendMessageToChecklist("Picture_OK");
            }

            else {
                displayMsg(String.valueOf("Prise de photo annulée"));
                pagerAgentViewModel.sendMessageToChecklist("ANNULE");
            }

        }
        else if (requestCode == 99) {
            if (resultCode == RESULT_CANCELED) {
                displayMsg("Prise de photo annulée");
            } else {
                pagerAgentViewModel.sendMessageToInfos("Photo_ok");
            }
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void sendOnChannel3(String Title, String Message, Context context, String operateur) {
        int op;
        if (operateur.equals("Orange")) {
            op = R.drawable.logo_orange;
        } else {
            op = R.drawable.logo_bytel;
        }
        @SuppressLint("WrongConstant") Notification notification_dep = new Notification.Builder(this, CHANNEL_3_ID)
                .setSmallIcon(R.drawable.ic_genere)
                .setOngoing(true)
                .setContentTitle(Title)
                .setContentText(Message)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setCategory(NotificationCompat.CATEGORY_PROGRESS)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), op))
                .setProgress(100, 0, true)
//                .setDefaults(Notification.DEFAULT_SOUND|Notification.DEFAULT_LIGHTS|Notification.DEFAULT_VIBRATE)
                .build();

        notificationManager.notify(3, notification_dep);
    }

    //Fonction pour afficher un message toast
    public void displayMsg(String str) {
        Toast.makeText(this, str, Toast.LENGTH_LONG).show();
    }

    //Fonction pour créer une notification
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void sendOnChannel1(String Title, String Message, Context context, String operateur, int ic) {
        int op = 0;
        if (operateur.equals("Orange")) {
            op = R.drawable.logo_orange;
        } else if (operateur.equals("Bouygues Télécom")) {
            op = R.drawable.logo_bytel;
        }
        else if (operateur.equals("Free Mobile")) {
            op = R.drawable.logo_free;
        }

        Intent resultintent = new Intent(this,Activity_Accueil.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 1, resultintent, PendingIntent.FLAG_UPDATE_CURRENT);

        @SuppressLint("WrongConstant") Notification notification = new Notification.Builder(this, CHANNEL_1_ID)
                .setSmallIcon(ic)
                .setContentTitle(Title)
                .setContentText(Message)
                .setContentIntent(resultPendingIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_EVENT)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                        op))
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
                .setStyle(new Notification.BigTextStyle().bigText(Message))
                .setAutoCancel(true)
                .build();

        notificationManager.notify(1, notification);
    }


}

//---------------------------------------------------------Classes pour créer un footer dans le pdf-----------------------------------------

class MyFooter extends PdfPageEventHelper {
    Font ffont = new Font(Font.FontFamily.UNDEFINED, 14, Font.ITALIC);
    Font ffont2 = new Font(Font.FontFamily.UNDEFINED, 14);

    int page_number = 1;
    String str_code_site;
    String str_nom_site;

    public MyFooter(String codesite_cr, String nomsite_cr) {
        this.str_code_site=codesite_cr;
        this.str_nom_site=nomsite_cr;
    }

    public void onEndPage(PdfWriter writer, Document document) {
        PdfContentByte cb = writer.getDirectContent();

        Phrase header = new Phrase("Visite technique : " + str_code_site + " - " + str_nom_site, ffont);
        Phrase header2 = new Phrase("");
        Phrase footer = new Phrase(String.valueOf(page_number), ffont2);
        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                header,
                (document.right() - document.left()) / 2 + document.leftMargin(),
                document.top(), 0);

        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                header2,
                (document.right() - document.left()) / 2 + document.leftMargin(),
                document.top()+30, 0);

        ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT,
                footer,
                (document.right()),
                document.bottom(), 0);
        page_number++;
    }
}

class ZoomOutPageTransformer implements ViewPager2.PageTransformer {
    private static final float MIN_SCALE = 0.85f;
    private static final float MIN_ALPHA = 0.5f;

    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();
        int pageHeight = view.getHeight();

        if (position < -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.
            view.setAlpha(0f);

        } else if (position <= 1) { // [-1,1]
            // Modify the default slide transition to shrink the page as well
            float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
            float vertMargin = pageHeight * (1 - scaleFactor) / 2;
            float horzMargin = pageWidth * (1 - scaleFactor) / 2;
            if (position < 0) {
                view.setTranslationX(horzMargin - vertMargin / 2);
            } else {
                view.setTranslationX(-horzMargin + vertMargin / 2);
            }

            // Scale the page down (between MIN_SCALE and 1)
            view.setScaleX(scaleFactor);
            view.setScaleY(scaleFactor);

            // Fade the page relative to its size.
            view.setAlpha(MIN_ALPHA +
                    (scaleFactor - MIN_SCALE) /
                            (1 - MIN_SCALE) * (1 - MIN_ALPHA));

        } else { // (1,+Infinity]
            // This page is way off-screen to the right.
            view.setAlpha(0f);
        }
    }
}

@RequiresApi(21)
class DepthPageTransformer implements ViewPager2.PageTransformer {
    private static final float MIN_SCALE = 0.75f;

    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();

        if (position < -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.
            view.setAlpha(0f);

        } else if (position <= 0) { // [-1,0]
            // Use the default slide transition when moving to the left page
            view.setAlpha(1f);
            view.setTranslationX(0f);
            view.setTranslationZ(0f);
            view.setScaleX(1f);
            view.setScaleY(1f);

        } else if (position <= 1) { // (0,1]
            // Fade the page out.
            view.setAlpha(1 - position);

            // Counteract the default slide transition
            view.setTranslationX(pageWidth * -position);
            // Move it behind the left page
            view.setTranslationZ(-1f);

            // Scale the page down (between MIN_SCALE and 1)
            float scaleFactor = MIN_SCALE
                    + (1 - MIN_SCALE) * (1 - Math.abs(position));
            view.setScaleX(scaleFactor);
            view.setScaleY(scaleFactor);

        } else { // (1,+Infinity]
            // This page is way off-screen to the right.
            view.setAlpha(0f);
        }
    }
}
