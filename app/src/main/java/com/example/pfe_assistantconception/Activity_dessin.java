package com.example.pfe_assistantconception;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.pdf.PdfRenderer;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.alexvasilkov.gestures.Settings;
import com.alexvasilkov.gestures.views.GestureFrameLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.skydoves.colorpickerview.ColorEnvelope;
import com.skydoves.colorpickerview.ColorPickerDialog;
import com.skydoves.colorpickerview.listeners.ColorEnvelopeListener;


import java.io.File;
import java.io.IOException;

public class Activity_dessin extends AppCompatActivity {

    PaintView paintView;
    FloatingActionButton save, color, undo, redo, clear, zoom, menu_dessin, draw_size, choose_forme;
    TextView Titre_croquis, Mode_dessin, Txt_save_croquis, Txt_supprimer_croquis, Txt_précédent, Txt_suivant, Txt_mode_zoom, Txt_couleur, Txt_style_trait, Txt_type_dessin;
    String FOLDERPATH, Titre, code_site, File_Path;
    int reqheight, reqwidth;
    int heightgen, widthgen;
    int num_page;
    Boolean isPDF, creation, modeDessin = true, isOpen = false;
    Bitmap FondExistant;
    GestureFrameLayout gesture;
    ImageView img_color_selected, img_icone_type_dessin;
    Animation fabOpen, fabClose, fabRClockwise, fabRAntiClockWise;


    @SuppressLint("ResourceAsColor")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dessin);

        Titre_croquis = findViewById(R.id.Txt_Croquis_Title);
        Mode_dessin = findViewById(R.id.Txt_mode);
        save = findViewById(R.id.Fab_save_croquis);
        color = findViewById(R.id.Fab_color);
        undo = findViewById(R.id.Fab_undo);
        redo = findViewById(R.id.Fab_redo);
        clear = findViewById(R.id.Fab_clear);
        zoom = findViewById(R.id.Fab_zoom);
        menu_dessin = findViewById(R.id.Fab_menu_dessin);
        img_color_selected = findViewById(R.id.Img_color_picked);
        draw_size = findViewById(R.id.Fab_draw_size);
        img_icone_type_dessin = findViewById(R.id.Img_view_icone_type_dessin);
        choose_forme = findViewById(R.id.Fab_choose_forme);

        Txt_save_croquis = findViewById(R.id.Txt_save_croquis);
        Txt_supprimer_croquis = findViewById(R.id.Txt_clear_croquis);
        Txt_précédent = findViewById(R.id.Txt_précédent_croquis);
        Txt_suivant = findViewById(R.id.Txt_suivant_croquis);
        Txt_mode_zoom = findViewById(R.id.Txt_mode_zoom);
        Txt_couleur = findViewById(R.id.Txt_couleur);
        Txt_type_dessin = findViewById(R.id.Txt_type_dessin);
        Txt_style_trait = findViewById(R.id.Txt_style_trait);

        fabOpen = AnimationUtils.loadAnimation(Activity_dessin.this, R.anim.fab_open);
        fabClose = AnimationUtils.loadAnimation(Activity_dessin.this, R.anim.fab_close);
        fabRClockwise = AnimationUtils.loadAnimation(Activity_dessin.this, R.anim.rotate_clockwise);
        fabRAntiClockWise = AnimationUtils.loadAnimation(Activity_dessin.this, R.anim.rotate_anticlockwise);

        menu_dessin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOpen) {
                    menu_dessin.startAnimation(fabRClockwise);
                    save.startAnimation(fabClose);
                    color.startAnimation(fabClose);
                    undo.startAnimation(fabClose);
                    redo.startAnimation(fabClose);
                    clear.startAnimation(fabClose);
                    zoom.startAnimation(fabClose);
                    choose_forme.startAnimation(fabClose);
                    draw_size.startAnimation(fabClose);

                    Txt_save_croquis.startAnimation(fabClose);
                    Txt_supprimer_croquis.startAnimation(fabClose);
                    Txt_précédent.startAnimation(fabClose);
                    Txt_suivant.startAnimation(fabClose);
                    Txt_mode_zoom.startAnimation(fabClose);
                    Txt_couleur.startAnimation(fabClose);
                    Txt_type_dessin.startAnimation(fabClose);
                    Txt_style_trait.startAnimation(fabClose);

                    color.setClickable(false);
                    choose_forme.setClickable(false);
                    draw_size.setClickable(false);


                    save.setClickable(false);
                    undo.setClickable(false);
                    redo.setClickable(false);
                    clear.setClickable(false);
                    zoom.setClickable(false);

                    isOpen = false;
                } else {
                    menu_dessin.startAnimation(fabRAntiClockWise);
                    save.startAnimation(fabOpen);
                    color.startAnimation(fabOpen);
                    undo.startAnimation(fabOpen);
                    redo.startAnimation(fabOpen);
                    clear.startAnimation(fabOpen);
                    zoom.startAnimation(fabOpen);
                    choose_forme.startAnimation(fabOpen);
                    draw_size.startAnimation(fabOpen);

                    Txt_save_croquis.startAnimation(fabOpen);
                    Txt_supprimer_croquis.startAnimation(fabOpen);
                    Txt_précédent.startAnimation(fabOpen);
                    Txt_suivant.startAnimation(fabOpen);
                    Txt_mode_zoom.startAnimation(fabOpen);
                    Txt_couleur.startAnimation(fabOpen);
                    Txt_type_dessin.startAnimation(fabOpen);
                    Txt_style_trait.startAnimation(fabOpen);

                    color.setClickable(true);
                    choose_forme.setClickable(true);
                    draw_size.setClickable(true);

                    save.setClickable(true);
                    undo.setClickable(true);
                    redo.setClickable(true);
                    clear.setClickable(true);
                    zoom.setClickable(true);


                    isOpen = true;
                }
            }
        });

        changeFABColor();
        color.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorAccent));
        choose_forme.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorAccent));
        draw_size.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorAccent));

        gesture = findViewById(R.id.gesture);
        gesture.getController().getSettings()
                .setMaxZoom(3f)
                .setDoubleTapZoom(-1f) // Falls back to max zoom level
                .setPanEnabled(true)
                .setZoomEnabled(true)
                .setDoubleTapEnabled(true)
                .setRotationEnabled(false)
                .setRestrictRotation(false)
                .setOverscrollDistance(0f, 0f)
                .setOverzoomFactor(2f)
                .setFillViewport(false)
                .setFitMethod(Settings.Fit.INSIDE)
                .setGravity(Gravity.CENTER);
        gesture.getController().getSettings()
                .disableBounds()
                .disableGestures();

//*********************************************************************************************************************************************************************
//                                                          RECUPERATION DES PARAMETRES
//*********************************************************************************************************************************************************************

        //Récupération des données passées en paramètres
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("Titre_croquis")) {
                Titre = intent.getStringExtra("Titre_croquis");
                Titre_croquis.setText(Titre);
            }
            if (intent.hasExtra("arg_Folder")) {
                FOLDERPATH = intent.getStringExtra("arg_Folder");
            }
            if (intent.hasExtra("arg_Code_site")) {
                code_site = intent.getStringExtra("arg_Code_site");
            }
            if (intent.hasExtra("arg_File_Path")) {
                File_Path = intent.getStringExtra("arg_File_Path");
            }
            if (intent.hasExtra("arg_is_PDF")) {
                isPDF = intent.getBooleanExtra("arg_is_PDF", false);
            }
            if (intent.hasExtra("arg_num_page")) {
                num_page = intent.getIntExtra("arg_num_page", -1);
            }

            if (intent.hasExtra("arg_isCreation")) {
                creation = intent.getBooleanExtra("arg_isCreation", false);

                if (intent.hasExtra("arg_CroquisExistant")) {
                    String path = getIntent().getStringExtra("arg_CroquisExistant");
                    FondExistant = BitmapFactory.decodeFile(path);
                }

                if(intent.hasExtra("arg_img_cr")){
                    String path = getIntent().getStringExtra("arg_img_cr");
                    FondExistant = BitmapFactory.decodeFile(path);
                }
            }
        }

        //Récupération des mesures de l'écran
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

//*********************************************************************************************************************************************************************
//                                                          MODE EDITION
//*********************************************************************************************************************************************************************
        if (!creation) {
            paintView = findViewById(R.id.paintView);
            heightgen = metrics.heightPixels;
            widthgen = metrics.widthPixels;
            //Initilisation du croquis avec fond
            Bitmap bit =getScaledBitMapBaseOnScreenSize(FondExistant, widthgen, heightgen);


            paintView.initialiseEDIT(bit.getHeight(), bit.getWidth(), bit, Titre);
        }

//*********************************************************************************************************************************************************************
//                                                          MODE CREATION
//*********************************************************************************************************************************************************************
        else {

            //Numéro de page = -2 --> le fond à utiliser provient d'une photographie du compte-rendu
            if(num_page==-2){
                paintView = findViewById(R.id.paintView);

                //Rotation de l'image
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                Bitmap rotatedBitmap = Bitmap.createBitmap(FondExistant, 0, 0, FondExistant.getWidth(), FondExistant.getHeight(), matrix, true);

                reqheight = metrics.heightPixels;
                reqwidth = metrics.widthPixels;

                //Adaptation de l'image aux dimensions de l'écran tout en conservant les proportions
                Bitmap bit =getScaledBitMapBaseOnScreenSize(rotatedBitmap, reqwidth, reqheight);

                //Initialistion de la zone de dessins, même fonction que si le fond est tiré d'un fichier PDF
                Log.i("tryinitImg", "initpdfnormal");
                paintView.initialisePDF(bit.getHeight(), bit.getWidth(), bit);

            }
            else{

                //Si il n'y a pas de chemin pdf passé en paramètre, il s'agit d'un croquis vierge
                if (!isPDF) {
                    Log.i("tryinitImg", "isnotpdf");

                    paintView = findViewById(R.id.paintView);
                    //Fond vierge donc zone de dessin des dimensions de l'écran
                    heightgen = metrics.heightPixels;
                    widthgen = metrics.widthPixels;
                    //Initialisation du paintview
                    paintView.initialise(heightgen, widthgen);

                }

                //Sinon il s'agit d'un croquis avec un fond de pdf
                else {
                    Log.i("tryinitImg", "ispdf");

                    paintView = findViewById(R.id.paintView);

                    //Création du fichier
                    File pdfFile = new File(File_Path);
                    //Récupération des dimensions de l'écran pour scale le pdf
                    reqheight = metrics.heightPixels;
                    reqwidth = metrics.widthPixels;
                    //Conversion des pages du pdf en bitmap
                    Bitmap fond = pdfToBitmap(pdfFile, num_page); //Fon du croquis selon le numéro de page

                    //Si le bitmap de fon est au format paysage, on tourne le bitmap à 90°
                    if (fond.getHeight() < fond.getWidth()) {
                        Matrix matrix = new Matrix();
                        matrix.postRotate(90);
                        Bitmap rotatedBitmap = Bitmap.createBitmap(fond, 0, 0, fond.getWidth(), fond.getHeight(), matrix, true);

                        //Récupération des dimensions du fond pdf pour l'appliquer au paintview, pour qu'il ait les mêmes dimensions
                        widthgen = rotatedBitmap.getWidth();
                        heightgen = rotatedBitmap.getHeight();

                        //Initilisation du croquis avec fond

                        paintView.initialisePDF(heightgen, widthgen, rotatedBitmap);
                    } else {
                        widthgen = fond.getWidth();
                        heightgen = fond.getHeight();

                        //Initilisation du croquis avec fond
                        paintView.initialisePDF(heightgen, widthgen, fond);
                    }

                }

            }


        }

        //Initialisation de la taille de l'image view de visu de la taille
        float dim = (float) paintView.getStrokeWidth();
        float px = convertDpToPx(Activity_dessin.this, dim);
        int px1 = (int) px;
        ConstraintLayout.LayoutParams params1 = (ConstraintLayout.LayoutParams) img_color_selected.getLayoutParams();
        params1.height = px1;
        img_color_selected.setLayoutParams(params1);


        //Réaction au clic du bouton couleur
        zoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (paintView.isDraw()) {
                    gesture.getController().getSettings()
                            .enableBounds()
                            .enableGestures();
                    displayMsg("Zoom activé");
                    paintView.setDraw(false);
                    zoom.setImageResource(R.drawable.ic_edit_white);

                    Mode_dessin.setText("Mode zoom");
                    Mode_dessin.setTextColor(ContextCompat.getColor(Activity_dessin.this, R.color.colorSade)); //Bleu Sade

                    Txt_mode_zoom.setText("Mode dessin");

                    modeDessin = false;
                    changeFABColor();

                    color.setClickable(false);
                    choose_forme.setClickable(false);
                    draw_size.setClickable(false);

                    color.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.grey));
                    choose_forme.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.grey));
                    draw_size.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.grey));

                } else {

                    paintView.setDraw(true);
                    gesture.getController().getSettings()
                            .disableGestures()
                            .disableBounds();
                    zoom.setImageResource(R.drawable.ic_zoom);
                    displayMsg("Zoom désactivé");

                    Mode_dessin.setText("Mode dessin");
                    Txt_mode_zoom.setText("Mode zoom");

                    Mode_dessin.setTextColor(ContextCompat.getColor(Activity_dessin.this, R.color.colorAccent));

                    color.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorAccent));
                    choose_forme.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorAccent));
                    draw_size.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorAccent));

                    color.setClickable(true);
                    choose_forme.setClickable(true);
                    draw_size.setClickable(true);


                    modeDessin = true;
                    changeFABColor();
                }
            }
        });

        color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new ColorPickerDialog.Builder(Activity_dessin.this)
                        .setTitle("Sélection de couleur")
                        .setPreferenceName("Sélection de couleur")
                        .setPositiveButton(getString(R.string.select),
                                new ColorEnvelopeListener() {
                                    @Override
                                    public void onColorSelected(ColorEnvelope envelope, boolean fromUser) {
                                        paintView.setColor(envelope.getColor());
//                                        img_color_selected.getDrawable().setColorFilter(envelope.getColor(), PorterDuff.Mode.SRC);
                                        GradientDrawable bgShape = (GradientDrawable) img_color_selected.getBackground();
                                        bgShape.setColor(envelope.getColor());
                                    }
                                })
                        .setNegativeButton(getString(R.string.annule),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                        .attachAlphaSlideBar(true) // the default value is true.
                        .attachBrightnessSlideBar(true)  // the default value is true.
                        .setBottomSpace(12) // set a bottom space between the last slidebar and buttons.
                        .show();


            }
        });

        //Réaction au clic du bouton undo
        undo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paintView.undo();
            }
        });

        //Réaction au clic du bouton redo
        redo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paintView.redo();
            }
        });

        //Réaction au lic du bouton clear
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //AlertDialog pour confirmer le clear du croquis

                Dialog dialog = new Dialog(Activity_dessin.this);
                dialog.setContentView(R.layout.dialog_box_custom);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));

                }

                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
                dialog.setCancelable(false);
                Button ok = dialog.findViewById((R.id.Btn_dialog_ok));
                ok.setText("Oui");
                Button cancel = dialog.findViewById(R.id.Btn_dialog_cancel);
                cancel.setText("Non");
                TextView Title = dialog.findViewById(R.id.Txt_Title_Alert);
                Title.setText("Nettoyer le croquis ?");
                ImageView ic = dialog.findViewById(R.id.Ic_alrt_dialog);
                ic.setImageDrawable(getDrawable(R.drawable.ic_warning_orange));
                TextView msg = dialog.findViewById(R.id.Txt_Msg_alert_dialog);
                msg.setText("Voulez-vous supprimer les données ajoutées au croquis ? ATTENTION : Cette action est irréversible.");

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        paintView.clear();
                        dialog.dismiss();
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        draw_size.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                Dialog dialog = new Dialog(Activity_dessin.this);
                dialog.setContentView(R.layout.dialog_seekbar);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                }

                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
                dialog.setCancelable(false);
                Button ok = dialog.findViewById((R.id.Btn_size_ok));

                //L'image view pour visu la taille prend la couleur courante
                ImageView visu_size = dialog.findViewById(R.id.Img_visu_size);
                GradientDrawable bgShape = (GradientDrawable) visu_size.getBackground();
                bgShape.setColor(paintView.getCurrentColor());

                //Fond blanc pour gérer la transparence
                ImageView visu_size_fond = dialog.findViewById(R.id.Img_visu_size_fond);
                GradientDrawable bgShape2 = (GradientDrawable) visu_size_fond.getBackground();
                bgShape2.setColor(Color.WHITE);

                //Initialisation de la taille de l'image view de visu de la taille
                float dim = (float) paintView.getStrokeWidth();
                float px = convertDpToPx(Activity_dessin.this, dim);
                int px1 = (int) px;
                ConstraintLayout.LayoutParams params1 = (ConstraintLayout.LayoutParams) visu_size_fond.getLayoutParams();
                params1.height = px1;
                visu_size_fond.setLayoutParams(params1);
                ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) visu_size.getLayoutParams();
                params.height = px1;
                visu_size.setLayoutParams(params);

                //Initialisation de la seekbar
                SeekBar seekBar = dialog.findViewById(R.id.seekBar);
                seekBar.setMax(40);
                seekBar.setMin(1);
                seekBar.setProgress(paintView.getStrokeWidth());

                //MAJ de l'épaisseur la valeur ondiquée par le seekbar
                seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        float dim = (float) progress;
                        float px = convertDpToPx(Activity_dessin.this, dim);
                        int px1 = (int) px;
                        ConstraintLayout.LayoutParams params1 = (ConstraintLayout.LayoutParams) visu_size_fond.getLayoutParams();
                        params1.height = px1 / 2;
                        visu_size_fond.setLayoutParams(params1);

                        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) visu_size.getLayoutParams();
                        params.height = px1 / 2;
                        visu_size.setLayoutParams(params);

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        paintView.setStrokeWidth(seekBar.getProgress());

                        //Initialisation de la taille de l'image view de visu de la taille
                        float dim = (float) paintView.getStrokeWidth();
                        float px = convertDpToPx(Activity_dessin.this, dim);
                        int px1 = (int) px;
                        ConstraintLayout.LayoutParams params1 = (ConstraintLayout.LayoutParams) img_color_selected.getLayoutParams();
                        params1.height = px1 / 2;
                        img_color_selected.setLayoutParams(params1);

                        dialog.dismiss();
                    }
                });

                dialog.show();

            }


        });

        choose_forme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dialog dialog = new Dialog(Activity_dessin.this);
                dialog.setContentView(R.layout.dialog_select_mode);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                }

                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
                dialog.setCancelable(true);
                FloatingActionButton draw_finger = dialog.findViewById(R.id.Fab_finger);
                FloatingActionButton draw_line = dialog.findViewById(R.id.Fab_draw_line);
                FloatingActionButton draw_triangle = dialog.findViewById(R.id.Fab_draw_triangle);
                FloatingActionButton draw_rectangle = dialog.findViewById(R.id.Fab_draw_rect);
                FloatingActionButton draw_triangle2 = dialog.findViewById(R.id.Fab_draw_triangle_2);
                FloatingActionButton draw_circle = dialog.findViewById(R.id.Fab_draw_circle);
                draw_triangle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        paintView.setStyle(5);
                        img_icone_type_dessin.setImageResource(R.drawable.triangle1_black);
                        dialog.dismiss();
                    }
                });

                draw_triangle2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        paintView.setStyle(6);
                        img_icone_type_dessin.setImageResource(R.drawable.triangle2_black);
                        dialog.dismiss();
                    }
                });

                draw_line.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        paintView.setStyle(4);
                        img_icone_type_dessin.setImageResource(R.drawable.ligne_black);
                        dialog.dismiss();
                    }
                });

                draw_circle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        paintView.setStyle(3);
                        img_icone_type_dessin.setImageResource(R.drawable.cercle_black);
                        dialog.dismiss();
                    }
                });

                draw_rectangle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        paintView.setStyle(2);
                        img_icone_type_dessin.setImageResource(R.drawable.rectangle_black);
                        dialog.dismiss();
                    }
                });

                draw_finger.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        paintView.setStyle(1);
                        img_icone_type_dessin.setImageResource(R.drawable.ic_touch_black);
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dialog dialog = new Dialog(Activity_dessin.this);
                dialog.setContentView(R.layout.dialog_box_custom);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                }

                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
                dialog.setCancelable(false);
                Button ok = dialog.findViewById((R.id.Btn_dialog_ok));
                ok.setText("Oui");
                Button cancel = dialog.findViewById(R.id.Btn_dialog_cancel);
                cancel.setText("Non");
                TextView Title = dialog.findViewById(R.id.Txt_Title_Alert);
                Title.setText("Valider le croquis ?");
                ImageView ic = dialog.findViewById(R.id.Ic_alrt_dialog);
                ic.setImageDrawable(getDrawable(R.drawable.ic_check_orange));
                TextView msg = dialog.findViewById(R.id.Txt_Msg_alert_dialog);
                msg.setText("Voulez-vous valider le croquis et l'enregistrer en l'état ?");

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Envoie d'un message au fragment infos pour indiquer d'envoyer les données pour génrer le CR
                        try {
                            paintView.saveImage(FOLDERPATH, Titre, code_site);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                        finish();
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();

            }
        });

    }

    //Conversion des pages du PDF en image Bitmap
    private Bitmap pdfToBitmap(File pdfFile, int num_page) {
        Bitmap bitmap = null;

        try {
            PdfRenderer renderer = new PdfRenderer(ParcelFileDescriptor.open(pdfFile, ParcelFileDescriptor.MODE_READ_ONLY));

            PdfRenderer.Page page = renderer.openPage(num_page);
            int width, height;

            //Si la page est au format paysage, on inverse le dimensions hauteur et largeur du bitmap
            if (page.getHeight() < page.getWidth()) {
                width = getResources().getDisplayMetrics().densityDpi / 72 * page.getHeight();
                height = getResources().getDisplayMetrics().densityDpi / 72 * page.getWidth();
                float scale = Math.min((float) reqwidth / width, (float) reqheight / height);
                bitmap = Bitmap.createBitmap((int) (height * scale), (int) (width * scale), Bitmap.Config.ARGB_8888);

            } else {
                width = getResources().getDisplayMetrics().densityDpi / 72 * page.getWidth();
                height = getResources().getDisplayMetrics().densityDpi / 72 * page.getHeight();
                float scale = Math.min((float) reqwidth / width, (float) reqheight / height);
                bitmap = Bitmap.createBitmap((int) (width * scale), (int) (height * scale), Bitmap.Config.ARGB_8888);

            }

            Canvas canvas = new Canvas(bitmap);
            canvas.drawColor(Color.WHITE);
            canvas.drawBitmap(bitmap, 0, 0, null);
            page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            page.close();

            renderer.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return bitmap;

    }

    //Fonction pour afficher des messages de type toasts
    public void displayMsg(String str) {
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {

        //Création d'une alerte en cas d'appuie sur le bouton retour
        Dialog dialog1 = new Dialog(Activity_dessin.this);
        dialog1.setContentView(R.layout.dialog_box_custom);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog1.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
        }

        dialog1.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog1.getWindow().getAttributes().windowAnimations = R.style.animation;
        dialog1.setCancelable(false);
        Button ok = dialog1.findViewById((R.id.Btn_dialog_ok));
        ok.setText("Oui");
        Button cancel = dialog1.findViewById(R.id.Btn_dialog_cancel);
        cancel.setText("Non");
        TextView Title = dialog1.findViewById(R.id.Txt_Title_Alert);
        Title.setText("Attention");
        ImageView ic = dialog1.findViewById(R.id.Ic_alrt_dialog);
        ic.setImageDrawable(getDrawable(R.drawable.ic_warning_orange));
        TextView msg = dialog1.findViewById(R.id.Txt_Msg_alert_dialog);
        msg.setText("Vous voulez abandonner ce croquis ? Les modifications non sauvegardées seront perdues.");

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
                finish();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });

        dialog1.show();
    }

    public void changeFABColor() {
        if (modeDessin) {
            save.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorAccent));
//            color.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorAccent));
            undo.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorAccent));
            redo.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorAccent));
            clear.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorAccent));
            zoom.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorAccent));
            menu_dessin.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorAccent));
        } else {
            save.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorSade));
//            color.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorSade));
            undo.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorSade));
            redo.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorSade));
            clear.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorSade));
            zoom.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorSade));
            menu_dessin.setBackgroundTintList(AppCompatResources.getColorStateList(Activity_dessin.this, R.color.colorSade));
        }
    }

    private Bitmap getScaledBitMapBaseOnScreenSize(Bitmap bitmapOriginal, int width, int height){

        //Création d'un nouveau bitmap
        Bitmap background = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        //Sauvegarde des dimensions de l'original
        float originalWidth = bitmapOriginal.getWidth();
        float originalHeight = bitmapOriginal.getHeight();

        Canvas canvas = new Canvas(background);

        //Scale de l'image de fond
        float scale = width / originalWidth;
        Matrix transformation = new Matrix();
        transformation.preScale(scale, scale);

        //Création du paint
        Paint paint = new Paint();
        paint.setFilterBitmap(true);

        //application du fond
        canvas.drawBitmap(bitmapOriginal, transformation, paint);

        //Renvoi du nouveau bitmap
        return background;
    }

    public float convertDpToPx(Context context, float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }
}
