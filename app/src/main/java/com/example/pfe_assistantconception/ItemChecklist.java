package com.example.pfe_assistantconception;

//***********************************************CLASSE POUR ITEM DE LA CHECKLIST*********************************************************************
public class ItemChecklist {
    private String Nom_Item;
    private String Descriptif_Item;
    private String Commentaire_oui_non;
    private String Photo_oui_non;
    private int Num;

    public ItemChecklist(String Nom_Item,String Descriptif_Item,String Commentaire_oui_non,String Photo_oui_non){
        this.Nom_Item=Nom_Item;
        this.Descriptif_Item=Descriptif_Item;
        this.Commentaire_oui_non=Commentaire_oui_non;
        this.Photo_oui_non=Photo_oui_non;
    }

    public String getNom_Item(){
        return Nom_Item;
    }

    public String getDescriptif_Item(){
        return Descriptif_Item;
    }

    public String getCommentaire_oui_non(){
        return Commentaire_oui_non;
    }

    public String getPhoto_oui_non(){
        return Photo_oui_non;
    }

    public void setNum(int num){
        Num=num;
    }

    public int getNum(){
       return Num;
    }

    public void setNom_Item(String Nom){
        Nom_Item=Nom;
    }
}
