package com.example.pfe_assistantconception;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;


//#########################################################################################################################################################
//--------------------------------------------------------------------CHOIX DU CONTEXTE DE LA VT------------------------------------------------------
//#########################################################################################################################################################


public class Activity_Choix_contexte extends AppCompatActivity {

    private SwitchCompat SwBytel, SwOrange, SwNeuf, SwExistant, SwFree;
    private TextView Txt_Precis_Edifice;
    private EditText Edit_txt_Edifice, Edit_txt_code_site, EditNomSite;
    private Button Bt_Valider_contexte;
    private Spinner Spin_edifice, Spin_Phase;
    public boolean bool_Orange, bool_Bytel, bool_site_neuf, bool_Free;
    public String Str_nom_projet, Str_edifice, Str_code_site, Str_Nom_site, Str_phase, str_operateur;
    public Spinner Spin_Projet;
    public String[] Txt_Projets_Bytel, Txt_Projets_Orange, Txt_Projets_Free;

//#########################################################################################################################################################
//--------------------------------------------------------------------DEBUT METHODE ONCREATE------------------------------------------------------
//#########################################################################################################################################################

    @SuppressLint("WrongViewCast")
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choix_contexte);

        //Récupération de l'espace de stockage disponible
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
        } else {
            bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
        }
        long megAvailable = bytesAvailable / (1024 * 1024);

        //**************************************************************Gestion du stockage**************************************************************

        //Stockage critique, création d'un nouveau CR impossible
        if (megAvailable < 300) {

            Dialog dialog = new Dialog(Activity_Choix_contexte.this);
            dialog.setContentView(R.layout.dialog_box_custom);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
            }

            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
            dialog.setCancelable(false);
            Button ok = dialog.findViewById((R.id.Btn_dialog_ok));
            ok.setText("OK");
            Button cancel = dialog.findViewById(R.id.Btn_dialog_cancel);
            cancel.setVisibility(View.INVISIBLE);
            TextView Title = dialog.findViewById(R.id.Txt_Title_Alert);
            Title.setText("Espace de stockage critique !");
            ImageView ic = dialog.findViewById(R.id.Ic_alrt_dialog);
            ic.setImageDrawable(getDrawable(R.drawable.ic_warning_orange));
            TextView msg = dialog.findViewById(R.id.Txt_Msg_alert_dialog);
            msg.setText("L'espace de stockage de votre appareil est saturé. Vous n'avez que " + String.valueOf(megAvailable) + " Mo disponible.\n" +
                    "Vous devez libérer de la place avant d'établir un nouveau compte-rendu. Vous allez être redirigé vers la liste des comptes-rendus enregistrés.");

            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    finish();
                    Intent intent = new Intent(Activity_Choix_contexte.this, Activity_liste.class);
                    startActivity(intent);
                }
            });

            dialog.show();
        }

        //Récupération des phases du projet dans un fichier txt
        ArrayList<String> ListPhases = new ArrayList<>();

        String path_fichier_phases_documents = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString() + "/ACSIT_CHECKLISTS/Phases.txt";
        File fichier_phases = new File(path_fichier_phases_documents);
        if (fichier_phases.exists()) {
            Scanner s = null;
            try {
                s = new Scanner(new File(path_fichier_phases_documents)).useDelimiter(System.lineSeparator());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            //Tant que le fichier comporte d'autres lignes
            while (s.hasNextLine()) {
                //On ajoute la ligne dans la liste
                ListPhases.add(s.nextLine());

            }
            s.close();
        } else {
            ListPhases = GetPhases();
        }

        //Initialisation des boutons et des zones de textes
        SwBytel = findViewById(R.id.Sw_Bytel);
        SwOrange = findViewById(R.id.Sw_Orange);
        SwFree = findViewById(R.id.Sw_Free);
        Spin_edifice = findViewById(R.id.Spin_edifice);
        Txt_Precis_Edifice = findViewById(R.id.Txt_Precis_edifice);
        Edit_txt_Edifice = findViewById(R.id.Edit_precision_edifice);
        SwNeuf = findViewById(R.id.Sw_neuf);
        SwExistant = findViewById(R.id.Sw_Existant);
        Edit_txt_code_site = findViewById(R.id.Edit_Code_site);
        EditNomSite = findViewById(R.id.Edit_nom_site);
        Bt_Valider_contexte = findViewById(R.id.Btn_valider);
        Bt_Valider_contexte.setEnabled(false);
        Spin_Projet = findViewById(R.id.Spin_projet);
        Spin_Projet.setEnabled(false);
        Spin_Phase = findViewById(R.id.Spin_Phase);

        //Initialisation du Spinner pour les phases du projet
        ArrayAdapter<String> adapter = new ArrayAdapter<>(Activity_Choix_contexte.this, android.R.layout.simple_spinner_dropdown_item, ListPhases);
        Spin_Phase.setAdapter(adapter);


        //ArrayLists avec la liste des projets des 2 opérateurs
        ArrayList<String> Projets_Bytel = new ArrayList<>();
        ArrayList<String> Projets_Orange = new ArrayList<>();
        ArrayList<String> Projets_Free = new ArrayList<>();

        //Remplissage des ArrayLists avec les fichiers textes

        Projets_Bytel = GetProjets(Projets_Bytel, "Bouygues");
        Txt_Projets_Bytel = Projets_Bytel.toArray(new String[Projets_Bytel.size()]);


        Projets_Orange = GetProjets(Projets_Orange, "Orange");
        Txt_Projets_Orange = Projets_Orange.toArray(new String[Projets_Orange.size()]);

        Projets_Free = GetProjets(Projets_Orange, "Free");
        Txt_Projets_Free = Projets_Free.toArray(new String[Projets_Free.size()]);

        //Appel de la fonction Masque_Demasque pour gérer les Switch et CheckBox
        Masque_demasque_switch_checkbox(SwOrange, SwBytel, SwFree, SwNeuf, SwExistant, Bt_Valider_contexte, Spin_Projet);

        //Vérifier changement choix liste déroulante nature édifice
        Masque_demasque_precision_edifice(Spin_edifice, Txt_Precis_Edifice, Edit_txt_Edifice);

        //****************************************************VALIDATION DU CONTEXTE ET VERIFICATION DES DONNEES*****************************************************
        Bt_Valider_contexte.setOnClickListener(v -> {

            //Récupération du choix de l'opérateur
            if (SwBytel.isChecked()) {
                str_operateur = "Bouygues Télécom";
                bool_Bytel = true;
                bool_Orange = false;
                bool_Free = false;

            } else if (SwOrange.isChecked()) {
                str_operateur = "Orange";
                bool_Orange = true;
                bool_Bytel = false;
                bool_Free = false;
            } else if (SwFree.isChecked()) {
                str_operateur = "Free Mobile";
                bool_Orange = false;
                bool_Bytel = false;
                bool_Free = true;
            }

            //Récupérer nature de l'édifice
            Str_edifice = Spin_edifice.getSelectedItem().toString();
            if (Str_edifice.equals("Autre (préciser)")) {
                Str_edifice = Edit_txt_Edifice.getText().toString(); //Récupération champ de texte si edifice autre
            }

            //Récupérer site neuf oui ou non
            if (SwNeuf.isChecked()) {
                bool_site_neuf = true;
            } else if (SwExistant.isChecked()) {
                bool_site_neuf = false;
            }

            Str_code_site = Edit_txt_code_site.getText().toString().trim();
            if(Str_code_site.contains("\n")){
                Str_code_site.replace("\n","");
            }
            Str_Nom_site = EditNomSite.getText().toString().trim();
            if(Str_Nom_site.contains("\n")){
                Str_Nom_site.replace("\n","");
            }

            //Récupérer Nom du projet
            Str_nom_projet = Spin_Projet.getSelectedItem().toString();
            Str_phase = Spin_Phase.getSelectedItem().toString();

            //Si un des élément est vide, l'utilisateur est informé avec une boite de dialogue
            if (Str_Nom_site == null || Str_Nom_site.isEmpty() || Str_code_site == null || Str_code_site.isEmpty()) {
                Dialog dialog = new Dialog(Activity_Choix_contexte.this);
                dialog.setContentView(R.layout.dialog_box_custom);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                }

                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
                dialog.setCancelable(false);
                Button ok = dialog.findViewById((R.id.Btn_dialog_ok));
                ok.setText("OK");
                Button cancel = dialog.findViewById(R.id.Btn_dialog_cancel);
                TextView Title = dialog.findViewById(R.id.Txt_Title_Alert);
                Title.setText("Element(s) manquant(s)");
                ImageView ic = dialog.findViewById(R.id.Ic_alrt_dialog);
                ic.setImageDrawable(getDrawable(R.drawable.ic_warning_orange));
                TextView msg = dialog.findViewById(R.id.Txt_Msg_alert_dialog);
                msg.setText("Veuillez entrer le code site et le nom du site avant de continuer.");
                cancel.setVisibility(View.INVISIBLE);

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();

                if(Str_edifice!=null){
                    Log.i("tryedi", Str_edifice);
                }
            }

            else if(Spin_edifice.getSelectedItem().toString().equals("Autre (préciser)") && Str_edifice.trim().equals("")){
                Log.i("tryedi","1");

//                if() {
//                    Log.i("tryedi","2");


                    Dialog dialog = new Dialog(Activity_Choix_contexte.this);
                    dialog.setContentView(R.layout.dialog_box_custom);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                    }

                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
                    dialog.setCancelable(false);
                    Button ok = dialog.findViewById((R.id.Btn_dialog_ok));
                    ok.setText("OK");
                    Button cancel = dialog.findViewById(R.id.Btn_dialog_cancel);
                    TextView Title = dialog.findViewById(R.id.Txt_Title_Alert);
                    Title.setText("Précision manquante");
                    ImageView ic = dialog.findViewById(R.id.Ic_alrt_dialog);
                    ic.setImageDrawable(getDrawable(R.drawable.ic_warning_orange));
                    TextView msg = dialog.findViewById(R.id.Txt_Msg_alert_dialog);
                    msg.setText("Veuillez préciser le type de site concerné par la visite technique.");
                    cancel.setVisibility(View.INVISIBLE);

                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
//                }
            }
            //Si tous les éléments sont renseignés, on demande confirmation à l'utilisateur pour créer le CR
            else {
                Dialog dialog = new Dialog(Activity_Choix_contexte.this);
                dialog.setContentView(R.layout.dialog_box_custom);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                }

                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
                dialog.setCancelable(false);
                Button ok = dialog.findViewById((R.id.Btn_dialog_ok));
                ok.setText("Oui");
                Button cancel = dialog.findViewById(R.id.Btn_dialog_cancel);
                cancel.setText("Non");
                TextView Title = dialog.findViewById(R.id.Txt_Title_Alert);
                Title.setText("Valider ?");
                ImageView ic = dialog.findViewById(R.id.Ic_alrt_dialog);
                ic.setImageDrawable(getDrawable(R.drawable.ic_check_orange));
                TextView msg = dialog.findViewById(R.id.Txt_Msg_alert_dialog);
                msg.setText("Voulez-vous valider ces données et accéder à la création du compte-rendu ?");

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        finish();
                        dialog.dismiss();
                        Creation_CR();
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }

        });

    }


//#########################################################################################################################################################
//--------------------------------------------------------------------FIN METHODE ONCREATE------------------------------------------------------
//#########################################################################################################################################################

    //Fonction pour gérer l'affichage des Switchs
    public void Masque_demasque_switch_checkbox(final SwitchCompat SwOrange, final SwitchCompat SwBytel, final SwitchCompat SwFree, final SwitchCompat Swneuf, final SwitchCompat SwExistant, final Button Bt_Valider_contexte, Spinner spin_Projet) {
        SwOrange.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                //Si le switch 1 est coché
                ArrayAdapter<String> adapter = new ArrayAdapter<>(Activity_Choix_contexte.this, android.R.layout.simple_spinner_dropdown_item, Txt_Projets_Orange);
                spin_Projet.setAdapter(adapter);
                spin_Projet.setClickable(true);
                spin_Projet.setEnabled(true);


                SwBytel.setChecked(false);
                SwFree.setChecked(false);
                Swneuf.setEnabled(true);
                spin_Projet.setEnabled(true);
                SwExistant.setEnabled(true);
                Swneuf.setOnCheckedChangeListener((buttonView13, isChecked13) -> {
                    if (isChecked13) {
                        Bt_Valider_contexte.setEnabled(true);
                        if (SwExistant.isChecked()) {
                            SwExistant.setChecked(false);
                            Swneuf.setChecked(true);
                        }
                        //Si la Checkbox 1 est cochée
                        SwExistant.setChecked(false);
                        bool_site_neuf = true;
                        Swneuf.setChecked(true);

                    } else {
                        //Si la Checkbox 1 est décochée
                        SwExistant.setChecked(true);

                    }

                });

                SwExistant.setOnCheckedChangeListener((buttonView14, isChecked14) -> {
                    if (isChecked14) {
                        Bt_Valider_contexte.setEnabled(true);

                        //Si la Checkbox 2 est cochée
                        Swneuf.setChecked(false);
                        bool_site_neuf = false;

                        if (Swneuf.isChecked()) {
                            Swneuf.setChecked(false);
                            SwExistant.setChecked(true);
                        }
                    } else {
                        //Si la Checkbox 2 est décochée
                        Swneuf.setChecked(true);
                    }
                });
            } else {
                Swneuf.setEnabled(false);
                SwExistant.setEnabled(false);
                spin_Projet.setEnabled(false);
            }

        });

        SwBytel.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                //Si le switch 2 est coché
                ArrayAdapter<String> adapter = new ArrayAdapter<>(Activity_Choix_contexte.this, android.R.layout.simple_spinner_dropdown_item, Txt_Projets_Bytel);
                spin_Projet.setAdapter(adapter);
                spin_Projet.setClickable(true);
                spin_Projet.setEnabled(true);


                SwOrange.setChecked(false);
                SwFree.setChecked(false);
                Swneuf.setEnabled(true);
                SwExistant.setEnabled(true);
                spin_Projet.setEnabled(true);
                Swneuf.setOnCheckedChangeListener((buttonView13, isChecked13) -> {
                    if (isChecked13) {
                        Bt_Valider_contexte.setEnabled(true);
                        if (SwExistant.isChecked()) {
                            SwExistant.setChecked(false);
                            Swneuf.setChecked(true);
                        }
                        //Si la Checkbox 1 est cochée
                        SwExistant.setChecked(false);
                        bool_site_neuf = true;
                        Swneuf.setChecked(true);
                    } else {
                        //Si la Checkbox 1 est décochées
                        SwExistant.setChecked(true);

                    }

                });

                SwExistant.setOnCheckedChangeListener((buttonView14, isChecked14) -> {
                    if (isChecked14) {
                        Bt_Valider_contexte.setEnabled(true);

                        //Si la Checkbox 2 est cochée
                        Swneuf.setChecked(false);
                        bool_site_neuf = false;

                        if (Swneuf.isChecked()) {
                            Swneuf.setChecked(false);
                            SwExistant.setChecked(true);
                        }
                    } else {
                        //Si la Checkbox 2 est décochée
                        Swneuf.setChecked(true);
                    }
                });

            } else {
                Swneuf.setEnabled(false);
                SwExistant.setEnabled(false);
                spin_Projet.setEnabled(false);
            }
        });

        SwFree.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                //Si le switch 2 est coché
                ArrayAdapter<String> adapter = new ArrayAdapter<>(Activity_Choix_contexte.this, android.R.layout.simple_spinner_dropdown_item, Txt_Projets_Bytel);
                spin_Projet.setAdapter(adapter);
                spin_Projet.setClickable(true);
                spin_Projet.setEnabled(true);


                SwOrange.setChecked(false);
                SwBytel.setChecked(false);
                Swneuf.setEnabled(true);
                SwExistant.setEnabled(true);
                spin_Projet.setEnabled(true);
                Swneuf.setOnCheckedChangeListener((buttonView13, isChecked13) -> {
                    if (isChecked13) {
                        Bt_Valider_contexte.setEnabled(true);
                        if (SwExistant.isChecked()) {
                            SwExistant.setChecked(false);
                            Swneuf.setChecked(true);
                        }
                        //Si la Checkbox 1 est cochée
                        SwExistant.setChecked(false);
                        bool_site_neuf = true;
                        Swneuf.setChecked(true);
                    } else {
                        //Si la Checkbox 1 est décochées
                        SwExistant.setChecked(true);

                    }

                });

                SwExistant.setOnCheckedChangeListener((buttonView14, isChecked14) -> {
                    if (isChecked14) {
                        Bt_Valider_contexte.setEnabled(true);

                        //Si la Checkbox 2 est cochée
                        Swneuf.setChecked(false);
                        bool_site_neuf = false;

                        if (Swneuf.isChecked()) {
                            Swneuf.setChecked(false);
                            SwExistant.setChecked(true);
                        }
                    } else {
                        //Si la Checkbox 2 est décochée
                        Swneuf.setChecked(true);
                    }
                });

            } else {
                Swneuf.setEnabled(false);
                SwExistant.setEnabled(false);
                spin_Projet.setEnabled(false);
            }
        });
    }

    //Fonction pour gérer l'affichage de la précision de l'édifice
    public static void Masque_demasque_precision_edifice(final Spinner spin_edifice, final TextView TxtPrecisEdifice, final EditText EditPrecisEdifice) {
        spin_edifice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String test = spin_edifice.getSelectedItem().toString();
                if (test.equals("Autre (préciser)")) {              //Si "Autre (préciser)" est sélectionné, la saisi de texte pour précision apparait
                    TxtPrecisEdifice.setVisibility(View.VISIBLE);
                    EditPrecisEdifice.setVisibility(View.VISIBLE);
                } else {
                    TxtPrecisEdifice.setVisibility(View.GONE);     //Si "Autre (préciser)" est désélectionné, la saisi de texte pour précision disparait
                    EditPrecisEdifice.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {  //Si rien n'est sélectionné

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        SwNeuf.setChecked(false);
        SwExistant.setChecked(false);
        SwBytel.setChecked(false);
        SwFree.setChecked(false);
        SwOrange.setChecked(false);
        Bt_Valider_contexte.setEnabled(false);
        EditNomSite.setText("");
        Edit_txt_code_site.setText("");
        Edit_txt_Edifice.setText("");
    }

    //Fonction pour afficher des messages de type toasts
    public void displayMsg(String str) {
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    //Fonction pour créer l'intent de création de CR et démarrage de l'activité correspondante
    public void Creation_CR() {
        Intent intent = new Intent(this, Activity_Creation_CR.class);
        intent.putExtra("Orange", bool_Orange);
        intent.putExtra("Bytel", bool_Bytel);
        intent.putExtra("Free", bool_Free);
        intent.putExtra("Operateur", str_operateur);
        intent.putExtra("Nom_projet", Str_nom_projet);
        intent.putExtra("Code_site", Str_code_site);
        intent.putExtra("Nom_Site", Str_Nom_site);
        intent.putExtra("Site_neuf", bool_site_neuf);
        intent.putExtra("Type_site", Str_edifice);
        intent.putExtra("Nom_Phase", Str_phase);
        startActivity(intent);
    }

    //Fonction pour récupérer la liste des projets dans les fichiers texte
    public ArrayList<String> GetProjets(ArrayList<String> ListProjets, String Operateur) {
        ArrayList<String> projet = new ArrayList();
        ArrayList<String> temp = new ArrayList();
        String fichier = "";
        if (Operateur.equals("Bouygues")) {
            fichier = "Drive_Bouygues.txt";
        } else if (Operateur.equals("Orange")) {
            fichier = "Drive_Orange.txt";
        } else if (Operateur.equals("Free")) {
            fichier = "Drive_Free.txt";
        }

        try {
            InputStream data = getAssets().open(fichier);
            InputStreamReader isr = new InputStreamReader(data);
            BufferedReader reader = new BufferedReader(isr);
            while (reader.ready()) {
                temp.add(reader.readLine());
                projet.add(" ");
            }
            ArrayList<String[]> ligne_temp = new ArrayList<>(); //ArrayList de tableaux de string temporaire pour split
            for (int y = 0; y < temp.size(); y++) {
                ligne_temp.add(temp.get(y).split(":", 2)); //Le résultat du split se place dans la nouvelle ArrayList
                projet.set(y, ligne_temp.get(y)[0].trim()); //On remplit l'ArrayList avec les noms des projets
            }

            //On parcourt la liste des projets
            ListProjets.addAll(projet);
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("trytxt", e.toString());
        }
        return ListProjets;
    }

    //Fonction pour récupérer les phases des projets dans un fichier texte
    public ArrayList<String> GetPhases() {
        ArrayList<String> ListPhases = new ArrayList();
        String fichier = "Phases.txt";

        try {
            InputStream data = getAssets().open(fichier);
            InputStreamReader isr = new InputStreamReader(data);
            BufferedReader reader = new BufferedReader(isr);
            while (reader.ready()) {
                ListPhases.add(reader.readLine());
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.i("trytxt", e.toString());
        }
        return ListPhases;
    }
}
