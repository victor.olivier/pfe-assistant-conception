package com.example.pfe_assistantconception;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;

//#########################################################################################################################################################
//--------------------------------------------------------------------MENU D'ACCUEIL DE L'APPLICATION------------------------------------------------------
//#########################################################################################################################################################

public class Activity_Accueil extends AppCompatActivity {

    View add_cr, list_cr,outils_google;
    ImageView new_cr, ic_add_drive, ic_pdf, ic_edit, ic_delete, Img_google;
    Button Btn_G_Form;

//#########################################################################################################################################################
//--------------------------------------------------------------------DEBUT METHODE ONCREATE------------------------------------------------------
//#########################################################################################################################################################


    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        setContentView(R.layout.activity_main);

        //Check des permissions de stockage, app photo et GPS
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.CAMERA,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
        };

        //Si autorisations non accordées, on demande à l'utilisateur de les accorder à nouveau
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        //Récupération de l'espace de stockage disponible sur le téléphone
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable;
        if (android.os.Build.VERSION.SDK_INT >=
                android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
        } else {
            bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
        }
        long megAvailable = bytesAvailable / (1024 * 1024);

        Img_google=findViewById(R.id.Img_logo_google);

        //**************************************************************Gestion du stockage**************************************************************

        //Stockage critique, moins de 400Mo dispo
        if (megAvailable < 400) {

            //Affichage d'une boite de dialogue pour alerter du faible espace de stockage disponible
            Dialog dialog = new Dialog(Activity_Accueil.this);
            dialog.setContentView(R.layout.dialog_box_custom);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
            }
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
            dialog.setCancelable(false);
            Button ok = dialog.findViewById((R.id.Btn_dialog_ok));
            ok.setText("OK");
            Button cancel = dialog.findViewById(R.id.Btn_dialog_cancel);
            cancel.setVisibility(View.INVISIBLE);
            TextView Title = dialog.findViewById(R.id.Txt_Title_Alert);
            Title.setText("Espace de stockage critique !");
            ImageView ic = dialog.findViewById(R.id.Ic_alrt_dialog);
            ic.setImageDrawable(getDrawable(R.drawable.ic_warning_orange));
            TextView msg = dialog.findViewById(R.id.Txt_Msg_alert_dialog);
            msg.setText("L'espace de stockage de votre appareil est presque plein. Vous n'avez que " + megAvailable + " Mo disponible.\n" +
                    "Veuillez supprimer des comptes-rendus déjà déposés (ou d'autres données sur votre appareil) avant d'en créer un nouveau pour éviter de perdre des données.\n" +
                    "Vous allez être redirigé vers la liste des comptes-rendus enregistrés.");

            //Si l'utilisateur appuie sur OK, on ouvre la liste des CR enregistrés pour libérer de la place
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent intent = new Intent(Activity_Accueil.this, Activity_liste.class);
                    startActivity(intent);
                }
            });

            dialog.show();
        }
        //Première alerte de stockage, moins d'un Go de disponible
        else if (megAvailable < 1000) {

            Dialog dialog = new Dialog(Activity_Accueil.this);
            dialog.setContentView(R.layout.dialog_box_custom);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
            }

            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
            dialog.setCancelable(false);
            Button ok = dialog.findViewById((R.id.Btn_dialog_ok));
            ok.setText("OK");
            Button cancel = dialog.findViewById(R.id.Btn_dialog_cancel);
            cancel.setVisibility(View.INVISIBLE);
            TextView Title = dialog.findViewById(R.id.Txt_Title_Alert);
            Title.setText("Espace de stockage faible !");
            ImageView ic = dialog.findViewById(R.id.Ic_alrt_dialog);
            ic.setImageDrawable(getDrawable(R.drawable.ic_warning_orange));
            TextView msg = dialog.findViewById(R.id.Txt_Msg_alert_dialog);
            msg.setText("L'espace de stockage de votre appareil est presque plein. Vous n'avez que " + String.valueOf(megAvailable) + " Mo disponible.\n" +
                    "Veuillez libérer de la place pour éviter toute perte de données lors de la création d'un nouveau compte-rendu.");

            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }


        //****************************************************************FIN DE LA GESTION DU STOCKAGE *********************************************************************************

        //Bouton pour créer un nouveau compte-rendu et image associée
        add_cr = findViewById(R.id.btn_add_cr_invisible);
        add_cr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Choix_contexte(v);
            }
        });

        //Bouton pour gérer les comptes-rendus sauvegardés et images associées
        list_cr = findViewById(R.id.btn_list_cr_invisible);
        list_cr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Liste(v);
            }
        });

        outils_google=findViewById(R.id.btn_outils_google_invisible);
        outils_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Img_google.setImageDrawable(getDrawable(R.drawable.google_accent));
                Dialog dialog = new Dialog(Activity_Accueil.this);
                dialog.setContentView(R.layout.dialog_select_outil_google);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
                }

                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
                dialog.setCancelable(true);

                ImageView Drive = dialog.findViewById(R.id.Img_clic_drive);
                ImageView Gmail = dialog.findViewById(R.id.Img_clic_gmail);
                ImageView Maps = dialog.findViewById(R.id.Img_clic_maps);
                ImageView Agenda = dialog.findViewById(R.id.Img_clic_agenda);
                ImageView Chrome = dialog.findViewById(R.id.Img_clic_chrome);
                ImageView Hangouts = dialog.findViewById(R.id.Img_clic_hangout);

                Drive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = getPackageManager().getLaunchIntentForPackage("com.google.android.apps.docs");
                            startActivity(intent);
                        }catch (Exception e){
                            displayMsg("L'application Google Drive n'est pas installée sur cet appareil");
                        }
                    }
                });

                Gmail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = getPackageManager().getLaunchIntentForPackage("com.google.android.gm");
                            startActivity(intent);
                        } catch (Exception e) {
                            displayMsg("L'application Gmail n'est pas installée sur cet appareil");
                        }
                    }

                });

                Maps.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = getPackageManager().getLaunchIntentForPackage("com.google.android.apps.maps");
                            startActivity(intent);
                        } catch (Exception e) {
                            displayMsg("L'application Google Maps n'est pas installée sur cet appareil");
                        }

                    }
                });

                Agenda.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = getPackageManager().getLaunchIntentForPackage("com.google.android.calendar");
                            startActivity(intent);
                        }catch (Exception e){
                            displayMsg("L'application Google Agenda n'est pas installée sur cet appareil");
                        }
                    }
                });

                Chrome.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = getPackageManager().getLaunchIntentForPackage("com.android.chrome");
                            startActivity(intent);
                        }catch (Exception e){
                            displayMsg("L'application Google Chrome n'est pas installée sur cet appareil");
                        }
                    }
                });

                Hangouts.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = getPackageManager().getLaunchIntentForPackage("com.google.android.talk");
                            startActivity(intent);
                        }catch (Exception e){
                            displayMsg("L'application Hangouts n'est pas installée sur cet appareil");
                        }
                    }
                });

                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        Img_google.setImageDrawable(getDrawable(R.drawable.google_bleu_sade));

                    }
                });

                dialog.show();
            }
        });


        String path_fichier_checklist_documents=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString() + "/ACSIT_CHECKLISTS";
        File dossier_checklist=new File(path_fichier_checklist_documents);
        if(!dossier_checklist.exists()){
            dossier_checklist.mkdirs();
        }


    }

//#########################################################################################################################################################
//--------------------------------------------------------------------FIN METHODE ONCREATE------------------------------------------------------
//#########################################################################################################################################################


    public void displayMsg(String str) {
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        new_cr = findViewById(R.id.Img_add_cr);
        new_cr.setImageDrawable(getDrawable(R.drawable.ic_add_document_sade));

        ic_add_drive = findViewById(R.id.Img_drive);
        ic_add_drive.setImageDrawable(getDrawable(R.drawable.ic_add_drive_sade));
        ic_pdf = findViewById(R.id.Img_pdf);
        ic_pdf.setImageDrawable(getDrawable(R.drawable.ic_pdf_sade));
        ic_edit = findViewById(R.id.Img_edit);
        ic_edit.setImageDrawable(getDrawable(R.drawable.ic_edit_sade));
        ic_delete = findViewById(R.id.Img_delete);
        ic_delete.setImageDrawable(getDrawable(R.drawable.ic_delete_sade));

        Img_google.setImageDrawable(getDrawable(R.drawable.google_bleu_sade));
    }

    //Au clic sur le bouton de création d'un nouveau CR, préparation de l'intent et changement de l'icône en orange pour mettre le clic en valeur
    public void Choix_contexte(View view) {
        try {
            Intent intent = new Intent(this, Activity_Choix_contexte.class);
            startActivity(intent);
            new_cr.setImageDrawable(getDrawable(R.drawable.ic_add_document_orange));
        } catch (Exception e) {
        }
    }

    //Au clic sur le bouton de listage des CR, préparation de l'intent et changement des icônes en orange pour mettre le clic en valeur
    public void Liste(View view) {
        Intent intent = new Intent(this, Activity_liste.class);

        try {
            startActivity(intent);
            ic_add_drive.setImageDrawable(getDrawable(R.drawable.ic_add_drive_orange));
            ic_pdf.setImageDrawable(getDrawable(R.drawable.ic_pdf_orange));
            ic_edit.setImageDrawable(getDrawable(R.drawable.ic_edit_orange));
            ic_delete.setImageDrawable(getDrawable(R.drawable.ic_delete_orange));
        } catch (Exception e) {
        }
    }

    //Au clic sur le bouton "remonter un incident", préparation de l'intent et ouverture du formulaire correspondant
    public void GForm(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://docs.google.com/forms/d/1xt7KP1mzIjg9tKGD5X9zEKPjHYA8Dsqk4eQhjtroMQs"));
        startActivity(intent);

    }

    //Fonction pour vérifier si les permissions nécessaires à l'application ont été accordées ou non
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {

                    String path_fichier_checklist_documents=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString() + "/ACSIT_CHECKLISTS";
                    File dossier_checklist=new File(path_fichier_checklist_documents);
                    if(!dossier_checklist.exists()){
                        dossier_checklist.mkdirs();
                    }
                    return false;
                }
            }
        }
        return true;
    }

}
