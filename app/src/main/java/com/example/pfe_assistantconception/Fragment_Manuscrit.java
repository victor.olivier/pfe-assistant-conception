package com.example.pfe_assistantconception;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.itextpdf.text.io.RandomAccessSourceFactory;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.file.Path;
import java.util.ArrayList;

import butterknife.BindView;

import static android.app.Activity.RESULT_OK;

public class Fragment_Manuscrit extends Fragment {

    private FloatingActionButton Fab_add_croquis;
    String FOLDERPATH, code_site, Path_croquis, Path_file;
    Boolean isPDF, creation ;
    @BindView(R.id.Recycler_croquis)
    RecyclerView recyclerView_croquis;
    ArrayList<Bitmap> Liste_Croquis = new ArrayList<>();
    ArrayList<String> Nom_Croquis = new ArrayList<>();
    CroquisAdapter croquisAdapter;
    LinearLayoutManager linearLayoutManager;
    TextView pas_croquis;
    NumberPicker numberPicker;
    String Path_picture;

    private static final int MY_RESULT_CODE_FILECHOOSER = 2000;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_manuscrit, container, false);

        if (getArguments() != null) {
            try {
                FOLDERPATH = getArguments().getString("arg_Folder");
                code_site = getArguments().getString("arg_Code_site");
            } catch (Exception e) {
            }
        }



        Path_croquis=FOLDERPATH+"/Croquis";

        recyclerView_croquis= v.findViewById(R.id.Recycler_croquis);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView_croquis.setLayoutManager(linearLayoutManager);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));
        recyclerView_croquis.addItemDecoration(itemDecorator);


        Lister_Croquis(Path_croquis);
        croquisAdapter=new CroquisAdapter(Liste_Croquis,Nom_Croquis, code_site);
        recyclerView_croquis.setAdapter(croquisAdapter);

        //Gestion de l'affichage/masquage du recyclerview en fonction de la présence ou non de croquis
        pas_croquis = v.findViewById(R.id.Txt_Pas_de_croquis);
        if(Liste_Croquis==null || Liste_Croquis.size()==0){
            pas_croquis.setVisibility(View.VISIBLE);
            recyclerView_croquis.setVisibility(View.GONE);
        }
        else{
            pas_croquis.setVisibility(View.GONE);
            recyclerView_croquis.setVisibility(View.VISIBLE);
        }

        Fab_add_croquis = v.findViewById(R.id.Fab_add_croquis);
        //Bouton pour ajouter un croquis
        Fab_add_croquis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                creation=true;

                Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.dialog_select_fond_croquis);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dialog.getWindow().setBackgroundDrawable(getActivity().getDrawable(R.drawable.dialog_background));
                }

                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
                dialog.setCancelable(true);
                Button vierge = dialog.findViewById((R.id.Btn_dialog_vierge_paint));
                vierge.setText("Vierge");
                Button existant = dialog.findViewById(R.id.Btn_dialog_pdf_paint);
                existant.setText("PDF");
                TextView Title = dialog.findViewById(R.id.Txt_Title_Alert_paint);
                Title.setText("Ajout d'un croquis");
                Button img_cr = dialog.findViewById(R.id.Btn_dialog_pic_cr_paint);
                img_cr.setText("Photo du compte-rendu");
                TextView msg = dialog.findViewById(R.id.Txt_Msg_alert_dialog_paint);
                msg.setText("Quel type de fond souhaitez vous utiliser pour votre croquis ?");

                File file = new File(FOLDERPATH+"/Photos");
                File[] files = file.listFiles();

                if(files==null){
                    img_cr.setVisibility(View.GONE);
                }
                else if(files.length==0){
                    img_cr.setVisibility(View.GONE);
                }
                else{
                    img_cr.setVisibility(View.VISIBLE);
                }

                vierge.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        isPDF=false;
                        SaisiNomCroquis(-1);

                    }
                });

                img_cr.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                        Intent intent = new Intent(getActivity(), FilePickerActivity.class);
                        intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                                .setCheckPermission(true)
                                .setShowImages(true)
                                .setRootPath(FOLDERPATH+"/Photos")
                                .setSingleChoiceMode(true)
                                .setSingleClickSelection(true)
                                .setSkipZeroSizeFiles(true)
                                .build());

                        displayMsg("Sélectionner une image");

                        startActivityForResult(intent,6);
                    }
                });

                existant.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        isPDF=true;
                        //Choix d'un fichier PDF
                        FilePicker();
                        displayMsg("Choisissez un fichier PDF");
                    }
                });

                dialog.show();

                recyclerView_croquis.addOnScrollListener(new RecyclerView.OnScrollListener(){
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                        if (dy<0 && !Fab_add_croquis.isShown())
                            Fab_add_croquis.show();
                        else if(dy>0 && Fab_add_croquis.isShown())
                            Fab_add_croquis.hide();
                    }

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                    }
                });
            }
        });


        croquisAdapter.setOnItemClickListener(new CroquisAdapter.OnItemClickListener() {
            //Si l'utilisateur clique sur modifier
            @Override
            public void OnEditClick(int position) {
                creation=false;
                Bitmap croquis=Liste_Croquis.get(position);
                String nom_croquis = Nom_Croquis.get(position);
                String s = nom_croquis.replace(".png","");
                String s1 = s.replace(code_site+"_","");
                Intent intent = new Intent(getActivity(), Activity_dessin.class);
                intent.putExtra("Titre_croquis", s1);
                intent.putExtra("arg_Folder", FOLDERPATH);
                intent.putExtra("arg_Code_site", code_site);
                intent.putExtra("arg_num_page", position);
                intent.putExtra("arg_isCreation",creation);

                //Convert to byte array
//                ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                croquis.compress(Bitmap.CompressFormat.PNG, 100, stream);
//                byte[] byteArray = stream.toByteArray();

                intent.putExtra("arg_CroquisExistant", Path_croquis+"/"+Nom_Croquis.get(position));
                startActivity(intent);

            }

            //Si l'utilisateur clique sur supprimer
            @Override
            public void OnDeleteClick(int position) {

                Dialog del = new Dialog(getActivity());
                del.setContentView(R.layout.dialog_box_custom);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    del.getWindow().setBackgroundDrawable(getActivity().getDrawable(R.drawable.dialog_background));
                }

                del.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                del.getWindow().getAttributes().windowAnimations = R.style.animation;
                del.setCancelable(true);
                Button Oui = del.findViewById((R.id.Btn_dialog_ok));
                Oui.setText("Oui");
                Button Non = del.findViewById(R.id.Btn_dialog_cancel);
                Non.setText("Non");
                TextView Title = del.findViewById(R.id.Txt_Title_Alert);
                Title.setText("Suppression du croquis");
                ImageView ic = del.findViewById(R.id.Ic_alrt_dialog);
                ic.setImageDrawable(getActivity().getDrawable(R.drawable.ic_warning_orange));
                TextView msg = del.findViewById(R.id.Txt_Msg_alert_dialog);
                msg.setText("Voulez-vous supprimer ce croquis ? ATTENTION : Cette action est irréversible et votre croquis sera définitivement supprimé.");

                Oui.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        del.dismiss();
                        String CroquisPath = Path_croquis+"/"+Nom_Croquis.get(position);
                        File croquistodelete = new File(CroquisPath);
                        //Suppression du fichier concerné et suppression des données de la liste du Recyclerview
                        croquistodelete.delete();
                        Liste_Croquis.remove(position);
                        Nom_Croquis.remove(position);
                        croquisAdapter.notifyDataSetChanged();
                        if(Liste_Croquis.size()==0){
                            pas_croquis.setVisibility(View.VISIBLE);
                        }
                    }
                });

                Non.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        del.dismiss();
                    }
                });

                del.show();
            }
            });


        return v;
    }

    private void Lister_Croquis(String Path_croquis) {
        Liste_Croquis.clear();
        Nom_Croquis.clear();
        File Croquis_Folder = new File(Path_croquis);
        File[] TabFile;
        TabFile=Croquis_Folder.listFiles();
        int i=0;
        if(Croquis_Folder!=null){
            if(TabFile!=null){
                Log.i("trycroq", String.valueOf(TabFile.length));
                for(File croquis : TabFile ){
                    if(croquis.getName().contains(".png")){
                        Liste_Croquis.add(BitmapFactory.decodeFile(croquis.getAbsolutePath()));
                        Nom_Croquis.add(croquis.getName());
                    }
                }
            }
        }


    }

    //Méthode pour choisir un fichier pdf dans la mémoire interne du téléphone
    public void FilePicker(){

        //ANDROID 9 ET INFERIEUR
            Intent chooseFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFileIntent.setType("application/pdf");
            chooseFileIntent.addCategory(Intent.CATEGORY_OPENABLE);
            chooseFileIntent = Intent.createChooser(chooseFileIntent, "Choisissez un fichier PDF");
            startActivityForResult(chooseFileIntent, 5);
    }

    //Boite de dialogue pour saisir le nom du croquis
    public void SaisiNomCroquis(int num_page){
        Dialog saisi = new Dialog(getActivity());
        saisi.setContentView(R.layout.dialog_box_saisi);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            saisi.getWindow().setBackgroundDrawable(getActivity().getDrawable(R.drawable.dialog_background));
        }
        saisi.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        saisi.getWindow().getAttributes().windowAnimations = R.style.animation;
        saisi.setCancelable(true);
        Button valider_nom = saisi.findViewById(R.id.Btn_saisi_ok);
        valider_nom.setText("Valider");
        EditText Edit_nom_croquis = saisi.findViewById(R.id.Edit_nom_croquis);
        Edit_nom_croquis.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        TextView Titre = saisi.findViewById(R.id.Txt_Title_saisi);
        Titre.setText("Saisissez le titre de votre croquis");

        valider_nom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saisi.dismiss();

                String nom_croquis = Edit_nom_croquis.getText().toString();

                if(nom_croquis.trim().matches("")){

                    Dialog dialog = new Dialog(getActivity());
                    dialog.setContentView(R.layout.dialog_box_custom);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        dialog.getWindow().setBackgroundDrawable(getActivity().getDrawable(R.drawable.dialog_background));
                    }

                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    dialog.getWindow().getAttributes().windowAnimations = R.style.animation;
                    dialog.setCancelable(false);
                    Button ok = dialog.findViewById((R.id.Btn_dialog_ok));
                    ok.setText("OK");
                    Button cancel = dialog.findViewById(R.id.Btn_dialog_cancel);
                    cancel.setVisibility(View.INVISIBLE);
                    TextView Title = dialog.findViewById(R.id.Txt_Title_Alert);
                    Title.setText("Nom de croquis vide");
                    ImageView ic = dialog.findViewById(R.id.Ic_alrt_dialog);
                    ic.setImageDrawable(getActivity().getDrawable(R.drawable.ic_warning_orange));
                    TextView msg = dialog.findViewById(R.id.Txt_Msg_alert_dialog);
                    msg.setText("Vous devez saisir un nom pour votre croquis");

                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            SaisiNomCroquis(num_page);
                        }
                    });

                    dialog.show();
                }
                else {
                    Intent intent = new Intent(getActivity(), Activity_dessin.class);
                    if(num_page==-2){
                        intent.putExtra("Titre_croquis", nom_croquis);
                        intent.putExtra("arg_Folder", FOLDERPATH);
                        intent.putExtra("arg_Code_site", code_site);
                        intent.putExtra("arg_is_PDF", false);
                        intent.putExtra("arg_num_page", num_page);
                        intent.putExtra("arg_isCreation", true);

                        intent.putExtra("arg_img_cr", Path_picture);
                        startActivity(intent);


                    }
                    else {

                        intent.putExtra("Titre_croquis", nom_croquis);
                        intent.putExtra("arg_Folder", FOLDERPATH);
                        intent.putExtra("arg_Code_site", code_site);
                        intent.putExtra("arg_is_PDF", isPDF);
                        intent.putExtra("arg_num_page", num_page);
                        intent.putExtra("arg_isCreation", creation);

                        if (isPDF != null) {
                            intent.putExtra("arg_File_Path", Path_file);
                            intent.putExtra("Titre_croquis", nom_croquis);

                        }
                        startActivity(intent);

                    }
                }
            }
        });
        saisi.show();
    }



    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        final int[] num_page_pdf = new int[1];

        if(resultCode == RESULT_OK && data!=null){
            Log.i("tryuri", data.toString());

            switch (requestCode) {
                case 5:
                    Uri uri_pdf = data.getData();
                    Path_file=FileUtils.getPath(getActivity(),uri_pdf);
                    Log.i("tryuri", uri_pdf.toString());
                    Log.i("tryuri", Path_file);

                    File pdf = new File(Path_file);
                    if(pdf.exists() ) {
                        int pages;

                        try {
                            PdfRenderer renderer = new PdfRenderer(ParcelFileDescriptor.open(pdf, ParcelFileDescriptor.MODE_READ_ONLY));
                            pages = renderer.getPageCount();

                        } catch (IOException e) {
                            e.printStackTrace();
                            //Si erreur lors du comptage des pages, on fixe le nombre de pages du PDF à 100
                            pages = 100;
                        }

                        //Si le fichier PDF sélectionné comporte plus d'une page, on demande à l'utilisateur de choisir une page avec une boite de dialogue
                        if (pages > 1) {
                            Dialog number = new Dialog(getActivity());
                            number.setContentView(R.layout.dialog_number_picker);
                            numberPicker = number.findViewById(R.id.Num_page);
                            numberPicker.setMinValue(1);
                            numberPicker.setMaxValue(pages);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                number.getWindow().setBackgroundDrawable(getActivity().getDrawable(R.drawable.dialog_background));
                            }

                            number.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            number.getWindow().getAttributes().windowAnimations = R.style.animation;
                            number.setCancelable(true);
                            Button valider = number.findViewById((R.id.Btn_num_ok));
                            valider.setText("Valider");
                            Button voir = number.findViewById((R.id.Btn_voir_pdf));
                            voir.setText("Voir");
                            TextView Title = number.findViewById(R.id.Txt_num);
                            Title.setText("Saisissez le numéro de page");

                            valider.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //On retire 1 au numéro de page pour correspondre à la position dans la future arrayList
                                    num_page_pdf[0] = numberPicker.getValue() - 1;
                                    number.dismiss();
                                    SaisiNomCroquis(num_page_pdf[0]);

                                }
                            });

                            voir.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    intent.setDataAndType(uri_pdf, "application/pdf");
                                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(intent);
                                }
                            });
                            number.show();
                        }

                        //Si le fichier PDF n'a qu'une page, ce sera le fond du croquis
                        else {
                            //Position 0 pour correspondre à la position dans la future arrayList
                            num_page_pdf[0] = 0;
                            SaisiNomCroquis(num_page_pdf[0]);
                        }
                    }
                    else{
                        displayMsg("Une erreur s'est produite lors de la sélection du fichier. Veuillez en sélectionner un autre");
                    }

                    break;
                case 6:
                    ArrayList<MediaFile> files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                    if (files != null && files.size() > 0) {
                        files.get(0).getUri();

                        Path_picture = FileUtils.getPath(getActivity(),files.get(0).getUri());

                        SaisiNomCroquis(-2);
                    }

                    break;
            }

        }
    }


    //Fonction pour afficher des messages de type toasts
    public void displayMsg(String str) {
        Toast.makeText(getActivity(), str, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onResume() {
        super.onResume();

        Lister_Croquis(Path_croquis);
        //MAJ de la checklist en arrière-plan
        recyclerView_croquis.post(new Runnable() {
            @Override
            public void run() {
                croquisAdapter.notifyDataSetChanged();
            }
        });

        if(Liste_Croquis==null || Liste_Croquis.size()==0){
            pas_croquis.setVisibility(View.VISIBLE);
            recyclerView_croquis.setVisibility(View.GONE);
        }
        else{
            pas_croquis.setVisibility(View.GONE);
            recyclerView_croquis.setVisibility(View.VISIBLE);
        }

        if (Fab_add_croquis.isShown())
            Fab_add_croquis.show();
        else if(Fab_add_croquis.isShown())
            Fab_add_croquis.hide();
    }
}

