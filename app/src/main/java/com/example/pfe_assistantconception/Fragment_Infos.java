package com.example.pfe_assistantconception;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.itextpdf.text.Font;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import static com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY;

public class Fragment_Infos extends Fragment {

    private String FILENAMEPDF,FILENAMETXT,FOLDERNAME;

    private RelativeLayout RL_Lay_Formulaire;
    public EditText Edit_coordonnes, Edit_projet, Edit_operateur, Edit_site_neuf, Edit_type_site, Edit_code_site, Edit_nom_site, Edit_adresse, Edit_code_postal, Edit_ville, Edit_Commentaire, Edit_Phase;
    private TextView Txt_operateur, Txt_code_site, Txt_type_site, Txt_projet, Txt_Etat_Site, Txt_nom_site, Txt_adresse, Txt_code_postal, Txt_ville, Txt_Commentaire, Txt_number_pictures;
    private String str_Nom_projet, str_Type_site, str_Contenu_CRTXT, str_Contenu_CRPDF,str_code_site,str_Nom_site, FOLDERPATH, str_Phase, date,line = System.getProperty("line.separator"), test;
    private ImageView AppPhoto, Map;
    private Boolean bool_bytel, bool_orange, bool_free, bool_site_neuf,bool_collect_data=false;
    public ArrayList<String> Data_CR = new ArrayList<>(),Data = new ArrayList<>();
    public String str_operateur;
    private static final Font Titre = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    FusedLocationProviderClient client;

    public Fragment_Infos() {
        // Required empty public constructor
    }

    //Appellé lors de la création du fragment
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //Initialisation du listener

    }

//#########################################################################################################################################################
//--------------------------------------------------------------------DEBUT METHODE ONCREATEVIEW------------------------------------------------------
//#########################################################################################################################################################

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment__infos, container, false);
        RL_Lay_Formulaire = v.findViewById(R.id.RL_Lay_Formulaire);
        //TextView
        Txt_operateur = v.findViewById(R.id.Txt_Operateur);
        Txt_code_site = v.findViewById(R.id.Txt_Code_site);
        Txt_type_site = v.findViewById(R.id.Txt_Type_site);
        Txt_projet = v.findViewById(R.id.Txt_list_Projet);
        Txt_Etat_Site =v.findViewById(R.id.Txt_Etat_Site);
        Txt_nom_site = v.findViewById(R.id.Txt_nom_site);
        Txt_adresse = v.findViewById(R.id.Txt_Adresse);
        Txt_code_postal =v.findViewById(R.id.Txt_Code_postal);
        Txt_ville =  v.findViewById(R.id.Txt_Ville);
        Txt_Commentaire=v.findViewById(R.id.Txt_Comm_Divers);
        Txt_number_pictures = v.findViewById(R.id.Txt_Number_Pictures_div);
        //EditText
        Edit_projet= v.findViewById(R.id.Edit_Projet);
        Edit_projet.setEnabled(false);
        Edit_operateur= v.findViewById(R.id.Edit_Operateur);
        Edit_operateur.setEnabled(false);
        Edit_site_neuf =  v.findViewById(R.id.Edit_Oui_Non);
        Edit_site_neuf.setEnabled(false);
        Edit_type_site =v.findViewById(R.id.Edit_Type_Site);
        Edit_type_site.setEnabled(false);
        Edit_code_site = v.findViewById(R.id.Edit_Code_Site);
        Edit_code_site.setEnabled(false);
        Edit_nom_site = v.findViewById(R.id.Edit_Nom_site);
        Edit_nom_site.setEnabled(false);
        Edit_adresse = v.findViewById(R.id.Edit_Adresse);
        Edit_code_postal =  v.findViewById(R.id.Edit_Code_postal);
        Edit_ville = v.findViewById(R.id.Edit_Ville);
        Edit_Commentaire = v.findViewById(R.id.Edit_commentaire_divers);
        Edit_Phase=v.findViewById(R.id.Edit_Phase);
        AppPhoto=v.findViewById(R.id.Img_AppPhoto_Infos);
        Map=v.findViewById(R.id.Img_maps);
        Edit_coordonnes=v.findViewById(R.id.Edit_GPS);
        Edit_coordonnes.setEnabled(false);

        client= LocationServices.getFusedLocationProviderClient(getActivity());


        //Ecoute des messages de type "string"
        ViewModelProviders.of(getActivity()).get(CRViewModel.class).getMessageContainerInfos().observe(this, new Observer<String>() {
            //Appellé lorsqu'un message est reçu
            @Override
            public void onChanged(@Nullable String msg) {
                if(msg.matches("collect_data_verif"))
                {
                    //On récupère ce qui a été entré dans les champs du formulaire
                    ArrayList<String> contenu;
                    contenu=collect_donnees();
                    int champs_vide=0;
                    for(int i=0;i<contenu.size();i++){
                        if(contenu.get(i).matches("")|| contenu.get(i).matches("")){
                            //Si un champs est vide, on augmente le nombre de champs vides de 1
                            champs_vide++;
                        }
                    }
                    //Le résultat est renvoyé au fragment Checklist
                    ArrayList<String> retour = new ArrayList<>();
                    retour.add("POUR_VERIF");
                    retour.add(String.valueOf(champs_vide));
                    ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(CRViewModel.class).sendArrayListToActivity(retour);
                    retour.clear();
                }
                else if(msg.matches("collect_data_verif_pour_genere")){
                    //On récupère ce qui a été entré dans les champs du formulaire
                    ArrayList<String> contenu;
                    contenu=collect_donnees();
                    int champs_vide=0;
                    for(int i=0;i<contenu.size();i++){
                        if(contenu.get(i).matches("")|| contenu.get(i).matches("")){
                            //Si un champs est vide, on augmente le nombre de champs vides de 1
                            champs_vide++;
                        }
                    }
                    Nommage_dossier();

                    //Le résultat est renvoyé à l'activité
                    ArrayList<String> retour = new ArrayList<>();
                    retour.add("POUR_GENERE");
                    retour.add(String.valueOf(champs_vide));
                    retour.add(FOLDERNAME);
                    retour.add(FILENAMETXT);
                    retour.add(FILENAMEPDF);
                    str_Contenu_CRTXT = generation_CR_txt();
                    retour.add(str_Contenu_CRTXT);
//                    str_Contenu_CRPDF=generation_CR_PDF();
                    retour.add(Edit_operateur.getText().toString());
                    retour.add(Edit_code_site.getText().toString());
                    retour.add(Edit_type_site.getText().toString());
                    retour.add(Edit_projet.getText().toString());
                    retour.add(Edit_site_neuf.getText().toString());
                    retour.add(Edit_nom_site.getText().toString());
                    retour.add(Edit_adresse.getText().toString());
                    retour.add(Edit_code_postal.getText().toString());
                    retour.add(Edit_ville.getText().toString());
                    if(bool_bytel){
                        retour.add("bool_bytel");
                    }
                    else if(bool_orange){
                        retour.add("bool_orange");
                    }
                    else if(bool_free){
                        retour.add("bool_free");
                    }
                    retour.add("Visite réalisée le : " + date);
                    retour.add(Edit_Commentaire.getText().toString());
                    retour.add(Edit_Phase.getText().toString());
                    retour.add(Edit_coordonnes.getText().toString());

                    ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(CRViewModel.class).sendArrayListToActivity(retour);
                }
                else if(msg.matches("Photo_ok")){
                    int num_pic = Integer.parseInt(Txt_number_pictures.getText().toString());
                    num_pic++;
                    Txt_number_pictures.setText(String.valueOf(num_pic));
                }
            }
        });


        //Récupération des données du choix du contexte de la VT grâce au Bundle
        if (getArguments() != null) {
            try{
                str_Nom_projet = getArguments().getString("arg_Nom_Projet");
                str_code_site= getArguments().getString("arg_Code_Site");
                bool_bytel = getArguments().getBoolean("arg_Bool_Bytel");
                bool_free = getArguments().getBoolean("arg_Bool_Free");
                bool_orange = getArguments().getBoolean("arg_Bool_Orange");
                bool_site_neuf = getArguments().getBoolean("arg_Bool_Site_Neuf");
                str_Type_site = getArguments().getString("arg_Type_site");
                str_Nom_site=getArguments().getString("arg_Nom_site");
                FOLDERPATH = getArguments().getString("arg_Folder");
                str_Phase = getArguments().getString("arg_Nom_Phase");
                str_operateur = getArguments().getString("arg_operateur");
                date=getArguments().getString("arg_date");
            }catch (Exception e){
            }

            try{
                Data_CR=getArguments().getStringArrayList("arg_data");
                str_code_site= getArguments().getString("arg_Code_Site");
                FOLDERPATH = getArguments().getString("arg_Folder");
                date=getArguments().getString("arg_date");
            }catch (Exception e){
            }
            try{
                test=getArguments().getString("key");
            }catch (Exception e){
            }

        }
        try{
            //Si le nom du projet n'est pas vide, on est en mode création
            if(str_Type_site != null){
                Edit_projet.setText(str_Nom_projet);
                Edit_type_site.setText(str_Type_site);
                Edit_code_site.setText(str_code_site);
                Edit_nom_site.setText(str_Nom_site);
                Edit_Phase.setText(str_Phase);
                Log.i("trybool", "Orange : " + Boolean.toString(bool_orange));
                Log.i("trybool", "Bytel : " + Boolean.toString(bool_bytel));
                Log.i("trybool", "op : " + str_operateur);
                Edit_operateur.setText(str_operateur);
                Edit_Phase.setText(str_Phase);
                if(bool_site_neuf){
                    Edit_site_neuf.setText("Site neuf");
                }
                else{
                    Edit_site_neuf.setText("Site existant");
                }
            }
            //Si le tableau n'est pas vide, on est mode édition
            if(Data_CR !=null){
                Edit_operateur.setText(Data_CR.get(0));
                Edit_code_site.setText(Data_CR.get(1));
                Edit_type_site.setText(Data_CR.get(2));
                Edit_projet.setText(Data_CR.get(3));
                String t = Data_CR.get(4);
                if(t.trim().matches("Site neuf")){
                    Edit_site_neuf.setText("Site neuf");
                    bool_site_neuf=true;
                }
                else if(t.trim().matches("Site existant")){
                    Edit_site_neuf.setText("Site existant");
                    bool_site_neuf=false;
                }
//                if(bool_site_neuf){
//                    Edit_site_neuf.setText("Site neuf");
//                }
//                else{
//                    Edit_site_neuf.setText("Site existant");
//                }
                Edit_nom_site.setText(Data_CR.get(5));
                Edit_adresse.setText(Data_CR.get(6));
                Edit_code_postal.setText(Data_CR.get(7));
                Edit_ville.setText(Data_CR.get(8));
                Edit_Phase.setText(Data_CR.get(9));
                Edit_coordonnes.setText(Data_CR.get(10));
                test="";
                for(int i=11;i<Data_CR.size();i++){
                    test=test+Data_CR.get(i)+"\n";
                }
                Edit_Commentaire.setText(test);

                //Initialisation des booléens pour générer les logos dans le pdf
                String Operateur = Edit_operateur.getText().toString();
                if(Operateur.matches("Orange")){
                    bool_orange=true;
                    bool_bytel=false;
                    bool_free=false;
                }
                else if(Operateur.matches("Bouygues Télécom")){
                    bool_bytel=true;
                    bool_orange=false;
                    bool_free=false;
                }
                else if(Operateur.matches("Free Mobile")){
                    bool_bytel=false;
                    bool_orange=false;
                    bool_free=true;
                }
                str_Type_site=Edit_type_site.getText().toString();


                //On récupère le nombre de photos "Diverses" prises pour ce site
                int pict_div_number =0;
                File path = new File (FOLDERPATH+"/Photos");
                //Passe dans la boucle si le dossier en question existe bien
                if( path.exists() ) {
                    File[] files = path.listFiles();
                    if (files != null) {
                        //Supprime les fichiers un par un
                        for (File file : files) {
                            if (file.getPath().contains(str_code_site+"_Image_Diverse")) {
                                pict_div_number++;
                            }
                        }
                }
                }

                Txt_number_pictures.setText(String.valueOf(pict_div_number));

            }

            if(test!=null){
                Log.i("trybund",test);
            }

        }catch (Exception e){}

        //Récupération des coordonnées quand appuie sur l'icone
        Map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED){
                    getCurrentLocation();
                }
            }
        });

        //Ouverture de l'appareil photo quand clic sur l'image
        AppPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //On ouvre l'appareil photo en indiquant l'URI de la photo à créer
                String Nom_image=str_code_site+"_Image_Diverse";
                String dossier = FOLDERPATH+"/Photos";
                File Path  =new File(dossier);
                //On liste les fichiers du dossier "Photos"
                File[] list_img = Path.listFiles();
                int nb=0;
                if(list_img!=null){
                    //On parcourt la liste des fichiers
                    for(int i=0; i<list_img.length;i++){
                        //On vérifie si une photo à déjà été prise pour l'élément en cours
                        nb=Verif_exist(list_img[i], Nom_image, nb);
                    }
                }
                //On fixe le nom de la photo selon le nombre d'éléments
                if(nb!=0){
                    Nom_image=Nom_image+"("+nb+")";
                }
                Uri uriimage = FileProvider.getUriForFile(getActivity(),getActivity().getApplicationContext().getPackageName() +".provider", new File(dossier+"/"+Nom_image+".JPG"));
                Intent intent_picture=new Intent((MediaStore.ACTION_IMAGE_CAPTURE));
                intent_picture.putExtra(MediaStore.EXTRA_OUTPUT,uriimage);
                intent_picture.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                ((Activity) getActivity()).startActivityForResult(intent_picture,99);
            }
        });

        //EditText commentaire scrollable
        Edit_Commentaire.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (Edit_Commentaire.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                    }
                }
                return false;
            }
        });

        return v;
    }

//#########################################################################################################################################################
//--------------------------------------------------------------------FIN METHODE ONCREATEVIEW------------------------------------------------------
//#########################################################################################################################################################

    //Récupération des coordonnées GPS
    @SuppressLint("MissingPermission")
    private void getCurrentLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService((Context.LOCATION_SERVICE));
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            client.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                        LocationRequest locationRequest = new LocationRequest()
                                .setPriority(PRIORITY_HIGH_ACCURACY)
                                .setInterval(10000)
                                .setFastestInterval(1000)
                                .setNumUpdates(1);

                        LocationCallback locationCallback = new LocationCallback(){
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                Location location = locationResult.getLastLocation();
                                location.setAccuracy(7);
                                //On place les coordonnées dans l'edit Text
                                Edit_coordonnes.setText(String.valueOf(location.getLatitude())+" ; "+String.valueOf(location.getLongitude()));
                            }
                        };
                        client.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
                    }
//                }
            });
        }
        else{
            displayMsg("Veuillez activer la localisation sur votre appareil");
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }

    }

    public int Verif_exist(File img, String Nom_image, int nb){
        if(img.getName().contains(Nom_image)){
            nb++;
            return  nb;
        }
        else{
            return  nb;
        }

    }

    //Génération du fichier txt avec les éléments de base renseignés dans le formulaire
    public String generation_CR_txt(){
        str_Contenu_CRTXT =
                Edit_operateur.getText() + line
                +Edit_code_site.getText() + line
                +Edit_type_site.getText() + line
                +Edit_projet.getText() + line
                +Edit_site_neuf.getText() + line
                +Edit_nom_site.getText() + line
                +Edit_adresse.getText() + line
                +Edit_code_postal.getText() + line
                +Edit_ville.getText() + line
                + Edit_Phase.getText() + line
                + Edit_coordonnes.getText()+line
                + Edit_Commentaire.getText() + line
                +"*****";
        return str_Contenu_CRTXT;
    }

    public void Nommage_dossier(){
        String nom_site = Edit_nom_site.getText().toString();
        if(nom_site.contains("/")){
            nom_site = Edit_nom_site.getText().toString().replace("/"," ");
        }
        String FILENAME = Edit_code_site.getText() + "_" + nom_site + "_VT_" + date;
        FILENAMEPDF = FILENAME + ".pdf";
        FILENAMETXT = FILENAME + ".txt";
        FOLDERNAME = "Comptes-rendus de VT"+ "/" + Edit_operateur.getText() + "/" + Edit_projet.getText() + "/" + Edit_code_site.getText() + "_" +  nom_site;
    }


    public void displayMsg(String str){
        Toast.makeText(getContext(), str, Toast.LENGTH_LONG).show();
    }

    public ArrayList<String> collect_donnees(){
        ArrayList<String> contenu=new ArrayList<>();
        contenu.add(Edit_operateur.getText().toString().trim());
        contenu.add(Edit_code_site.getText().toString().trim());
        contenu.add(Edit_type_site.getText().toString().trim());
        contenu.add(Edit_projet.getText().toString().trim());
        contenu.add(Edit_site_neuf.getText().toString().trim());
        contenu.add(Edit_nom_site.getText().toString().trim());
        contenu.add(Edit_adresse.getText().toString().trim());
        contenu.add(Edit_code_postal.getText().toString().trim());
        contenu.add(Edit_ville.getText().toString().trim());
        contenu.add(Edit_Phase.getText().toString().trim());
        contenu.add(Edit_Commentaire.getText().toString().trim());
        contenu.add(Edit_coordonnes.getText().toString().trim());

        return contenu;
    }

}
