package com.example.pfe_assistantconception;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

//*********************************************************************************************************************************************************
//                                                        CLASSE POUR CREER LES NOTIFICATIONS
//*********************************************************************************************************************************************************

public class App extends android.app.Application {

    public static final String CHANNEL_1_ID = "channel1";
    public static final String CHANNEL_2_ID = "channel2";
    public static final String CHANNEL_3_ID = "channel3";


    @Override
    public void onCreate() {
        super.onCreate();

        createNotificationChannels();

    }

    private void createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Channel 1 pour notification ponctuelle
            NotificationChannel channel1 = new NotificationChannel(
                    CHANNEL_1_ID,
            "Dépot terminé",
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel1.setDescription("Notification déclenchée lorsqu'un compte-rendu a bien été déposé");

            NotificationChannel channel2 = new NotificationChannel(
                    CHANNEL_2_ID,
                    "Dépot en cours...",
                    NotificationManager.IMPORTANCE_LOW
            );
            //Notification de type progrès
            channel2.setDescription("Notification déclenchée lorsqu'un compte-rendu est en cours de dépôt");

            NotificationChannel channel3 = new NotificationChannel(
                    CHANNEL_3_ID,
                    "Génération du CR en cours...",
                    NotificationManager.IMPORTANCE_LOW
            );
            channel3.setDescription("Notification déclenchée lorsqu'un compte-rendu est en cours de génération");




            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel1);
            manager.createNotificationChannel(channel2);
            manager.createNotificationChannel(channel3);
        }
    }
}


