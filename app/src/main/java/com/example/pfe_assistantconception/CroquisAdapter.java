package com.example.pfe_assistantconception;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;




public class CroquisAdapter extends RecyclerView.Adapter<CroquisAdapter.CroquisViewHolder> {

    ArrayList<Bitmap> Liste_Croquis = new ArrayList<>();
    ArrayList<String> Nom_Croquis = new ArrayList<>();
    String Code_site;
    CroquisViewHolder croquisViewHolder;
    private CroquisAdapter.OnItemClickListener Listener;


    public CroquisAdapter(ArrayList<Bitmap> liste, ArrayList<String> nom, String code_site) {
        this.Liste_Croquis = liste;
        this.Nom_Croquis =nom;
        this.Code_site = code_site;
    }

    //Initialisation des méthodes onclicks sur les éléments
    public interface OnItemClickListener {
        void OnEditClick(int position);

        void OnDeleteClick(int position);
    }

    public void setOnItemClickListener(CroquisAdapter.OnItemClickListener listener) {
        Listener = listener;
    }

    public static class CroquisViewHolder extends RecyclerView.ViewHolder {

        ImageView Img_Croquis, Img_edit, Img_del;
        TextView Titre_Croquis;

        public CroquisViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);

            Img_Croquis = itemView.findViewById(R.id.Img_croquis);
            Titre_Croquis = itemView.findViewById(R.id.Txt_Nom_Croquis);
            Img_edit = itemView.findViewById(R.id.Img_edit_croquis);
            Img_del = itemView.findViewById(R.id.Img_delete_croquis);

            Img_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.OnEditClick(position);
                        }
                    }

                }
            });

            Img_del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.OnDeleteClick(position);
                        }
                    }
                }
            });
        }

    }

    @NonNull
    @Override
    public CroquisViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_croquis, parent, false);
        croquisViewHolder = new CroquisAdapter.CroquisViewHolder(view, Listener);
        return croquisViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CroquisViewHolder holder, int position) {
        //On modifie le texte à afficher dans la liste des croquis, on supprime le code site et le .png
        String s = Nom_Croquis.get(position).replace(Code_site+"_","");
        String s2=s.replace(".png","");

        holder.Titre_Croquis.setText(s2);
        holder.Img_Croquis.setImageBitmap(Liste_Croquis.get(position));

    }

    @Override
    public int getItemCount() {
        return Liste_Croquis.size();
    }

}
