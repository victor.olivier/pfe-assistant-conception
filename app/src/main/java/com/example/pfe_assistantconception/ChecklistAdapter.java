package com.example.pfe_assistantconception;

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//*********************************************************************************************************************************************************
//                                                        ADAPTER DE LA CHECKLIST
//*********************************************************************************************************************************************************

public class ChecklistAdapter extends RecyclerView.Adapter<ChecklistAdapter.ChecklistViewHolder> implements Filterable {

    Context context;
    private ChecklistAdapter.OnItemClickListener Listener;
    private ChecklistAdapter.OnTextChange ListenerTxt;
    Fragment_Checklist fragment_checklist;
    LifecycleOwner lifecycleowner;
    ChecklistViewHolder checklistViewHolder;
    //Liste contenant les éléments répondant aux conditions du filtrage
    public ArrayList<HashMap<String, String>> List;
    //Liste complète des éléments
    public ArrayList<HashMap<String, String>> ListFULL;
    List<HashMap<String, String>> filteredListwithoutAccent = new ArrayList<>();


    public ChecklistAdapter(ArrayList<HashMap<String, String>> dataList, Context context, LifecycleOwner lifecyclerOwner, Fragment_Checklist fragment_checklist) {
        this.context = context;
        this.lifecycleowner = lifecyclerOwner;
        List = dataList;
        ListFULL = new ArrayList<>(List);
        this.fragment_checklist = fragment_checklist;

        //On parcourt la liste de tous les éléments de la checklist
        for (int i = 0; i < ListFULL.size(); i++) {
            HashMap<String, String> DataMap = new HashMap<>();
            String nom = removeDiacriticalMarks(ListFULL.get(i).get("Nom"));
            DataMap.put("Nom", nom);
            String descriptif = removeDiacriticalMarks(ListFULL.get(i).get("Descriptif"));
            DataMap.put("Descriptif", descriptif);
            String Cat = removeDiacriticalMarks(ListFULL.get(i).get("Title"));
            DataMap.put("Title", Cat);
            String Comm = removeDiacriticalMarks(ListFULL.get(i).get("Commentaire"));
            DataMap.put("Commentaire", Comm);
            filteredListwithoutAccent.add(DataMap);
        }

    }

    //Initialisation des méthodes onclicks sur les éléments
    public interface OnItemClickListener {
        void onAppPhotoClick(int position) throws IOException;

        void OnItemClick(int position);

        void OnEyeClick(int position);

        void OnGalleryClick(int position);
    }

    public void setOnItemClickListener(ChecklistAdapter.OnItemClickListener listener) {
        Listener = listener;
    }

    public interface OnTextChange {
        void addCommentary(int position, String Commentaire);
    }

    public void setOnTextChange(ChecklistAdapter.OnTextChange listener) {
        ListenerTxt = listener;
    }

    //View HOLDER

    public static class ChecklistViewHolder extends RecyclerView.ViewHolder {

        TextView Nom_Element, Descriptif_Element, Txt_Pictures_Number;
        EditText Zone_Commentaire;
        ImageView App_photo, Check_green, Img_open_pictures, Img_add_picture_gallery;


        public ChecklistViewHolder(@NonNull View itemView, OnItemClickListener listener, OnTextChange listenertxt) {
            super(itemView);
            Nom_Element = itemView.findViewById(R.id.Txt_NomElement);
            Descriptif_Element = itemView.findViewById(R.id.Txt_DescriptifElement);
            Zone_Commentaire = itemView.findViewById(R.id.Edit_commentaire);
            App_photo = itemView.findViewById(R.id.Img_App_Photo);
            Txt_Pictures_Number = itemView.findViewById(R.id.Txt_view_nb_pictures);
            Check_green = itemView.findViewById(R.id.check_green);
            Img_open_pictures=itemView.findViewById(R.id.Img_open_pictures);
            Img_add_picture_gallery = itemView.findViewById(R.id.Img_add_picture_gallery);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.OnItemClick(position);
                        }
                    }
                }
            });

            App_photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            try {
                                listener.onAppPhotoClick(position);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });

            Img_open_pictures.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.OnEyeClick(position);
                        }
                    }
                }
            });

            Img_add_picture_gallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.OnGalleryClick(position);
                        }
                    }

                }
            });

                //SETONCLICK IMG

            Zone_Commentaire.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {


                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (Zone_Commentaire.hasFocus()) {
                        if (listenertxt != null) {
                            int position = getAdapterPosition();
                            if (position != RecyclerView.NO_POSITION) {
                                String Comm = s.toString();
                                listenertxt.addCommentary(position, Comm);
                            }
                        }
                    }
                }
            });

        }
    }

    @NonNull
    @Override
    public ChecklistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checklist, parent, false);
        checklistViewHolder = new ChecklistViewHolder(view, Listener, ListenerTxt);
        return checklistViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ChecklistViewHolder holder, int position) {
        if (List.size() == 0) {
            //Afficher msg erreur
        }

        HashMap<String, String> dataMap = List.get(position);
        holder.Nom_Element.setText(dataMap.get("Nom"));
        holder.Descriptif_Element.setText(dataMap.get("Descriptif"));

        //-------------------------------------------------------------Affichage ou non de la zone de commentaire--------------------------------------
        if (dataMap.get("Commentaire_oui_non").contains("oui")) {
            holder.Zone_Commentaire.setVisibility(View.VISIBLE);
            if (dataMap.get("Commentaire").contains("REQUIRED")) {
                holder.Zone_Commentaire.setText("");

            } else {
                holder.Zone_Commentaire.setText(dataMap.get("Commentaire"));
            }
        } else {
            holder.Zone_Commentaire.setVisibility(View.GONE);
        }

        //-------------------------------------------------------------Affichage ou non de l'appareil photo et des commentaires--------------------------------------

        if (dataMap.get("Photos_oui_non").contains("oui")) {
            holder.App_photo.setVisibility(View.VISIBLE);
            holder.Txt_Pictures_Number.setVisibility((View.VISIBLE));
            holder.Img_open_pictures.setVisibility(View.VISIBLE);
            holder.Img_add_picture_gallery.setVisibility(View.VISIBLE);
        } else {
            holder.App_photo.setVisibility(View.GONE);
            holder.Txt_Pictures_Number.setVisibility((View.GONE));
            holder.Img_open_pictures.setVisibility(View.GONE);
            holder.Img_add_picture_gallery.setVisibility(View.GONE);
        }

        if (dataMap.get("CORRECT_INCORRECT").matches("CORRECT")) {
            holder.Check_green.setVisibility(View.VISIBLE);
//            holder.itemView.setBackgroundColor(Color.parseColor("#009900"));
        } else {
            holder.Check_green.setVisibility(View.GONE);
        }

        holder.Txt_Pictures_Number.setText(dataMap.get("Pictures_number"));

        //Si il n'y a pas de photos prise pour l'élément, le bouton pour visualiser les photos n'est pas affiché
        if(dataMap.get("Pictures_number").matches("0")){
            holder.Img_open_pictures.setVisibility(View.GONE);
        }
        else{
            holder.Img_open_pictures.setVisibility(View.VISIBLE);
        }

        //EditText commentaire scrollable
        holder.Zone_Commentaire.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (holder.Zone_Commentaire.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                    }
                }
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return List.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public Filter getFilter() {
        return Filter;
    }

    //Filtre de recherche

    private Filter Filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<HashMap<String, String>> filteredList = new ArrayList<>();
            //Si il n'y a pas de filtre mis en place, la liste filtrée est égale à la liste complète
            String filterPattern = null;
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(ListFULL);
            } else {
                filterPattern = constraint.toString().toLowerCase().trim();
                //On rempli la liste des éléments à afficher selon le critère renseigné
                for (int i = 0; i < filteredListwithoutAccent.size(); i++) {
                    if (filteredListwithoutAccent.get(i).get("Nom").toLowerCase().contains(filterPattern) || filteredListwithoutAccent.get(i).get("Title").toLowerCase().contains(filterPattern) || filteredListwithoutAccent.get(i).get("Commentaire").toLowerCase().contains(filterPattern) || filteredListwithoutAccent.get(i).get("Descriptif").toLowerCase().contains(filterPattern)) {
                        filteredList.add(ListFULL.get(i));
                    } else if (ListFULL.get(i).get("Nom").toLowerCase().contains(filterPattern) || ListFULL.get(i).get("Title").toLowerCase().contains(filterPattern) || ListFULL.get(i).get("Commentaire").toLowerCase().contains(filterPattern) || ListFULL.get(i).get("Descriptif").toLowerCase().contains(filterPattern)) {
                        filteredList.add(ListFULL.get(i));

                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List.clear();
            List.addAll((List) results.values);
            if (List.size() == 0) {
                fragment_checklist.Txt_search_nok.setVisibility(View.VISIBLE);
                fragment_checklist.Search_check.setBackgroundColor(Color.parseColor("#CD1B1B"));
            } else {
                fragment_checklist.Txt_search_nok.setVisibility(View.GONE);
                fragment_checklist.Search_check.setBackgroundColor(Color.parseColor("#EA751F"));
            }
            notifyDataSetChanged();
        }
    };


    public static String removeDiacriticalMarks(String string) {
        return Normalizer.normalize(string, Normalizer.Form.NFD)
                .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }

    public ArrayList<HashMap<String, String>> getListfull() {
        return ListFULL;
    }

    public void displayMsg(String str) {
        Toast.makeText(context, str, Toast.LENGTH_LONG).show();
    }
}