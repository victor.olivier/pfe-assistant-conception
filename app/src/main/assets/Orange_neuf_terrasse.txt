Acc�s/Acc�s de la rue/Rep�rer et photographier l'acc�s de la rue/commentaire_oui/photo_oui
Acc�s/Acc�s de l'immeuble/Rep�rer et photographier l'acc�s de l'immeuble/commentaire_oui/photo_oui
Acc�s/Acc�s du toit/Rep�rer et photographier l'acc�s du toit/commentaire_oui/photo_oui
Acc�s/Acc�s de la ZT/Rep�rer et photographier l'acc�s de la zone technique/commentaire_oui/photo_oui
Acc�s/Acc�s aux a�riens/Rep�rer l'acc�s aux a�riens/commentaire_oui/photo_oui
G�nie Civil/Conduits d'a�ration/Photographier et c�ter les conduits d'a�ration de la terrasse/commentaire_oui/photo_oui
Adduction/ENEDIS/Rep�rer l'arriv�e Enedis/commentaire_oui/photo_oui
Adduction/Colonne montante/Photographier la colonne montante de l'immeuble/commentaire_oui/photo_oui
Adduction/FTTA/Rep�rer l'arriv�e FTTA (si existante)/commentaire_oui/photo_oui
Travaux/Nacelle/Rep�rer l'emplacement de la nacelle pour les travaux/commentaire_oui/photo_oui
Travaux/Grue/Rep�rer l'emplacement de la grue pour les travaux/commentaire_oui/photo_oui
Travaux/Risques travaux/Liste les risques travaux (r�seaux d'eau ou de gaz, ligne haute tension etc...)/commentaire_oui/photo_oui
Travaux/Arret� de voirie/Indiquer si un arret� de voirie est n�cessaire ou non/commentaire_oui/photo_non
Travaux/Ascenceur/Indiquer le poids max de l'ascenceur/commentaire_oui/photo_oui
Travaux/Conduits d'a�ration/Rep�rer les conduits d'a�ration/commentaire_oui/photo_oui
Travaux/Conduits de chauffage/Rep�rer les conduits de chauffage/commentaire_oui/photo_oui
Travaux/Ascenceur/Rep�rer l'ascenceur et indiquer le poids max/commentaire_oui/photo_oui
Plans et Photomontages/Vue g�n�rale/Prendre une photo g�n�rale du site/commentaire_non/photo_oui
Plans et Photomontages/Vue de loin/Prendre une photo de loin pour les photomontages/commentaire_non/photo_oui
Plans et Photomontages/Vue de pr�s/Prendre une photo de pr�s pour les photomontages/commentaire_non/photo_oui
Plans et Photomontages/Hauteur du b�timent/Mesurer la hauteur du b�timent/commentaire_oui/photo_non
Plans et Photomontages/Longueur du b�timent/Mesurer la longueur du b�timent/commentaire_oui/photo_non
Plans et Photomontages/Acrot�re/Mesurer l'acrot�re et le photographier �l'emplacement de chaque secteur/commentaire_oui/photo_oui
Radio/Antennes/Lister les antennes panneaux � poser et photographier leurs emplacements/commentaire_oui/photo_oui
Radio/FH/Lister le(s) FHs �poser et photographier les emplacements possibles/commentaire_oui/photo_oui
Radio/RRU/Lister les RRUs � poser et photographier les emplacements possibles/commentaire_oui/photo_oui
Radio/Baies/Lister le nombre de baies � installer et photographier les emplacements possibles/commentaire_oui/photo_oui
Radio/Chemins de c�bles/Rep�rer les emplacements des chemins de c�bles/commentaire_oui/photo_oui
N�go/Info bailleur/Renseigner les coordonn�es du bailleur/commentaire_oui/photo_non
N�go/PLU/Indiquer les sp�cificit�s du PLU de la zone/commentaire_oui/photo_non
N�go/Int�gration/Indiquer les �l�ments d'int�grations � pr�voir/commentaire_oui/photo_non
N�go/Sites sensibles/Lister les sites sensibles � proximit� (�cole, cr�che, h�pital)/commentaire_oui/photo_non
N�go/Balcons/Lister le nombre de balcons pouvant se trouver dans le champs des antennes/commentaire_oui/photo_oui
Panoramiques/Secteur 1/Prendre les photos pour le panoramique du S1/commentaire_non/photo_oui
Panoramiques/Secteur 1 bis/Prendre les photos pour le panoramique du S1 bis/commentaire_non/photo_oui
Panoramiques/Secteur 2/Prendre les photos pour le panoramique du S2/commentaire_non/photo_oui
Panoramiques/Secteur 2 bis/Prendre les photos pour le panoramique du S2 bis/commentaire_non/photo_oui
Panoramiques/Secteur 3/Prendre les photos pour le panoramique du S3/commentaire_non/photo_oui
Panoramiques/Secteur 3 bis/Prendre les photos pour le panoramique du S3 bis/commentaire_non/photo_oui
S�curit�/Gardes-corps/Photographier les gardes-corps si le site en est �quip�/commentaire_oui/photo_oui
S�curit�/S�curit� collective ?/L'�difice est-il en s�curit� collective ?/commentaire_oui/photo_non
S�curit�/Balisage/Y a t-il un balisage � respecter pour circuler sur l'�difice ?/commentaire_oui/photo_oui
S�curit�/Saut de loup/Une pose de saut de loup est-elle n�cessaire ?/commentaire_oui/photo_oui
S�curit�/Divers/Lister et photographier des �l�ments de s�curit� divers/commentaire_oui/photo_oui